package com.seb.spg.gateway;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import com.seb.spg.jpa.dao.PaymentData;

public class PaymentDataStore extends LinkedHashMap<String, String> {

	private static final long serialVersionUID = 3621092644936178627L;

	private List<String> persistentExclusion = new ArrayList<>();
	
	public String put(String key, String value, boolean persistent){
		
		if(!persistent && !persistentExclusion.contains(key)){
			persistentExclusion.add(key);
		}
		
		return super.put(key, value);
	}
	
	public List<PaymentData> toPersistentList(long paymentId){
		
		List<PaymentData> datas = new ArrayList<>();
		
		if(!isEmpty()){
			
			for(java.util.Map.Entry<String, String> entry : entrySet()){
				
				if(!persistentExclusion.contains(entry.getKey())){
					PaymentData data = new PaymentData();
					data.setPaymentId(paymentId);
					data.setPaymentDataName(entry.getKey());
					if(entry.getValue() != null && !entry.getValue().isEmpty()){
						data.setPaymentDataValue(entry.getValue());
					}
					datas.add(data);	
				}
			}
		}
		
		return datas;
	}
}
