package com.seb.spg.gateway;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.server.ParamException.FormParamException;

@Provider
public class TypeRequestValidator implements ExceptionMapper<FormParamException> {

	private static final Logger logger = LogManager.getLogger(TypeRequestValidator.class);

	@Override
	public Response toResponse(FormParamException exception) {

		StringBuilder sb = new StringBuilder();
		sb.append("invalid data type for field ").append(exception.getParameterName());
		
		String message = sb.toString();
		logger.error(message);
		
		return Response.status(Response.Status.BAD_REQUEST)
				.entity(message)
				.type("text/plain")
				.build();
	}
}