package com.seb.spg.gateway;

import java.util.Iterator;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Provider
public class GenericRequestValidator implements ExceptionMapper<ConstraintViolationException> {

	private static final Logger logger = LogManager.getLogger(PaymentGateway.class);
	
    @Override
    public Response toResponse(final ConstraintViolationException exception) {
        return Response.status(Response.Status.BAD_REQUEST)
                       .entity(prepareMessage(exception))
                       .type("text/plain")
                       .build();
    }

    private String prepareMessage(ConstraintViolationException exception) {
    	
    	StringBuilder sb = new StringBuilder();
    	
    	Iterator<ConstraintViolation<?>> iterator = exception.getConstraintViolations().iterator();
    	while(iterator.hasNext()){
    		ConstraintViolation<?> cv = iterator.next();
    		sb.append(cv.getMessage());
    		
    		if(iterator.hasNext()){
    			sb.append("\n");
    		}
    	}
    	
    	String message = sb.toString();
    	logger.error(message);
    	
        return message;
    }
}