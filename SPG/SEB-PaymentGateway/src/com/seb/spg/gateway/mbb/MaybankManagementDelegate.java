package com.seb.spg.gateway.mbb;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.anteater.library.delegate.BaseDelegate;
import com.anteater.library.jpa.DatabaseFacade;
import com.anteater.library.jpa.DatabaseFacadeException;
import com.seb.spg.consts.PaymentStage;
import com.seb.spg.jpa.dao.Payment;
import com.seb.spg.jpa.helper.ConfigHelper;
import com.seb.spg.jpa.helper.PaymentHelper;

public class MaybankManagementDelegate implements BaseDelegate {

	private static final Logger logger = LogManager.getLogger(MaybankManagementDelegate.class);

	private static ScheduledExecutorService retryScheduler;

	@Override
	public void start() {
		logger.info("Maybank Management Delegate Started");
		
		createRetryScheduler();
	}
	
	private void createRetryScheduler(){
		Runnable run = new Runnable() {
			@Override
			public void run() {
				performServerToServerUpdateRetry();
			}
		};
		
		int retrySchedulerIntervalMinutes = 2;
		
		retryScheduler = Executors.newSingleThreadScheduledExecutor();
		retryScheduler.scheduleAtFixedRate(run, retrySchedulerIntervalMinutes, 
				retrySchedulerIntervalMinutes, TimeUnit.MINUTES);
		
		logger.info("Maybank S2S Retry scheduler configured at {} minutes interval", retrySchedulerIntervalMinutes);
	}
	
	@Override
	public void stop() {
		
		if(retryScheduler != null){
			retryScheduler.shutdown();
		}
		
		logger.info("Maybank Management Delegate Stopped");
	}
	
	private void performServerToServerUpdateRetry(){
		try(DatabaseFacade db = new DatabaseFacade()){
			ConfigHelper cHelper = new ConfigHelper(db);
			PaymentHelper pHelper = new PaymentHelper(db);
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			cal.add(Calendar.MINUTE, cHelper.getServerToServerUpdateRetryWithinMinutes() * -1);
			
			List<Payment> payments = pHelper.getUnnotifiedPayments(MaybankPaymentProcessor.GATEWAY_ID, cal.getTime());
			
			for(Payment p : payments){
			
				try(DatabaseFacade dbLock = new DatabaseFacade()){
					PaymentHelper lockedPaymentHelper = new PaymentHelper(dbLock);
					
					dbLock.beginTransaction();
					Payment payment = lockedPaymentHelper.getLockedPayment(p.getPaymentId());
					
					if(payment.getStatus() != PaymentStage.RECEIVED_FAILED && 
							payment.getStatus() != PaymentStage.NOTIFIED_SUCCESS){
						logger.info("This payment {} was successfully notified in another process", p.getPaymentId());
					}else{
						
						String transactionStatus = payment.getGatewayStatus().getStatus();
						if(MaybankPaymentProcessor.handlingServerToServerPaymentResponse(dbLock, payment, transactionStatus)){
							dbLock.update(payment);
						}
					}
					
					dbLock.commit();
				} catch (DatabaseFacadeException e) {
					logger.error("error processing payment - " + p.getPaymentId(), e);
				}
			}	
		}
	}
}
