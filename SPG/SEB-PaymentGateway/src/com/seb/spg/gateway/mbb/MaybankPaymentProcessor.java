package com.seb.spg.gateway.mbb;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.http.HttpException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.anteater.library.api.utility.StringUtil;
import com.anteater.library.jpa.DatabaseFacade;
import com.anteater.library.jpa.DatabaseFacadeException;
import com.seb.spg.consts.BypassGatewayStatus;
import com.seb.spg.consts.MbbCardType;
import com.seb.spg.consts.PaymentResponseStatus;
import com.seb.spg.consts.PaymentStage;
import com.seb.spg.consts.StatusDirection;
import com.seb.spg.consts.WebPath;
import com.seb.spg.gateway.FormBuilder;
import com.seb.spg.gateway.PaymentDataStore;
import com.seb.spg.gateway.PaymentGateway;
import com.seb.spg.gateway.exception.InvalidPaymentRequestException;
import com.seb.spg.gateway.exception.InvalidPaymentStatusException;
import com.seb.spg.jpa.dao.Merchant;
import com.seb.spg.jpa.dao.Payment;
import com.seb.spg.jpa.dao.PaymentData;
import com.seb.spg.jpa.dao.Status;
import com.seb.spg.jpa.helper.ConfigHelper;
import com.seb.spg.jpa.helper.MerchantHelper;
import com.seb.spg.jpa.helper.PaymentHelper;
import com.seb.spg.util.CryptographyUtil;
import com.seb.spg.util.HttpUtil;

public abstract class MaybankPaymentProcessor extends PaymentGateway<MaybankPaymentRequest, MaybankPaymentQuery, MaybankPaymentStatus> {

	private static final Logger logger = LogManager.getLogger(MaybankPaymentProcessor.class);
	
	public static final String GATEWAY_ID = "MBB";
	
	public static final String MBB_SUBMIT_URL_NAME = "SUBMIT_URL";
	public static final String MBB_QUERY_URL_NAME = "QUERY_URL";
	public static final String MBB_MERCHANT_ID_NAME = "MERCHANT_ID";
	public static final String MBB_HASH_KEY_NAME = "HASH_KEY";
	public static final String MBB_AMEX_MERCHANT_ID_NAME = "AMEX_MERCHANT_ID";
	public static final String MBB_AMEX_HASH_KEY_NAME = "AMEX_HASH_KEY";
	
	protected MbbCardType cardType;
	protected Date cardExpiry;
	
	protected long paymentId;
	protected Payment payment;
	
	@Override
	public Response makePayment(MaybankPaymentRequest request) throws InvalidPaymentRequestException {
		
		validatePaymentRequest(request);
		
		Payment payment = preparePayment(request);
		
		try(DatabaseFacade db = new DatabaseFacade()){
			
			ConfigHelper cHelper = new ConfigHelper(db);
			PaymentHelper pHelper = new PaymentHelper(db);// check payment id duplication
			Payment existingPayment = pHelper.getPayment(merchant.getMerchantId(), request.getTransactionId());
			if(existingPayment != null){
				logger.error("Merchant {} already have an existing transaction with Id {}", merchant.getMerchantName(), 
						request.getTransactionId());
				
				PaymentResponseStatus status = PaymentResponseStatus.REQUEST_DUPLICATE;
				String message = "Duplicate transaction_id " + request.getTransactionId();
				
				Status newStatus = new Status();
				newStatus.setPaymentId(existingPayment.getPaymentId());
				newStatus.setDirection(StatusDirection.SPG_TO_MERCHANT);
				newStatus.setStatus(status.toString());
				newStatus.setRemark(message);
				
				db.beginTransaction();
				db.insert(newStatus);
				db.commit();
				
				FormBuilder form = generateResponseForm(request, status, merchant, message);
				return form.toResponse(true);
			}else{
				db.beginTransaction();
				db.insert(payment);
				logger.info("Payment Id '{}' generated for merchant transaction '{}'", payment.getPaymentId(), request.getTransactionId());
				
				PaymentDataStore paymentDataStore = getPaymentData(payment.getPaymentId(), request, cHelper.getPublicWebPath());
				List<PaymentData> paymentDatas = paymentDataStore.toPersistentList(payment.getPaymentId());
				
				for(PaymentData paymentData : paymentDatas){
					db.insert(paymentData);
				}
				
				logger.debug("inserted {} payment data", paymentDatas.size());
				
				db.commit();
				
				if(isBypassGatewayRounting()){
					logger.info(">>>> TESTING MODE STARTED <<<<");
					return getBypassGatewayResponse(payment);
				}else{
					String submitUrl = getGatewayConfig(MBB_SUBMIT_URL_NAME);
					logger.debug("Payment committed successfully, now preparing Auto-Submit form of Action {}", submitUrl);
					FormBuilder form = new FormBuilder(submitUrl, paymentDataStore);
					return form.toResponse(isTestModeEnabled());	
				}
			}
		}catch (DatabaseFacadeException e) {			
			logger.error("error makePayment() - " + e.getMessage(), e);
			
			PaymentResponseStatus status = PaymentResponseStatus.SERVER_ERROR;
			String message = "Internal server error";
			
			FormBuilder form = generateResponseForm(request, status, merchant, message);
			return form.toResponse(true);
		} catch (InvalidPaymentStatusException e) {
			logger.error("error makePayment() - bypass - " + e.getMessage(), e);
			
			PaymentResponseStatus status = PaymentResponseStatus.REQUEST_ERROR;
			String message = e.getMessage();
			
			FormBuilder form = generateResponseForm(request, status, merchant, message);
			return form.toResponse(true);
		} 
	}

	@Override
	public Response queryPayment(MaybankPaymentQuery query) throws InvalidPaymentRequestException {
		
		validatePaymentQuery(query);
		
		try(DatabaseFacade db = new DatabaseFacade()){
			PaymentHelper pHelper = new PaymentHelper(db);
			ConfigHelper cHelper = new ConfigHelper(db);
			
			db.beginTransaction();
			logger.info("Requesting write locks on spg.payment - payment_id - {}", paymentId);
			Payment payment = pHelper.getLockedPayment(merchant.getMerchantId(), query.getTransactionId());
			
			if(payment == null){
				logger.error("invalid transaction_id - {} - not found", query.getTransactionId());			
				throw new InvalidPaymentStatusException("not found transaction_id - " + query.getTransactionId());
			}else if(!payment.getGatewayId().equals(GATEWAY_ID)){
				logger.error("invalid transaction_id - {} - gateway mismatch", query.getTransactionId());			
				throw new InvalidPaymentStatusException("not found transaction_id - " + query.getTransactionId());
			}else if(payment.getAmount().compareTo(query.getAmount()) != 0){
				logger.error("invalid amount - {} - amount mismatch - {}", query.getAmount(), payment.getAmount());			
				throw new InvalidPaymentStatusException("invalid amount - " + query.getAmount());
			}
			
			logPaymentDetails(payment);
			
			FormBuilder responseForm = null;
			
			if(payment.getStatus() == PaymentStage.NEW){
				
				logger.debug("Query request for NEW payment, sending query message to Maybank");
				
				int timeout = cHelper.getGatewayQueryTimeoutInSeconds() * 1000;
				String queryUrl = getGatewayConfig(MBB_QUERY_URL_NAME);
				FormBuilder form = new FormBuilder(queryUrl, getQueryPaymentData(payment));
				
				String response = HttpUtil.post(form, timeout);

				logger.debug(response);
				
				if (StringUtil.isEmpty(response) || response.equals("ERROR")) {
					logger.error("error response from Maybank - unable to get new status");
					
					Status statusLog = getStatusForSpgToMerchant(payment.getPaymentId(), 
							"NULL", "Merchant query payment details (Maybank Response Error)");
					db.insert(statusLog);
					
					responseForm = clientPaymentResponse(PaymentResponseStatus.WAITING, payment, merchant, null);
				} else {
					
					String mbbResponse = response.replace("<BR>", "&");
					Map<String, String> responseMap = getPostResponseMap(mbbResponse);
					
					String transactionStatus = responseMap.get("TXN_STATUS");
					String responseCode = responseMap.get("RESPONSE_CODE");
					
					if(StringUtil.isEmpty(transactionStatus)){
						logger.error("TXN_STATUS not found in Maybank query response - unable to get new status");
						
						Status statusLog = getStatusForSpgToMerchant(payment.getPaymentId(), 
								"NULL", "Merchant query payment details (Maybank Response Error - TXN_STATUS not found)");
						db.insert(statusLog);
						
						responseForm = clientPaymentResponse(PaymentResponseStatus.WAITING, payment, merchant, null);
					}else{
						PaymentResponseStatus responseStatus = getSimpleResponseStatus(transactionStatus, responseCode);
						PaymentStage newPaymentStatus = responseStatus.getPaymentStage();
						
						Status incomingStatusLog = getStatusForGatewayToSpg(payment.getPaymentId(), 
								transactionStatus, "SPG Call Maybank to query payment details - " + responseCode);
						Status outgoingStatusLog = getStatusForSpgToMerchant(payment.getPaymentId(), 
								transactionStatus, "Merchant query payment details - " + responseCode);
						
						db.insert(incomingStatusLog);
						db.insert(outgoingStatusLog);
						
						if(newPaymentStatus == PaymentStage.NEW){
							logger.debug("Payment is still pending");
						}else{
							
							payment.setGatewayStatusId(incomingStatusLog.getStatusId());
							
							handlePaymentStatusChanged(db, payment, newPaymentStatus, 
									responseStatus, responseMap.get("AUTH_DATE"), responseMap.get("TRANSACTION_ID"),
									transactionStatus, responseCode, true);
							
							payment.setLastUpdatedBy("Maybank-txn-query");
							
							db.update(payment);
						}
						
						responseForm = clientPaymentResponse(responseStatus, payment, merchant, transactionStatus);
					}
				}				
			}else{
				Status statusLog = getStatusForSpgToMerchant(payment.getPaymentId(), 
						payment.getGatewayStatus().getStatus(), "Merchant query payment details");
				db.insert(statusLog);
				
				PaymentResponseStatus responseStatus = getSimpleResponseStatus(payment.getGatewayStatus().getStatus(), null);
				responseForm = clientPaymentResponse(responseStatus, payment, merchant, payment.getGatewayStatus().getStatus());
			}
			
			db.commit();
			logger.debug("locks released, payment status is now '{}'", payment.getStatus().toString());
			
			return Response.ok(responseForm.toFormUrlEncoded(), MediaType.APPLICATION_FORM_URLENCODED_TYPE).build();
			
		} catch (InvalidPaymentStatusException e) {
			logger.error("Invalid pamynet status - " + e.getMessage(), e);
			return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (DatabaseFacadeException | 
				IOException | HttpException | ParseException e) {
			logger.error("error providing query payment response - " + e.getMessage(), e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("internal_server_error").build();
		} 
	}

	@Override
	public Response clientPaymentStatusUpdate(MaybankPaymentStatus status) throws InvalidPaymentStatusException {
		try {
			PaymentResponseStatus responseStatus = handlePaymentStatus(status);
			FormBuilder form = clientPaymentResponse(responseStatus, payment, merchant, status.getResponseCode());
			return form.toResponse(true);
		} catch (DatabaseFacadeException | ParseException e) {
			logger.error("error " + e.getMessage(), e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("internal_server_error").build();
		}
	}

	@Override
	protected void validatePaymentStatus(MaybankPaymentStatus status) throws InvalidPaymentStatusException {
		if(StringUtil.isEmpty(status.getMerchantTransactionId())){
			logger.error("MERCHANT_TRANID is empty");
			throw new InvalidPaymentStatusException("MERCHANT_TRANID is empty");
		}
		Payment tmpPayment = null;
		
		try(DatabaseFacade db = new DatabaseFacade()){
			PaymentHelper pHelper = new PaymentHelper(db);
			
			paymentId = Long.parseLong(status.getMerchantTransactionId());
			tmpPayment = pHelper.getPayment(paymentId);
			
			if(tmpPayment == null){
				throw new InvalidPaymentStatusException("MERCHANT_TRANID '" + paymentId + "' not found");
			}
			
			cardType = getCardTypeUsed(tmpPayment);
			
		} catch (NumberFormatException e) {
			logger.error("invalid MERCHANT_TRANID - " + status.getMerchantTransactionId() + ", " + e.getMessage(), e);			
			throw new InvalidPaymentStatusException("invalid MERCHANT_TRANID - " + status.getMerchantTransactionId());
		}
		
		if(status.shouldVerify()){
			String hashKey = getMerchantHashKey().toUpperCase();
			String merchantAccNo = status.getMerchantAccountNo();
			String merchantTransId = status.getMerchantTransactionId();
			String amount = new DecimalFormat("0.00").format(tmpPayment.getAmount());
			String transactionId = status.getTransactionId();
			String transactionStatus = status.getTransactionStatus() == null ? null : status.getTransactionStatus().toUpperCase();
			String responseCode = status.getResponseCode();
			
			String signatureFields = combineSignatureFieldsWithDelimiter(
					hashKey, merchantAccNo, merchantTransId, amount, transactionId,
					transactionStatus, responseCode);
			
			String computeTansactionSignature = CryptographyUtil.toSha512HexadecimalUppercase(signatureFields);
			
			if(!computeTansactionSignature.equals(status.getTransactionSignature())){
				logger.error("Checksum mismatch");
				logger.error(" Request Checksum : {}", status.getTransactionSignature());
				logger.error(" Compute Signature : {}", computeTansactionSignature);
				
				throw new InvalidPaymentStatusException("signature verify error");
			}
		}
	}

	@Override
	protected void validatePaymentRequest(MaybankPaymentRequest request) throws InvalidPaymentRequestException {
		super.validatePaymentRequest(request);
		
		try{
			cardType = MbbCardType.valueOf(request.getCardType());			
		}catch(IllegalArgumentException e){
			logger.error("error parsing card_type - " + request.getCardType(), e);
			throw new InvalidPaymentRequestException("invalid card_type " + request.getCardType());
		}
		
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
			cardExpiry = sdf.parse(request.getCardExpiry());
		}catch(ParseException e){
			logger.error("error parsing card_expiry - " + request.getCardExpiry(), e);
			throw new InvalidPaymentRequestException("invalid card_expiry " + request.getCardExpiry());
		}
		
		if(!StringUtil.isDigit(request.getCardNumber())){
			logger.error("invalid card_number - {}", request.getCardNumber());
			throw new InvalidPaymentRequestException("invalid card_number " + request.getCardNumber());
		}else if(!StringUtil.isDigit(request.getCardCvc())){
			logger.error("invalid card_cvc - {}", request.getCardCvc());
			throw new InvalidPaymentRequestException("invalid card_cvc " + request.getCardCvc());
		}
		
		if(request.getCardNumber().length() != cardType.getCardNumberLength()){
			logger.error("invalid card_number length - {}", request.getCardNumber());
			throw new InvalidPaymentRequestException("invalid card_number length " + request.getCardNumber());
		}else if(request.getCardCvc().length() != cardType.getCvcLength()){
			logger.error("invalid card_cvc length - {}", request.getCardCvc());
			throw new InvalidPaymentRequestException("invalid card_cvc length " + request.getCardCvc());
		}
	}
	
	@Override
	protected String getGatewayId() {
		return GATEWAY_ID;
	}
	
	private PaymentResponseStatus handlePaymentStatus(MaybankPaymentStatus status) throws DatabaseFacadeException, InvalidPaymentStatusException, ParseException{
		
		validatePaymentStatus(status);
		
		PaymentResponseStatus responseStatus = null;
		
		try(DatabaseFacade db = new DatabaseFacade()){
			PaymentHelper pHelper = new PaymentHelper(db);
			MerchantHelper mHelper = new MerchantHelper(db);
			
			db.beginTransaction();
			logger.info("Requesting write locks on spg.payment - payment_id - {}", paymentId);
			Payment payment = pHelper.getLockedPayment(paymentId);
			
			if(!payment.getGatewayId().equals(GATEWAY_ID)){
				logger.error("invalid MERCHANT_TRANID - {} - gateway mismatch", status.getMerchantTransactionId());			
				throw new InvalidPaymentStatusException("not found MERCHANT_TRANID - " + status.getMerchantTransactionId());
			}
			
			logPaymentDetails(payment);
			
			logger.debug("new gateway transaction status - {} {}", status.getTransactionStatus(), status.getResponseCode());
			logger.debug("'{}' - '{}'", status.getMerchantTransactionId(), status.getTransactionId());
			String existingTransactionStatus = null;
			if(payment.getGatewayStatus() != null){
				existingTransactionStatus = payment.getGatewayStatus().getStatus();
			}
			
			merchant = mHelper.getMerchant(payment.getMerchantId());
			
			responseStatus = getPaymentResponseStatus(existingTransactionStatus, status.getTransactionStatus(), status.getResponseCode());
			if(responseStatus == null){
				logger.info("status update from gateway ignored");
				Status statusLog = getStatusForGatewayToSpg(payment.getPaymentId(),
						status.getTransactionStatus(), "Duplicated updates - ignored > " + status.getResponseCode());
				db.insert(statusLog);
				
				responseStatus = getSimpleResponseStatus(status.getTransactionStatus(), status.getResponseCode()); // Still getting it for Client Redirection
				
				if(payment.getMerchantClientStatus() == null){
					Status clientStatus = getStatusForSpgToMerchant(payment.getPaymentId(), 
							status.getResponseCode(), "Client update status");
					db.insert(clientStatus);
					payment.setMerchantClientStatusId(clientStatus.getStatusId());
					payment.setLastUpdatedBy("maybank-client-update");
					
					db.update(payment);
				}
			}else{
				Status statusLog = getStatusForGatewayToSpg(payment.getPaymentId(), 
						status.getTransactionStatus(), " New Status updated from '" + existingTransactionStatus + "', response_code = " + status.getResponseCode());
				db.insert(statusLog);
				payment.setGatewayStatusId(statusLog.getStatusId());
				
				handlePaymentStatusChanged(db, payment, responseStatus.getPaymentStage(), 
						responseStatus, status.getAuthorizeDate(), status.getTransactionId(), 
						status.getTransactionStatus(), status.getResponseCode(), false);
				
				payment.setLastUpdatedBy("maybank-client-update");
				db.update(payment);
			}
			
			db.commit();
			
			logger.debug("locks released, payment status is now '{}'", payment.getStatus().toString());
			this.payment = payment;
			return responseStatus;
		} 
	}

	protected void handlePaymentStatusChanged(DatabaseFacade db, Payment payment, PaymentStage stage, 
			PaymentResponseStatus responseStatus, String mbbAuthDate, String mbbTransactionId,
			String transactionStatus, String responseCode, boolean s2s) throws DatabaseFacadeException, ParseException{
		
		payment.setStatus(stage);
		
		if(!StringUtil.isEmpty(mbbTransactionId)){
			if(StringUtil.isEmpty(payment.getGatewayTransactionId())){
				payment.setGatewayTransactionId(mbbTransactionId);
			}else if(!mbbTransactionId.equals(payment.getGatewayTransactionId())){
				logger.error("Payment Id {} having mbb transaction id changed from '{}' to '{}'", 
						payment.getPaymentId(), payment.getGatewayTransactionId(), mbbTransactionId);
				payment.setGatewayTransactionId(mbbTransactionId);
			}
		}
		
		if(stage.isSuccess() && !StringUtil.isEmpty(mbbAuthDate)){
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			payment.setGatewayAuthorizationDatetime(sdf.parse(mbbAuthDate));
		}
		
		handlingServerToServerPaymentResponse(db, responseStatus, payment, transactionStatus, responseCode);
		
		if(!s2s){
			Status clientStatus = getStatusForSpgToMerchant(payment.getPaymentId(), transactionStatus, "Client update status - " + responseCode);
			db.insert(clientStatus);
			payment.setMerchantClientStatusId(clientStatus.getStatusId());	
		}
	}
	
	public static boolean handlingServerToServerPaymentResponse(DatabaseFacade db, 
			Payment payment, String transactionStatus) throws DatabaseFacadeException{
		
		PaymentResponseStatus responseStatus = getSimpleResponseStatus(transactionStatus, null);
		return handlingServerToServerPaymentResponse(db, responseStatus, payment, transactionStatus, null);
	}
	
	protected static boolean handlingServerToServerPaymentResponse(DatabaseFacade db, 
			PaymentResponseStatus responseStatus, Payment payment, String transactionStatus,
			String responseCode) throws DatabaseFacadeException{
		// S2S or Client both need to perform server notification (when status changed)
		Status s2sStatus = serverToServerPaymentResponse(responseStatus, payment, transactionStatus, responseCode);
		
		if(s2sStatus != null){
			db.insert(s2sStatus);
			payment.setMerchantS2sStatusId(s2sStatus.getStatusId());
			return true;
		}else{
			return false;
		}
	}

	public static Status serverToServerPaymentResponse(PaymentResponseStatus responseStatus, Payment payment, 
			String transactionStatus, String responseCode) {

		try(DatabaseFacade db = new DatabaseFacade()){
			ConfigHelper cHelper = new ConfigHelper(db);
		
			int timeout = cHelper.getServerToServerUpdateTimeoutInSeconds() * 1000;
			Merchant merchant = payment.getMerchant();
			
			FormBuilder form = generateResponseForm(merchant.getSignatureSecret(), 
					merchant.getServerToServerPaymentUpdateUrl(), 
					payment.getMerchantTransactionId(), payment.getAmount(), payment.getPaymentId(), 
					payment.getGatewayTransactionId(), responseCode, 
					responseStatus, payment.getGatewayAuthorizationDatetime(), responseStatus.getDefaultMessage());
			
			HttpUtil.post(form, timeout);
			
			if(payment.getStatus().getNotifiedStatus() != null){
				payment.setStatus(payment.getStatus().getNotifiedStatus());
			}
			
			return getStatusForSpgToMerchant(payment.getPaymentId(), 
					transactionStatus, "Server to server update status - " + responseCode);
			
		} catch (IOException | HttpException e) {
			logger.error("error s2s update - " + e.getMessage(), e);
			return null;
		}
	}
	
	protected PaymentDataStore getPaymentData(long paymentId, MaybankPaymentRequest request, 
			String publicWebPath){
		
		PaymentDataStore store = new PaymentDataStore();
		
		String mbbMerchantAccNo = getMerchantAccountNo();
		String mbbMerchantTransId = String.valueOf(paymentId);
		String mbbAmount = new DecimalFormat("0.00").format(request.getAmount());
		String mbbTransactionType = "3"; // 3 = Authorize
		String mbbCardNo = request.getCardNumber();
		String mbbCardExpMm = new SimpleDateFormat("MM").format(cardExpiry);
		String mbbCardExpYy = new SimpleDateFormat("yy").format(cardExpiry);
		String mbbCardCvc = request.getCardCvc();
		String mbbCardHolderName = request.getCardName();
		String mbbCardType = cardType.getCode();
		String mbbResponseType = "HTTP";
		String mbbReturnUrl = WebPath.MBB_INDIRECT.getWebFullPath(publicWebPath);
		String mbbTransactionDescription = request.getProductDescription();
		
		String mbbHashKey = getMerchantHashKey();
		
		String signatureFields = combineSignatureFieldsWithDelimiter(
				mbbHashKey, mbbMerchantAccNo, mbbMerchantTransId,
				mbbAmount, getMaskedPan(mbbCardNo), mbbCardExpMm, mbbCardExpYy,
				getMaskedCvc(mbbCardCvc));
		
		String mbbTransactionSignature = CryptographyUtil.toSha512HexadecimalLowercase(signatureFields);
	
		store.put("MERCHANT_ACC_NO", mbbMerchantAccNo);
		store.put("MERCHANT_TRANID", mbbMerchantTransId);
		store.put("AMOUNT", mbbAmount);
		store.put("TRANSACTION_TYPE", mbbTransactionType);
		store.put("CARD_NO", mbbCardNo, false);
		store.put("CARD_EXP_MM", mbbCardExpMm, false);
		store.put("CARD_EXP_YY", mbbCardExpYy, false);
		store.put("CARD_CVC", mbbCardCvc, false);
		store.put("CARD_HOLDER_NAME", mbbCardHolderName, false);
		store.put("CARD_TYPE", mbbCardType);
		store.put("TXN_SIGNATURE", mbbTransactionSignature);
		store.put("RESPONSE_TYPE", mbbResponseType);
		store.put("RETURN_URL", mbbReturnUrl);
		store.put("TXN_DESC", mbbTransactionDescription);
		
		return store;
	}
	
	private String getMerchantAccountNo(){
		if(cardType == MbbCardType.AMEX){
			return getGatewayConfig(MBB_AMEX_MERCHANT_ID_NAME);
		}else{
			return getGatewayConfig(MBB_MERCHANT_ID_NAME);
		}
	}
	
	private String getMerchantHashKey(){
		if(cardType == MbbCardType.AMEX){
			return getGatewayConfig(MBB_AMEX_HASH_KEY_NAME);
		}else{
			return getGatewayConfig(MBB_HASH_KEY_NAME);
		}
	}
	
	private MbbCardType getCardTypeUsed(Payment payment){
		
		Set<PaymentData> datas = payment.getPaymentDatas();
		for(PaymentData data : datas){
			if(data.getPaymentDataName().equals("CARD_TYPE")){
				return MbbCardType.getCardType(data.getPaymentDataValue());
			}
		}
		
		return null;
	}
	
	private String getMaskedPan(String cardNo){
		
		int first = 6;
		int last = 4;
		
		int pad = cardNo.length() - first - last;
		
		StringBuilder sb = new StringBuilder();
		sb.append(cardNo.substring(0, first));
		sb.append(StringUtil.pad(pad, 'x'));
		sb.append(cardNo.substring(cardNo.length() - last, cardNo.length()));
		
		return sb.toString();
	}
	
	private String getMaskedCvc(String cvc){
		StringBuilder sb = new StringBuilder();
		sb.append(StringUtil.pad(cvc.length() - 1, 'x'));
		sb.append(cvc.substring(cvc.length() - 1, cvc.length()));
		return sb.toString();
	}
	
	/**
	 * Return null indicates no update were required
	 */
	public PaymentResponseStatus getPaymentResponseStatus(String existingTransactionStatus, 
			String transactionStatus, String responseCode){
		if(existingTransactionStatus == null){
			return getSimpleResponseStatus(transactionStatus, responseCode);
		}else{
			if(existingTransactionStatus.equals(transactionStatus)){
				return null;	
			}else{
				PaymentResponseStatus status = getSimpleResponseStatus(transactionStatus, responseCode);
				if(status == PaymentResponseStatus.REQUEST_DUPLICATE){
					return null;
				}else{
					return status;
				}
			}
		}
	}
	
	protected static PaymentResponseStatus getSimpleResponseStatus(String transactionStatus, String responseCode){
		if("A".equals(transactionStatus) || "C".equals(transactionStatus) || "S".equals(transactionStatus)){
			return PaymentResponseStatus.SUCCESS;
		}else if("N".equals(transactionStatus)){
			return PaymentResponseStatus.WAITING;
		}else{
			if("4003".equals(responseCode)){
				return PaymentResponseStatus.REQUEST_DUPLICATE;
			}else{
				return PaymentResponseStatus.FAILED;
			}
		}
	}
	
	protected Map<String, String> getQueryPaymentData(Payment payment){
		
		cardType = getCardTypeUsed(payment);
		
		String merchantAccNo = getMerchantAccountNo();
		String hashKey = getMerchantHashKey();
		String merchantTransId = String.valueOf(payment.getPaymentId());
		String amount = new DecimalFormat("0.00").format(payment.getAmount());
		String transactionType = "1"; // 1 = Query
		String responseType = "PLAIN";
		
		String signatureFields = combineSignatureFieldsWithDelimiter(
				hashKey, merchantAccNo, merchantTransId, amount);
		
		String computeTansactionSignature = CryptographyUtil.toSha512HexadecimalLowercase(signatureFields);
		
		Map<String, String> map = new HashMap<>();
		map.put("MERCHANT_ACC_NO", merchantAccNo);
		map.put("MERCHANT_TRANID", merchantTransId);
		map.put("AMOUNT", amount);
		map.put("TRANSACTION_TYPE", transactionType);
		map.put("TXN_SIGNATURE", computeTansactionSignature);
		map.put("RESPONSE_TYPE", responseType);
		
		return map;
	}
	
	private Response getBypassGatewayResponse(Payment payment) throws InvalidPaymentStatusException{
		try(DatabaseFacade db = new DatabaseFacade()){
			
			ConfigHelper cHelper = new ConfigHelper(db);
			BypassGatewayStatus bypassStatus = cHelper.getSPGBypassGatewayStatus();
			
			try {
				MaybankPaymentStatusForBypass status = getBypassGatewayDummyStatus(payment, bypassStatus);
				PaymentResponseStatus responseStatus = handlePaymentStatus(status);
				FormBuilder form = clientPaymentResponse(responseStatus, this.payment, merchant, status.getTransactionStatus());
				return form.toResponse(true);
			} catch (DatabaseFacadeException | ParseException e) {
				logger.error("error " + e.getMessage(), e);
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("internal_server_error").build();
			}
		}
	}
	
	private MaybankPaymentStatusForBypass getBypassGatewayDummyStatus(Payment payment, BypassGatewayStatus bypassStatus){
		MaybankPaymentStatusForBypass status = new MaybankPaymentStatusForBypass();
		
		status.setMerchantTransactionId(String.valueOf(payment.getPaymentId()));
		status.setTransactionId("MBB" + String.valueOf(System.currentTimeMillis()));
		
		if(bypassStatus == BypassGatewayStatus.SUCCESS){
			status.setTransactionStatus("A");
			status.setResponseCode("0");
		}else if(bypassStatus == BypassGatewayStatus.FAILED){
			status.setTransactionStatus("F");
			status.setResponseCode("99");
		}else if(bypassStatus == BypassGatewayStatus.PENDING_AUTH){
			status.setTransactionStatus("A");
			status.setResponseCode("0");
		}
		
		logger.info(">>> TESTING -> {}", status.toString());
		return status;
	}
	
	@Override
	public Response test() {
		if(!isTestModeEnabled()){
			logger.info("not enabled by settings");
			return Response.status(Response.Status.NOT_FOUND).entity("not found").build();
		}else{
			try(DatabaseFacade db = new DatabaseFacade()){
				ConfigHelper cHelper = new ConfigHelper(db);
				
				Set<PaymentData> datas = new HashSet<>();
				PaymentData ct = new PaymentData();
				ct.setPaymentDataName("CARD_TYPE");
				ct.setPaymentDataValue(MbbCardType.VISA.getCode());
				
				Payment dummyPayment = new Payment();
				dummyPayment.setPaymentDatas(datas);
				dummyPayment.setPaymentId(9999);
				dummyPayment.setAmount(new BigDecimal("100.00"));
				
				int timeout = cHelper.getGatewayQueryTimeoutInSeconds() * 1000;
				String queryUrl = getGatewayConfig(MBB_QUERY_URL_NAME);
				FormBuilder form = new FormBuilder(queryUrl, getQueryPaymentData(dummyPayment));
				
				logger.info("Connection Timeout {}", timeout);
				logger.info("Connection URL {}", queryUrl);
				logger.info("Connection Data {}", form.toFormUrlEncoded());
				
				String response = HttpUtil.post(form, timeout);
				logger.info("Response {}", response);
				
				return Response.status(Response.Status.OK)
						.entity(response).build();
			} catch(IOException | HttpException e) {
				logger.error("test() exception - " + e.getMessage(), e);
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
						.entity(e.getMessage()).build();
			}			
		}
	}
}
