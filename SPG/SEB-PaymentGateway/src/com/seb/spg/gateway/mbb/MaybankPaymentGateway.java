package com.seb.spg.gateway.mbb;

import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.spg.consts.WebPathConstants;
import com.seb.spg.gateway.exception.InvalidPaymentRequestException;
import com.seb.spg.gateway.exception.InvalidPaymentStatusException;

@Path(WebPathConstants.MBB_ROOT)
public class MaybankPaymentGateway extends MaybankPaymentProcessor {

	private static final Logger logger = LogManager.getLogger(MaybankPaymentGateway.class);
	
	@POST
	@Path(WebPathConstants.SPG_PAYMENT)
	@Override
	public Response makePayment(@BeanParam MaybankPaymentRequest request) throws InvalidPaymentRequestException {
		logger.info("[MBB] makePayment() Request - Transaction ID: {}, Amount: {}", request.getTransactionId(), request.getAmount());
		logger.debug(request);
		return super.makePayment(request);
	}

	@POST
	@Path(WebPathConstants.SPG_QUERY)
	@Override
	public Response queryPayment(@BeanParam MaybankPaymentQuery query) throws InvalidPaymentRequestException {
		logger.info("[MBB] queryPayment() Request - Transaction ID: {}, Amount: {}", query.getTransactionId(), query.getAmount());
		logger.debug(query);
		return super.queryPayment(query);
	}

	@POST
	@Path(WebPathConstants.SPG_INDIRECT)
	@Override
	public Response clientPaymentStatusUpdate(@BeanParam MaybankPaymentStatus status) throws InvalidPaymentStatusException {
		logger.info("[MBB] clientPaymentStatusUpdate() Request - Merchant Transaction ID: {}, Transaction Status: {}, Response Code: {}", 
				status.getMerchantTransactionId(), status.getTransactionStatus(), status.getResponseCode());
		return super.clientPaymentStatusUpdate(status);
	}

	@Override
	public Response serverToServerPaymentStatusUpdate(@BeanParam MaybankPaymentStatus status)
			throws InvalidPaymentStatusException {
		throw new UnsupportedOperationException("Maybank S2S not supported");
	}

	@GET
	@Path(WebPathConstants.SPG_TEST)
	@Override
	public Response test() {
		logger.info("[MBB] test() request");
		return super.test();
	}
}
