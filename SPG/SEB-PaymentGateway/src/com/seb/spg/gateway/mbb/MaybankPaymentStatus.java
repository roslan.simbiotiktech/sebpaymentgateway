package com.seb.spg.gateway.mbb;

import javax.ws.rs.FormParam;

import com.seb.spg.gateway.PaymentStatus;

public class MaybankPaymentStatus extends PaymentStatus {

	@FormParam("TRANSACTION_ID")
	protected String transactionId;
	
	@FormParam("MERCHANT_ACC_NO")
	protected String merchantAccountNo;

	@FormParam("TXN_STATUS")
	protected String transactionStatus;

	@FormParam("TXN_SIGNATURE2")
	protected String transactionSignature;

	@FormParam("RESPONSE_CODE")
	protected String responseCode;

	@FormParam("RESPONSE_DESC")
	protected String responseDescription;

	@FormParam("MERCHANT_TRANID")
	protected String merchantTransactionId;

	@FormParam("AUTH_DATE")
	protected String authorizeDate;
	
	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getMerchantAccountNo() {
		return merchantAccountNo;
	}

	public void setMerchantAccountNo(String merchantAccountNo) {
		this.merchantAccountNo = merchantAccountNo;
	}

	public String getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public String getTransactionSignature() {
		return transactionSignature;
	}

	public void setTransactionSignature(String transactionSignature) {
		this.transactionSignature = transactionSignature;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDescription() {
		return responseDescription;
	}

	public void setResponseDescription(String responseDescription) {
		this.responseDescription = responseDescription;
	}

	public String getMerchantTransactionId() {
		return merchantTransactionId;
	}

	public void setMerchantTransactionId(String merchantTransactionId) {
		this.merchantTransactionId = merchantTransactionId;
	}

	public String getAuthorizeDate() {
		return authorizeDate;
	}

	public void setAuthorizeDate(String authorizeDate) {
		this.authorizeDate = authorizeDate;
	}

	@Override
	public String toString() {
		return "MaybankPaymentStatus [transactionId=" + transactionId + ", merchantAccountNo=" + merchantAccountNo
				+ ", transactionStatus=" + transactionStatus + ", transactionSignature=" + transactionSignature
				+ ", responseCode=" + responseCode + ", responseDescription=" + responseDescription
				+ ", merchantTransactionId=" + merchantTransactionId + ", authorizeDate=" + authorizeDate + "]";
	}
}
