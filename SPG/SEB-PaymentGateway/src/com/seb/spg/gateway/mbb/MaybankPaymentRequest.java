package com.seb.spg.gateway.mbb;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.FormParam;

import com.seb.spg.gateway.PaymentRequest;
import com.seb.spg.util.StringUtil;

public class MaybankPaymentRequest extends PaymentRequest {

	@NotNull(message = "card_type may not be null")
	@Size(min = 4, max = 6, message = "card_type must be 'VISA', 'MASTER' or 'AMEX'")
	@FormParam("card_type")
	protected String cardType;
	
	@NotNull(message = "card_number may not be null")
	@Size(min = 15, max = 16, message = "card_type must be between 15 to 16 numeric characters")
	@FormParam("card_number")
	protected String cardNumber;
	
	@NotNull(message = "card_name may not be null")
	@Size(min = 1, max = 50, message = "card_name must be between 1 to 50 characters")
	@FormParam("card_name")
	protected String cardName;

	@NotNull(message = "card_expiry may not be null")
	@Size(min = 6, max = 6, message = "card_expiry must in 'yyyyMM' format")
	@FormParam("card_expiry")
	protected String cardExpiry;
	
	@NotNull(message = "card_cvc may not be null")
	@Size(min = 3, max = 4, message = "card_cvc must be between 3 to 4 numeric characters")
	@FormParam("card_cvc")
	protected String cardCvc;

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getCardName() {
		return cardName;
	}

	public void setCardName(String cardName) {
		this.cardName = cardName;
	}

	public String getCardExpiry() {
		return cardExpiry;
	}

	public void setCardExpiry(String cardExpiry) {
		this.cardExpiry = cardExpiry;
	}

	public String getCardCvc() {
		return cardCvc;
	}

	public void setCardCvc(String cardCvc) {
		this.cardCvc = cardCvc;
	}

	public String getSigningFields(){
		
		DecimalFormat df = new DecimalFormat("0.00");
		
		List<String> fields = new ArrayList<>();
		fields.add(merchantId);
		fields.add(transactionId);
		fields.add(df.format(amount));
		fields.add(productDescription);
		fields.add(customerUsername);
		fields.add(customerEmail);
		fields.add(returnUrl);
		fields.add(cardType);
		fields.add(cardNumber);
		fields.add(cardName);
		fields.add(cardExpiry);
		fields.add(cardCvc);

		return combineFields(fields);
	}
	
	/*
	 * Caution
	 * Only Log the following
	 *   Card Type
	 *   Card Number (Last 4 digit)
	 */
	@Override
	public String toString() {
		return "MaybankPaymentRequest [cardType=" + cardType + ", cardNumber=" + StringUtil.leftMasked(cardNumber, 4) + "]";
	}
}
