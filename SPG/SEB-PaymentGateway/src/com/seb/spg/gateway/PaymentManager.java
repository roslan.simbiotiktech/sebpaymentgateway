package com.seb.spg.gateway;

import javax.ws.rs.core.Response;

import com.seb.spg.gateway.exception.InvalidPaymentStatusException;

public interface PaymentManager<S extends PaymentStatus> {

	public Response serverToServerPaymentStatusUpdate(S status) throws InvalidPaymentStatusException;
	
	public Response clientPaymentStatusUpdate(S status) throws InvalidPaymentStatusException;
	
}
