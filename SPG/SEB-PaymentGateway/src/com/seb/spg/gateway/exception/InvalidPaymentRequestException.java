package com.seb.spg.gateway.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Provider
public class InvalidPaymentRequestException extends GeneralGatewayException implements ExceptionMapper<InvalidPaymentRequestException> {

	private static final Logger logger = LogManager.getLogger(InvalidPaymentRequestException.class);
	
	private static final long serialVersionUID = 6858877394228024972L;

	public InvalidPaymentRequestException(){ }
	
	public InvalidPaymentRequestException(String message){
		super(message);
	}
	
	@Override
	public Response toResponse(InvalidPaymentRequestException ex) {
		logger.error(ex.getMessage(), ex);
		return Response.status(Response.Status.BAD_REQUEST)
                .entity(ex.getMessage()).type("text/plain").build();
	}
}
