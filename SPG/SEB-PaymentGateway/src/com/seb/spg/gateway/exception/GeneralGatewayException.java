package com.seb.spg.gateway.exception;

public abstract class GeneralGatewayException extends Exception {

	private static final long serialVersionUID = -3406607531124595385L;
	
	public GeneralGatewayException() {
		super();
	}
	
	public GeneralGatewayException(String message) {
		super(message);
	}
}
