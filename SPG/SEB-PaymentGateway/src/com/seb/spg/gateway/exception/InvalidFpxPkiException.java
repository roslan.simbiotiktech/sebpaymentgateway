package com.seb.spg.gateway.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Provider
public class InvalidFpxPkiException extends GeneralGatewayException implements ExceptionMapper<InvalidFpxPkiException> {

	private static final Logger logger = LogManager.getLogger(InvalidFpxPkiException.class);
	
	private static final long serialVersionUID = 6858877394228024972L;

	private String errorCode;
	
	public InvalidFpxPkiException(){ }
	
	public InvalidFpxPkiException(String message, String errorCode){
		super(message);
		this.errorCode = errorCode;
	}
	
	public String getErrorCode(){
		return errorCode;
	}
	
	@Override
	public Response toResponse(InvalidFpxPkiException ex) {
		logger.error(ex.getMessage() + " - " + ex.getErrorCode(), ex);
		return Response.status(Response.Status.BAD_REQUEST)
                .entity(ex.getMessage()).type("text/plain").build();
	}

}
