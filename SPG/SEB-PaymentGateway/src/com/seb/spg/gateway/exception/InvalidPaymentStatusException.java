package com.seb.spg.gateway.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Provider
public class InvalidPaymentStatusException extends GeneralGatewayException implements ExceptionMapper<InvalidPaymentStatusException> {

	private static final long serialVersionUID = -6310851394554526041L;
	
	private static final Logger logger = LogManager.getLogger(InvalidPaymentStatusException.class);

	public InvalidPaymentStatusException(){ }
	
	public InvalidPaymentStatusException(String message){
		super(message);
	}
	
	@Override
	public Response toResponse(InvalidPaymentStatusException ex) {
		logger.error(ex.getMessage(), ex);
		return Response.status(Response.Status.BAD_REQUEST)
                .entity(ex.getMessage()).type("text/plain").build();
	}
}