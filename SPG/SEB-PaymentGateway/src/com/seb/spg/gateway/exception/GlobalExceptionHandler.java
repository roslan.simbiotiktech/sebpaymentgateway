package com.seb.spg.gateway.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Provider
public class GlobalExceptionHandler implements ExceptionMapper<Throwable> {

	private static final Logger logger = LogManager.getLogger(GlobalExceptionHandler.class);

	@Override
	public Response toResponse(Throwable ex) {
		logger.error(ex.getMessage(), ex);
		
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(ex.getMessage()).type("text/plain").build();
	}

}
