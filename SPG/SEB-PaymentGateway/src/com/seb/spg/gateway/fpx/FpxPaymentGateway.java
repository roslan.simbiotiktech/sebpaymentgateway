package com.seb.spg.gateway.fpx;

import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.spg.consts.WebPathConstants;
import com.seb.spg.gateway.exception.InvalidPaymentRequestException;
import com.seb.spg.gateway.exception.InvalidPaymentStatusException;

@Path(WebPathConstants.FPX_ROOT)
public class FpxPaymentGateway extends FpxPaymentProcessor {

	private static final Logger logger = LogManager.getLogger(FpxPaymentGateway.class);

	@POST
	@Path(WebPathConstants.SPG_PAYMENT)
	@Override
	public Response makePayment(@BeanParam FpxPaymentRequest request) throws InvalidPaymentRequestException {
		logger.info("[FPX] makePayment() Request - Transaction ID: {}, Amount: {}", request.getTransactionId(), request.getAmount());
		logger.debug(request);
		return super.makePayment(request);
	}
	
	@POST
	@Path(WebPathConstants.SPG_QUERY)
	@Override
	public Response queryPayment(@BeanParam FpxPaymentQuery query) throws InvalidPaymentRequestException {
		logger.info("[FPX] queryPayment() Request - Transaction ID: {}, Amount: {}", query.getTransactionId(), query.getAmount());
		logger.debug(query);
		return super.queryPayment(query);
	}

	@POST
	@Path(WebPathConstants.SPG_DIRECT)
	@Override
	public Response serverToServerPaymentStatusUpdate(@BeanParam FpxPaymentStatus status) throws InvalidPaymentStatusException {
		logger.info("[FPX] serverToServerPaymentStatusUpdate() Request - Seller Order No: {}, Debit Auth Code: {}", status.getSellerOrderNo(), status.getDebitAuthCode());
		logger.debug(status);
		return super.serverToServerPaymentStatusUpdate(status);
	}

	@POST
	@Path(WebPathConstants.SPG_INDIRECT)
	@Override
	public Response clientPaymentStatusUpdate(@BeanParam FpxPaymentStatus status) throws InvalidPaymentStatusException {
		logger.info("[FPX] clientPaymentStatusUpdate() Request - Seller Order No: {}, Debit Auth Code: {}", status.getSellerOrderNo(), status.getDebitAuthCode());
		logger.debug(status);
		return super.clientPaymentStatusUpdate(status);
	}
	
	@GET
	@Override
	@Path(WebPathConstants.SPG_BANK)
	public Response getBank(@QueryParam("id") int bankId){
		logger.info("[FPX] getBank() Request - {}", bankId);
		return super.getBank(bankId);
	}
	
	@GET
	@Override
	@Path(WebPathConstants.SPG_BANKS)
	public Response getBanks(){
		logger.info("[FPX] getBanks() Request");
		return super.getBanks();
	}
	
	@GET
	@Override
	@Produces("image/png")
	@Path(WebPathConstants.SPG_BANKLOGO)
	public Response getBankLogo(@QueryParam("id") int bankId){
		// logger.info("[FPX] getBankLogo() Request - {}", bankId);
		return super.getBankLogo(bankId);
	}
	
	@GET
	@Path(WebPathConstants.SPG_TEST)
	@Override
	public Response test() {
		logger.info("[FPX] test() request");
		return super.test();
	}

	@GET
	@Path("test2")
	public Response test2(@QueryParam("file") String privateKeyFile){
		logger.info("[FPX] test2() request");
		return super.test2(privateKeyFile);
	}
	
	@GET
	@Path("test3")
	public Response test3(@QueryParam("file") String fpxCert){
		logger.info("[FPX] test3() request");
		return super.test3(fpxCert);
	}
	
	@GET
	@Path("test4")
	public Response test4(){
		logger.info("[FPX] test4() request");
		return super.test4();
	}
}
