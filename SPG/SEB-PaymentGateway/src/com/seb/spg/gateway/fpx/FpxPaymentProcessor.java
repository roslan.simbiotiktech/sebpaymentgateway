package com.seb.spg.gateway.fpx;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.anteater.library.api.utility.StringUtil;
import com.anteater.library.jpa.DatabaseFacade;
import com.anteater.library.jpa.DatabaseFacadeException;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.seb.spg.consts.BypassGatewayStatus;
import com.seb.spg.consts.FpxBankType;
import com.seb.spg.consts.PaymentResponseStatus;
import com.seb.spg.consts.PaymentStage;
import com.seb.spg.consts.StatusDirection;
import com.seb.spg.consts.WebPath;
import com.seb.spg.gateway.FormBuilder;
import com.seb.spg.gateway.PaymentDataStore;
import com.seb.spg.gateway.PaymentGateway;
import com.seb.spg.gateway.exception.InvalidFpxPkiException;
import com.seb.spg.gateway.exception.InvalidPaymentRequestException;
import com.seb.spg.gateway.exception.InvalidPaymentStatusException;
import com.seb.spg.jpa.dao.FpxBank;
import com.seb.spg.jpa.dao.GatewayConfig;
import com.seb.spg.jpa.dao.Merchant;
import com.seb.spg.jpa.dao.Payment;
import com.seb.spg.jpa.dao.PaymentData;
import com.seb.spg.jpa.dao.Status;
import com.seb.spg.jpa.helper.ConfigHelper;
import com.seb.spg.jpa.helper.GatewayHelper;
import com.seb.spg.jpa.helper.MerchantHelper;
import com.seb.spg.jpa.helper.PaymentHelper;
import com.seb.spg.util.HttpUtil;

public abstract class FpxPaymentProcessor extends PaymentGateway<FpxPaymentRequest, FpxPaymentQuery, FpxPaymentStatus> {

	private static final Logger logger = LogManager.getLogger(FpxPaymentProcessor.class);
	
	public static final String CURRENCY = "MYR";
	
	public static final String GATEWAY_ID = "FPX";
	
	/**
	 * This is FPX Public Certificate. E.g. pxprod_current.cer
	 * - use to Verify Data transmitted from FPX
	 * - use to check expiry date
	 */
	public static final String FPX_PUBLIC_KEY_NAME = "PUBLIC_KEY";
	
	/**
	 * This is own Private Key File. E.g. <ExhangeId>.key
	 * - use to Sign data sent to FPX
	 */
	public static final String FPX_PRIVATE_KEY_NAME = "PRIVATE_KEY";
	
	/**
	 * This is Exchange Certificate from CSR Request. E.g. <ExhangeId>.cer
	 * - use to check expiry date
	 */
	public static final String FPX_PRIVATE_KEY_IDENTITY_CERT_NAME = "PRIVATE_KEY_IDENTITY_CERT";  
	
	public static final String FPX_SUBMIT_URL_NAME = "SUBMIT_URL";
	public static final String FPX_QUERY_URL_NAME = "QUERY_URL";
	public static final String FPX_BANK_LISTING_URL_NAME = "BANK_LISTING_URL";
	
	public static final String FPX_MERCHANT_ID_KEY_NAME = "MERCHANT_ID";
	public static final String FPX_EXCHANGE_ID_KEY_NAME = "EXCHANGE_ID";
	public static final String FPX_SELLER_BANK_CODE_KEY_NAME = "SELLER_BANK_CODE";
	public static final String FPX_VERSION_KEY_NAME = "VERSION";
	public static final String FPX_CSR_COUNTRY_NAME_NAME = "CSR_COUNTRY_NAME";
	public static final String FPX_CSR_STATE_OR_PROVINCE_NAME_NAME = "CSR_STATE_OR_PROVINCE_NAME";
	public static final String FPX_CSR_LOCALITY_NAME_NAME = "CSR_LOCALITY_NAME";
	public static final String FPX_CSR_ORGANIZATION_NAME_NAME = "CSR_ORGANIZATION_NAME";
	public static final String FPX_CSR_ORGANIZATION_UNIT_NAME = "CSR_ORGANIZATION_UNIT";
	public static final String FPX_CSR_COMMON_NAME_NAME = "CSR_COMMON_NAME";
	public static final String FPX_CSR_EMAIL_NAME = "CSR_EMAIL";
	public static final String FPX_CERTIFICATE_EXPIRY_WARNING_DAYS_BEFORE_NAME = "CERTIFICATE_EXPIRY_WARNING_DAYS_BEFORE";
	public static final String FPX_CERTIFICATE_EXPIRY_WARNING_EMAIL_NAME = "CERTIFICATE_EXPIRY_WARNING_EMAIL";
		
	protected FpxBank bank;
	protected long paymentId;
	protected BigDecimal amount;
	protected Payment payment;
	
	@Override
	protected String getGatewayId(){
		return GATEWAY_ID;
	}

	@Override
	protected void validatePaymentRequest(FpxPaymentRequest request) throws InvalidPaymentRequestException {
		super.validatePaymentRequest(request);
		
		try(DatabaseFacade db = new DatabaseFacade()){
			GatewayHelper gHelper = new GatewayHelper(db);
			
			FpxBank bank = gHelper.getFpxBank(request.getBankId());
			
			if(bank == null){
				logger.error("Fpx Bank with id {} not found", request.getBankId());
				throw new InvalidPaymentRequestException("bank not found");
			}else if(!bank.isEnabled()){
				logger.error("Fpx Bank with id {} was disabled", request.getBankId());
				throw new InvalidPaymentRequestException("bank was suspended");
			}
			
			this.bank = bank;
		}
	}
	
	@Override
	protected void validatePaymentStatus(FpxPaymentStatus status) throws InvalidPaymentStatusException {
		
		if(StringUtil.isEmpty(status.getSellerExOrderNo())){
			logger.error("fpx_sellerExOrderNo is empty");
			throw new InvalidPaymentStatusException("fpx_sellerExOrderNo is empty");
		}
		
		if(status.shouldVerify()){
			String signatureFields = combineSignatureFieldsWithDelimiter('|', status.getBuyerBankBranch(), status.getBuyerBankId(),
					status.getBuyerIban(), status.getBuyerId(), status.getBuyerName(), status.getCreditAuthCode(),
					status.getCreditAuthNo(), status.getDebitAuthCode(), status.getDebitAuthNo(),
					status.getFpxTxnId(), status.getFpxTxnTime(), status.getMakerName(), status.getMsgToken(),
					status.getMsgType(), status.getSellerExId(), status.getSellerExOrderNo(), status.getSellerId(),
					status.getSellerOrderNo(), status.getSellerTxnTime(), status.getTxnAmount(),
					status.getTxnCurrency());
			
			try {
				FpxPkiImplementation.verifyData(signatureFields, status.getCheckSum());
			} catch (InvalidFpxPkiException e) {
				logger.error("Checksum mismatch - " + e.getMessage(), e);
				logger.error(" Request Checksum : {}", status.getCheckSum());
				logger.error(" Compute Signature : {}", signatureFields);
				
				throw new InvalidPaymentStatusException("checksum verify error");
			}	
		}
		
		try {
			paymentId = Long.parseLong(status.getSellerExOrderNo());
		} catch (NumberFormatException e) {
			logger.error("invalid fpx_sellerExOrderNo - " + status.getSellerExOrderNo() + ", " + e.getMessage(), e);			
			throw new InvalidPaymentStatusException("invalid fpx_sellerExOrderNo - " + status.getSellerExOrderNo());
		}
		
		try {
			amount = new BigDecimal(status.getTxnAmount());
		} catch (NumberFormatException e) {
			logger.error("invalid fpx_txnAmount - " + status.getTxnAmount() + ", " + e.getMessage(), e);			
			throw new InvalidPaymentStatusException("invalid fpx_txnAmount - " + status.getTxnAmount());
		}
	}
	
	@Override
	public Response makePayment(FpxPaymentRequest request) throws InvalidPaymentRequestException {
		
		validatePaymentRequest(request);
		
		Payment payment = preparePayment(request);
		
		try(DatabaseFacade db = new DatabaseFacade()){
			
			PaymentHelper pHelper = new PaymentHelper(db);// check payment id dupl
			Payment existingPayment = pHelper.getPayment(merchant.getMerchantId(), request.getTransactionId());
			if(existingPayment != null){
				logger.error("Merchant {} already have an existing transaction with Id {}", merchant.getMerchantName(), 
						request.getTransactionId());
				
				PaymentResponseStatus status = PaymentResponseStatus.REQUEST_DUPLICATE;
				String message = "Duplicate transaction_id " + request.getTransactionId();
				
				Status newStatus = new Status();
				newStatus.setPaymentId(existingPayment.getPaymentId());
				newStatus.setDirection(StatusDirection.SPG_TO_MERCHANT);
				newStatus.setStatus(status.toString());
				newStatus.setRemark(message);
				
				db.beginTransaction();
				db.insert(newStatus);
				db.commit();
				
				FormBuilder form = generateResponseForm(request, status, merchant, message);
				return form.toResponse(true);
			}else{
				db.beginTransaction();
				db.insert(payment);
				logger.info("Payment Id '{}' generated for merchant transaction '{}'", payment.getPaymentId(), request.getTransactionId());
				
				PaymentDataStore paymentDataStore = getPaymentData(payment.getPaymentId(), request);
				List<PaymentData> paymentDatas = paymentDataStore.toPersistentList(payment.getPaymentId());
				
				for(PaymentData paymentData : paymentDatas){
					db.insert(paymentData);
				}
				
				logger.debug("inserted {} payment data", paymentDatas.size());
				
				db.commit();
				
				if(isBypassGatewayRounting()){
					logger.info(">>>> TESTING MODE STARTED <<<<");
					return getBypassGatewayResponse(payment);
				}else{
					String submitUrl = getGatewayConfig(FPX_SUBMIT_URL_NAME);
					logger.debug("Payment committed successfully, now preparing Auto-Submit form of Action {}", submitUrl);
					FormBuilder form = new FormBuilder(submitUrl, paymentDataStore);
					return form.toResponse(isTestModeEnabled());
				}
			}
		}catch (DatabaseFacadeException | InvalidFpxPkiException e) {			
			logger.error("error makePayment() - " + e.getMessage(), e);
			
			PaymentResponseStatus status = PaymentResponseStatus.SERVER_ERROR;
			String message = "Internal server error";
			
			FormBuilder form = generateResponseForm(request, status, merchant, message);
			return form.toResponse(true);
		} catch (InvalidPaymentStatusException e) {
			logger.error("error makePayment() bypass gateway - " + e.getMessage(), e);
			
			PaymentResponseStatus status = PaymentResponseStatus.REQUEST_ERROR;
			String message = e.getMessage();
			
			FormBuilder form = generateResponseForm(request, status, merchant, message);
			return form.toResponse(true);
		} 
	}

	@Override
	public Response queryPayment(FpxPaymentQuery query) throws InvalidPaymentRequestException {

		validatePaymentQuery(query);
		
		try(DatabaseFacade db = new DatabaseFacade()){
			PaymentHelper pHelper = new PaymentHelper(db);
			ConfigHelper cHelper = new ConfigHelper(db);
			
			db.beginTransaction();
			logger.info("Requesting write locks on spg.payment - transaction_id - {}", query.getTransactionId());
			Payment payment = pHelper.getLockedPayment(merchant.getMerchantId(), query.getTransactionId());
			
			if(payment == null){
				logger.error("invalid transaction_id - {} - not found", query.getTransactionId());			
				throw new InvalidPaymentStatusException("not found transaction_id - " + query.getTransactionId());
			}else if(!payment.getGatewayId().equals(GATEWAY_ID)){
				logger.error("invalid transaction_id - {} - gateway mismatch", query.getTransactionId());			
				throw new InvalidPaymentStatusException("not found transaction_id - " + query.getTransactionId());
			}else if(payment.getAmount().compareTo(query.getAmount()) != 0){
				logger.error("invalid amount - {} - amount mismatch - {}", query.getAmount(), payment.getAmount());			
				throw new InvalidPaymentStatusException("invalid amount - " + query.getAmount());
			}
			
			logPaymentDetails(payment);
			
			FormBuilder responseForm = null;
			
			if(payment.getStatus() == PaymentStage.NEW || payment.getStatus() == PaymentStage.PENDING_AUTH){
				
				logger.debug("Query request for {} payment, sending query message to FPX", payment.getStatus());
				
				int timeout = cHelper.getGatewayQueryTimeoutInSeconds() * 1000;
				String queryUrl = getGatewayConfig(FPX_QUERY_URL_NAME);
				FormBuilder form = new FormBuilder(queryUrl, getQueryPaymentData(payment));
				
				String response = HttpUtil.post(form, timeout);

				logger.debug(response);
				
				if (StringUtil.isEmpty(response) || 
						response.equals("msgfromfpx= PROSESSING ERROR") || response.equals("ERROR")) {
					logger.error("error response from FPX - unable to get new status");
					
					Status statusLog = getStatusForSpgToMerchant(payment.getPaymentId(), 
							"NULL", "Merchant query payment details (FPX Response Error)");
					
					db.insert(statusLog);
					
					responseForm = clientPaymentResponse(
							PaymentResponseStatus.getResponseStatusByStage(payment.getStatus()), payment, merchant, null);
				} else {
					Map<String, String> responseMap = getPostResponseMap(response);
					String debitAuthCode = responseMap.get("fpx_debitAuthCode");
					
					if(StringUtil.isEmpty(debitAuthCode)){
						logger.error("fpx_debitAuthCode not found in fpx AE response - unable to get new status");
						Status statusLog = getStatusForSpgToMerchant(payment.getPaymentId(), 
								"NULL", "Merchant query payment details (FPX Response Error - fpx_debitAuthCode not found)");
						
						db.insert(statusLog);
						
						responseForm = clientPaymentResponse(
								PaymentResponseStatus.getResponseStatusByStage(payment.getStatus()), payment, merchant, null);
					}else{
						PaymentResponseStatus responseStatus = getSimpleResponseStatus(debitAuthCode);
						PaymentStage newPaymentStatus = responseStatus.getPaymentStage();
						
						Status incomingStatusLog = getStatusForGatewayToSpg(payment.getPaymentId(), 
								debitAuthCode, "SPG Call AE Message to query payment details");
						Status outgoingStatusLog = getStatusForSpgToMerchant(payment.getPaymentId(), 
								debitAuthCode, "Merchant query payment details");
						
						db.insert(incomingStatusLog);
						db.insert(outgoingStatusLog);
						
						if(newPaymentStatus == payment.getStatus()){
							logger.debug("Payment is still pending");
						}else{
							
							payment.setGatewayStatusId(incomingStatusLog.getStatusId());
							
							handlePaymentStatusChanged(db, payment, newPaymentStatus, 
									true, responseStatus, responseMap.get("fpx_fpxTxnTime"), 
									responseMap.get("fpx_fpxTxnId"), debitAuthCode);
							
							payment.setLastUpdatedBy("fpx-AE-message-query");
							
							db.update(payment);
						}
						
						responseForm = clientPaymentResponse(responseStatus, payment, merchant, debitAuthCode);
					}
				}				
			}else{
				Status statusLog = getStatusForSpgToMerchant(payment.getPaymentId(), 
						payment.getGatewayStatus().getStatus(), "Merchant query payment details");
				db.insert(statusLog);
				
				PaymentResponseStatus responseStatus = getSimpleResponseStatus(payment.getGatewayStatus().getStatus());
				responseForm = clientPaymentResponse(responseStatus, payment, merchant, payment.getGatewayStatus().getStatus());
			}
			
			db.commit();
			logger.debug("locks released, payment status is now '{}'", payment.getStatus().toString());
			
			return Response.ok(responseForm.toFormUrlEncoded(), MediaType.APPLICATION_FORM_URLENCODED_TYPE).build();
			
		} catch (InvalidPaymentStatusException e) {
			logger.error("Invalid pamynet status - " + e.getMessage(), e);
			return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (DatabaseFacadeException | 
				IOException | HttpException | InvalidFpxPkiException | 
				ParseException e) {
			logger.error("error providing query payment response - " + e.getMessage(), e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("internal_server_error").build();
		} 
	}

	@Override
	public Response serverToServerPaymentStatusUpdate(FpxPaymentStatus status) throws InvalidPaymentStatusException {
				
		try {
			handlePaymentStatus(status, true);
			return Response.status(Response.Status.OK).entity("OK").build();
		} catch (DatabaseFacadeException | ParseException e) {
			logger.error("error " + e.getMessage(), e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("internal_server_error").build();
		}
	}

	@Override
	public Response clientPaymentStatusUpdate(FpxPaymentStatus status) throws InvalidPaymentStatusException {
		
		try {
			PaymentResponseStatus responseStatus = handlePaymentStatus(status, false);
			FormBuilder form = clientPaymentResponse(responseStatus, payment, merchant, status.getDebitAuthCode());
			return form.toResponse(true);
		} catch (DatabaseFacadeException | ParseException e) {
			logger.error("error " + e.getMessage(), e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("internal_server_error").build();
		}
	}
	
	public Response getBank(int bankId){
		try(DatabaseFacade db = new DatabaseFacade()){
			GatewayHelper gHelper = new GatewayHelper(db);
			FpxBank bank = gHelper.getFpxBank(bankId);
			
			if(bank != null && bank.isEnabled()){
				JsonObject json = getFpxBankJson(bank);
				return Response.ok(json.toString(), MediaType.APPLICATION_JSON_TYPE).build();
			}else{
				return Response.status(Response.Status.NOT_FOUND).entity("bank not found").build();
			}
		}
	}
	
	public Response getBanks(){

		try(DatabaseFacade db = new DatabaseFacade()){
			GatewayHelper gHelper = new GatewayHelper(db);
			ConfigHelper cHelper = new ConfigHelper(db);
			
			String publicWebPath = cHelper.getPublicWebPath();
			String publicBankLogoPath = WebPath.FPX_BANK_LOGO.getWebFullPath(publicWebPath);
			
			Map<FpxBankType, List<FpxBank>> banks = gHelper.getActiveFpxBanks()
					.stream().collect(Collectors.groupingBy(FpxBank::getBankType));
			
			JsonObject json = new JsonObject();
			
			for(FpxBankType type : FpxBankType.values()){
				
				JsonArray arr = new JsonArray();
				json.add(type.toString(), arr);
				
				List<FpxBank> list = banks.get(type);
				if(list != null && !list.isEmpty()){
					for(FpxBank b : list){
						
						JsonObject obj = getFpxBankJson(b);
						obj.addProperty("logo", publicBankLogoPath + "?id=" + b.getBankId());
						
						arr.add(obj);
					}
				}
			}
			
			return Response.ok(json.toString(), MediaType.APPLICATION_JSON_TYPE).build();
		}
	}
	
	public Response getBankLogo(int bankId){

		try(DatabaseFacade db = new DatabaseFacade()){
			GatewayHelper gHelper = new GatewayHelper(db);
			FpxBank bank = gHelper.getFpxBank(bankId);
			
			if(bank != null && bank.isEnabled() && bank.getLogo() != null && bank.getLogo().length != 0){
				return Response.ok(bank.getLogo()).build();	
			}else{
				return Response.status(Response.Status.NOT_FOUND).entity("bank not found").build();
			}
		}
	}
	
	private JsonObject getFpxBankJson(FpxBank b){
		JsonObject obj = new JsonObject();
		obj.addProperty("id", b.getBankId());
		obj.addProperty("name", b.getBankName() == null ? b.getBankFpxId() : b.getBankName());
		obj.addProperty("type", b.getBankType().toString());
		obj.addProperty("enabled", b.isEnabled());
		return obj;
	}
	
	protected PaymentResponseStatus handlePaymentStatus(FpxPaymentStatus status, boolean s2s) throws DatabaseFacadeException, InvalidPaymentStatusException, ParseException{
		
		validatePaymentStatus(status);
		
		PaymentResponseStatus responseStatus = null;
		
		try(DatabaseFacade db = new DatabaseFacade()){
			PaymentHelper pHelper = new PaymentHelper(db);
			MerchantHelper mHelper = new MerchantHelper(db);
			
			db.beginTransaction();
			logger.info("Requesting write locks on spg.payment - payment_id - {}", paymentId);
			Payment payment = pHelper.getLockedPayment(paymentId);
			
			if(payment == null){
				logger.error("invalid fpx_sellerExOrderNo - {} - not found", status.getSellerExOrderNo());			
				throw new InvalidPaymentStatusException("not found fpx_sellerExOrderNo - " + status.getSellerExOrderNo());
			}else if(!payment.getGatewayId().equals(GATEWAY_ID)){
				logger.error("invalid fpx_sellerExOrderNo - {} - gateway mismatch", status.getSellerExOrderNo());			
				throw new InvalidPaymentStatusException("not found fpx_sellerExOrderNo - " + status.getSellerExOrderNo());
			}else if(payment.getAmount().compareTo(amount) < 0){
				logger.error("invalid fpx_txnAmount - {} - amount mismatch - {}", status.getTxnAmount(), payment.getAmount());			
				throw new InvalidPaymentStatusException("invalid fpx_txnAmount - " + status.getTxnAmount());
			}else if(!status.getTxnCurrency().equalsIgnoreCase(CURRENCY)){
				logger.error("invalid fpx_txnCurrency - {} - amount mismatch - {}", status.getTxnCurrency(), CURRENCY);			
				throw new InvalidPaymentStatusException("invalid fpx_txnCurrency - " + status.getTxnCurrency());
			}
			
			merchant = mHelper.getMerchant(payment.getMerchantId());
			logPaymentDetails(payment);
			
			logger.debug("new gateway debit authorization code - {}", status.getDebitAuthCode());
			logger.debug("'{}' - '{}'", status.getSellerExOrderNo(), status.getFpxTxnId());
			
			String existingDebitAuthCode = null;
			if(payment.getGatewayStatus() != null){
				existingDebitAuthCode = payment.getGatewayStatus().getStatus();
			}
			
			responseStatus = getPaymentResponseStatus(existingDebitAuthCode, status.getDebitAuthCode());
			if(responseStatus == null){
				logger.info("status update from gateway ignored");
				Status statusLog = getStatusForGatewayToSpg(status, getLoggingPrefix(s2s) + " Duplicated updates - ignored");
				db.insert(statusLog);
				
				if(!s2s){
					responseStatus = getSimpleResponseStatus(status.getDebitAuthCode());
					
					if(payment.getMerchantClientStatus() == null){
						Status clientStatus = getStatusForSpgToMerchant(payment.getPaymentId(), 
								status.getDebitAuthCode(), "Client update status");
						db.insert(clientStatus);
						payment.setMerchantClientStatusId(clientStatus.getStatusId());
						payment.setLastUpdatedBy("fpx-client-update");
						
						db.update(payment);
					}
				}else{
					if(payment.getMerchantS2sStatus() == null){
						Status s2sStatus = getStatusForSpgToMerchant(payment.getPaymentId(), 
								status.getDebitAuthCode(), "Server to server update status");
						db.insert(s2sStatus);
						payment.setMerchantS2sStatusId(s2sStatus.getStatusId());
						payment.setLastUpdatedBy("fpx-server-to-server-update");
						
						db.update(payment);
					}
				}
			}else{
				logger.info("detected response status {} from debit auth code '{}' (existing {})", 
						responseStatus.toString(), status.getDebitAuthCode(), existingDebitAuthCode);
				
				Status statusLog = getStatusForGatewayToSpg(status, getLoggingPrefix(s2s) + " New Status updated from '" + existingDebitAuthCode + "'");
				db.insert(statusLog);
				payment.setGatewayStatusId(statusLog.getStatusId());
				
				handlePaymentStatusChanged(db, payment, responseStatus.getPaymentStage(), 
						s2s, responseStatus, status.getFpxTxnTime(), status.getFpxTxnId(), status.getDebitAuthCode());
				
				if(s2s){
					payment.setLastUpdatedBy("fpx-server-to-server-update");
				}else{					
					payment.setLastUpdatedBy("fpx-client-update");
				}
				
				db.update(payment);
			}
			
			db.commit();
			
			logger.debug("locks released, payment status is now '{}'", payment.getStatus().toString());
			this.payment = payment;
			return responseStatus;
		} 
	}
	
	protected void handlePaymentStatusChanged(DatabaseFacade db, Payment payment, PaymentStage stage, boolean s2s, 
			PaymentResponseStatus responseStatus, String fpxTxnTime, String fpxTxnId, String debitAuthCode) throws DatabaseFacadeException, ParseException{
		
		payment.setStatus(stage);
		
		if(!StringUtil.isEmpty(fpxTxnId)){
			if(StringUtil.isEmpty(payment.getGatewayTransactionId())){
				payment.setGatewayTransactionId(fpxTxnId);
			}else if(!fpxTxnId.equals(payment.getGatewayTransactionId())){
				logger.error("Payment Id {} having fpx transaction id changed from '{}' to '{}'", 
						payment.getPaymentId(), payment.getGatewayTransactionId(), fpxTxnId);
				payment.setGatewayTransactionId(fpxTxnId);
			}
		}
		
		payment.setGatewayTransactionId(fpxTxnId);
		if(!StringUtil.isEmpty(fpxTxnTime)){
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
			payment.setGatewayAuthorizationDatetime(sdf.parse(fpxTxnTime));
		}
		
		handlingServerToServerPaymentResponse(db, responseStatus, payment, debitAuthCode);
				
		if(!s2s){
			Status clientStatus = getStatusForSpgToMerchant(payment.getPaymentId(), debitAuthCode, "Client update status");
			db.insert(clientStatus);
			payment.setMerchantClientStatusId(clientStatus.getStatusId());
		}
	}
	
	public static boolean handlingServerToServerPaymentResponse(DatabaseFacade db, 
			Payment payment, String debitAuthCode) throws DatabaseFacadeException{
		
		PaymentResponseStatus responseStatus = getSimpleResponseStatus(debitAuthCode);
		return handlingServerToServerPaymentResponse(db, responseStatus, payment, debitAuthCode);
	}
	
	protected static boolean handlingServerToServerPaymentResponse(DatabaseFacade db, 
			PaymentResponseStatus responseStatus, Payment payment, String debitAuthCode) throws DatabaseFacadeException{
		// S2S or Client both need to perform server notification (when status changed)
		Status s2sStatus = serverToServerPaymentResponse(responseStatus, payment, debitAuthCode);
		
		if(s2sStatus != null){
			db.insert(s2sStatus);
			payment.setMerchantS2sStatusId(s2sStatus.getStatusId());
			return true;
		}else{
			return false;
		}
	}
	
	public static Status serverToServerPaymentResponse(PaymentResponseStatus responseStatus, Payment payment, String debitAuthCode) {

		try(DatabaseFacade db = new DatabaseFacade()){
			ConfigHelper cHelper = new ConfigHelper(db);
		
			int timeout = cHelper.getServerToServerUpdateTimeoutInSeconds() * 1000;
			Merchant merchant = payment.getMerchant();
			
			FormBuilder form = generateResponseForm(merchant.getSignatureSecret(), 
					merchant.getServerToServerPaymentUpdateUrl(), 
					payment.getMerchantTransactionId(), payment.getAmount(), payment.getPaymentId(), 
					payment.getGatewayTransactionId(), debitAuthCode,  
					responseStatus, payment.getGatewayAuthorizationDatetime(), 
					responseStatus.getDefaultMessage());
			
			HttpUtil.post(form, timeout);
			
			if(payment.getStatus().getNotifiedStatus() != null){
				payment.setStatus(payment.getStatus().getNotifiedStatus());
			}
			
			return getStatusForSpgToMerchant(payment.getPaymentId(), debitAuthCode, "Server to server update status");
			
		} catch (IOException | HttpException e) {
			logger.error("error s2s update - " + e.getMessage(), e);
			return null;
		}
	}

	protected Map<String, String> getQueryPaymentData(Payment payment) throws InvalidFpxPkiException{
		
		Set<PaymentData> datas = payment.getPaymentDatas();
		Map<String, String> map = new HashMap<>();
		
		for(PaymentData data : datas){
			map.put(data.getPaymentDataName(), data.getPaymentDataValue());
		}
		
		map.put("fpx_msgType", "AE");
		
		String signatureFields = combineSignatureFieldsWithDelimiter(
				'|',
				map.get("fpx_buyerAccNo"), 
				map.get("fpx_buyerBankBranch"), 
				map.get("fpx_buyerBankId"), 
				map.get("fpx_buyerEmail"), 
				map.get("fpx_buyerIban"), 
				map.get("fpx_buyerId"), 
				map.get("fpx_buyerName"), 
				map.get("fpx_makerName"), 
				map.get("fpx_msgToken"), 
				map.get("fpx_msgType"), 
				map.get("fpx_productDesc"), 
				map.get("fpx_sellerBankCode"), 
				map.get("fpx_sellerExId"), 
				map.get("fpx_sellerExOrderNo"), 
				map.get("fpx_sellerId"), 
				map.get("fpx_sellerOrderNo"), 
				map.get("fpx_sellerTxnTime"), 
				map.get("fpx_txnAmount"), 
				map.get("fpx_txnCurrency"), 
				map.get("fpx_version"));
		
		String signatureChecksum = FpxPkiImplementation.signData(signatureFields);
		
		map.put("fpx_checkSum", signatureChecksum);
		return map;
	}
	
	protected PaymentDataStore getPaymentData(long paymentId, FpxPaymentRequest request) throws InvalidFpxPkiException{
		
		PaymentDataStore store = new PaymentDataStore();
		
		String msgType = "AR";	
		String msgToken = bank.getBankType().getToken();
		String sellerExId = getGatewayConfig(FPX_EXCHANGE_ID_KEY_NAME);
		String sellerExOrderNo = String.valueOf(paymentId);
		String sellerTxnTime = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		String sellerOrderNo = request.getTransactionId();
		String sellerId =  getGatewayConfig(FPX_MERCHANT_ID_KEY_NAME);
		String sellerBankCode = getGatewayConfig(FPX_SELLER_BANK_CODE_KEY_NAME);
		String txnCurrency = CURRENCY;
		String txnAmount = new DecimalFormat("0.00").format(request.getAmount());
		String buyerEmail = request.getCustomerEmail();
		String buyerName = request.getCustomerUsername();
		String buyerBankId = bank.getBankFpxId();
		String buyerBankBranch =  null;
		String buyerAccNo = null;
		String buyerId = null;
		String makerName = null;
		String buyerIban = null;
		String productDesc = request.getProductDescription();
		String version = getGatewayConfig(FPX_VERSION_KEY_NAME);
		
		String signatureFields = combineSignatureFieldsWithDelimiter(
				'|',
				buyerAccNo, buyerBankBranch, buyerBankId, 
				buyerEmail, buyerIban, buyerId, buyerName, 
				makerName, msgToken, msgType, productDesc, 
				sellerBankCode, sellerExId, sellerExOrderNo, 
				sellerId, sellerOrderNo, sellerTxnTime, 
				txnAmount, txnCurrency, version);
		
		String signatureChecksum = FpxPkiImplementation.signData(signatureFields);
		
		store.put("fpx_msgType", msgType);
		store.put("fpx_msgToken", msgToken);
		store.put("fpx_sellerExId", sellerExId);
		store.put("fpx_sellerExOrderNo", sellerExOrderNo);
		store.put("fpx_sellerTxnTime", sellerTxnTime);
		store.put("fpx_sellerOrderNo", sellerOrderNo);
		store.put("fpx_sellerBankCode", sellerBankCode);
		store.put("fpx_txnCurrency", txnCurrency);
		store.put("fpx_txnAmount", txnAmount);
		store.put("fpx_buyerEmail", buyerEmail);
		store.put("fpx_buyerName", buyerName);
		store.put("fpx_buyerBankId", buyerBankId);
		store.put("fpx_buyerBankBranch", buyerBankBranch);
		store.put("fpx_buyerAccNo", buyerAccNo);
		store.put("fpx_buyerId", buyerId);
		store.put("fpx_makerName", makerName);
		store.put("fpx_buyerIban", buyerIban);
		store.put("fpx_productDesc", productDesc);
		store.put("fpx_version", version);
		store.put("fpx_sellerId", sellerId);	
		store.put("fpx_checkSum", signatureChecksum);
		
		return store;
	}
	
	protected Status getStatusForGatewayToSpg(FpxPaymentStatus fpxStatus, String remark){
		Status status = new Status();
		
		status.setStatus(fpxStatus.getDebitAuthCode());
		status.setDirection(StatusDirection.GATEWAT_TO_SPG);
		status.setPaymentId(paymentId);
		status.setRemark(remark);
		
		return status;
	}	
	
	/**
	 * Return null indicates no update were required
	 */
	public static PaymentResponseStatus getPaymentResponseStatus(String existingDebitAuthCode, String debitAuthCode){
		if(existingDebitAuthCode == null){
			return getSimpleResponseStatus(debitAuthCode);
		}else{
			if(existingDebitAuthCode.equals(debitAuthCode)){
				return null;	
			}else{
				PaymentResponseStatus status = getSimpleResponseStatus(debitAuthCode);
				if(status == PaymentResponseStatus.REQUEST_DUPLICATE){
					return null;
				}else{
					return status;
				}
			}
		}
	}
	
	protected static PaymentResponseStatus getSimpleResponseStatus(String debitAuthCode){
		if("00".equals(debitAuthCode)){
			return PaymentResponseStatus.SUCCESS;
		}else if("09".equals(debitAuthCode) || "76".equals(debitAuthCode)){
			return PaymentResponseStatus.WAITING;
		}else if("99".equals(debitAuthCode)){
			return PaymentResponseStatus.PENDING_AUTH;
		}else if("72".equals(debitAuthCode) || "XO".equals(debitAuthCode)){
			return PaymentResponseStatus.REQUEST_DUPLICATE;
		}else{
			return PaymentResponseStatus.FAILED;
		}
	}
	
	private String getLoggingPrefix(boolean s2s){
		if(s2s){
			return "[s2s]";
		}else{
			return "[client]";
		}
	}
	
	private Response getBypassGatewayResponse(Payment payment) throws InvalidPaymentStatusException{
		try(DatabaseFacade db = new DatabaseFacade()){
			
			ConfigHelper cHelper = new ConfigHelper(db);
			BypassGatewayStatus bypassStatus = cHelper.getSPGBypassGatewayStatus();
			
			try {
				FpxPaymentStatusForBypass status = getBypassGatewayDummyStatus(payment, bypassStatus);
				PaymentResponseStatus responseStatus = handlePaymentStatus(status, false);
				FormBuilder form = clientPaymentResponse(responseStatus, this.payment, merchant, status.getDebitAuthCode());
				return form.toResponse(true);
			} catch (DatabaseFacadeException | ParseException e) {
				logger.error("error " + e.getMessage(), e);
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("internal_server_error").build();
			}
		}
	}
	
	private FpxPaymentStatusForBypass getBypassGatewayDummyStatus(Payment payment, BypassGatewayStatus bypassStatus){
		FpxPaymentStatusForBypass status = new FpxPaymentStatusForBypass();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		
		status.setSellerExOrderNo(String.valueOf(payment.getPaymentId()));
		status.setTxnAmount(payment.getAmount().toString());
		status.setTxnCurrency(CURRENCY);
		status.setFpxTxnTime(sdf.format(new Date()));
		status.setFpxTxnId("FPX" + String.valueOf(System.currentTimeMillis()));
		
		if(bypassStatus == BypassGatewayStatus.SUCCESS){
			status.setDebitAuthCode("00");
		}else if(bypassStatus == BypassGatewayStatus.FAILED){
			status.setDebitAuthCode("01");
		}else if(bypassStatus == BypassGatewayStatus.PENDING_AUTH){
			status.setDebitAuthCode("99");
		}
		
		logger.info(">>> TESTING -> {}", status.toString());
		return status;
	}
	
	@Override
	public Response test() {
		if(!isTestModeEnabled()){
			logger.info("not enabled by settings");
			return Response.status(Response.Status.NOT_FOUND).entity("not found").build();
		}else{
			FpxManagementDelegate delegate = new FpxManagementDelegate();
			delegate.performCertificateExpiryChecking();
			int count = delegate.performBankUpdate();
			
			return Response.status(Response.Status.OK)
					.entity("Total " + count + " banks downloaded from FPX").build();		
		}
	}
	
	public Response test2(String privateKeyFile) {
		if(!isTestModeEnabled()){
			logger.info("not enabled by settings");
			return Response.status(Response.Status.NOT_FOUND).entity("not found").build();
		}else{
			String file = "/tmp/" + privateKeyFile;
			try {
				File f = new File(file);
				byte[] encoded = Base64.getEncoder().encode(FileUtils.readFileToByteArray(f));
				String s = new String(encoded, StandardCharsets.US_ASCII);
				
				try(DatabaseFacade db = new DatabaseFacade()){
					
					GatewayConfig config = new GatewayConfig();
					config.setGatewayId(FpxPaymentGateway.GATEWAY_ID);
					config.setConfigKey(FpxPaymentGateway.FPX_PRIVATE_KEY_NAME);
					config.setConfigValue(s);
					config.setCreatedBy("*");
					
					logger.info("[FPX] test2 - " + s);
					
					db.beginTransaction();
					db.insertOrUpdate(config);
					db.commit();
					
					return Response.status(Response.Status.OK)
							.entity("Private Key '" + file + "' loaded").build();	
				} 
			} catch (IOException | DatabaseFacadeException e) {
				logger.error("error test2() - " + e.getMessage(), e);
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
			}	
		}
	}
	
	public Response test3(String fpxCert) {
		if(!isTestModeEnabled()){
			logger.info("not enabled by settings");
			return Response.status(Response.Status.NOT_FOUND).entity("not found").build();
		}else{
			String file = "/tmp/" + fpxCert;
			try {
				File f = new File(file);
				byte[] encoded = Base64.getEncoder().encode(FileUtils.readFileToByteArray(f));
				String s = new String(encoded, StandardCharsets.US_ASCII);
				
				try(DatabaseFacade db = new DatabaseFacade()){
					
					GatewayConfig config = new GatewayConfig();
					config.setGatewayId(FpxPaymentGateway.GATEWAY_ID);
					config.setConfigKey(FpxPaymentGateway.FPX_PUBLIC_KEY_NAME);
					config.setConfigValue(s);
					config.setCreatedBy("*");
					
					logger.info("[FPX] test3 - " + s);
					
					db.beginTransaction();
					db.insertOrUpdate(config);
					db.commit();
					
					return Response.status(Response.Status.OK)
							.entity("FPX Public Key '" + file + "' loaded").build();	
				} 
			} catch (IOException | DatabaseFacadeException e) {
				logger.error("error test3() - " + e.getMessage(), e);
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
			}	
		}
	}
	
	public Response test4() {
		if(!isTestModeEnabled()){
			logger.info("not enabled by settings");
			return Response.status(Response.Status.NOT_FOUND).entity("not found").build();
		}else{
			
			try {
				String csr = FpxPkiImplementation.generateCsr();
				FileUtils.writeByteArrayToFile(new File("/tmp/output.csr"), csr.getBytes());
				
				return Response.status(Response.Status.OK)
						.entity("CSR 'output.csr' generated at /tmp").build();	
			} catch (IOException | InvalidFpxPkiException e) {
				logger.error("error test4() - " + e.getMessage(), e);
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
			}
		}
	}
}
