package com.seb.spg.gateway.fpx;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.FormParam;

import com.seb.spg.gateway.PaymentRequest;

public class FpxPaymentRequest extends PaymentRequest {

	@NotNull(message = "bank_id may not be null")
	@FormParam("bank_id")
	protected Integer bankId;

	public Integer getBankId() {
		return bankId;
	}

	public void setBankId(Integer bankId) {
		this.bankId = bankId;
	}

	public String getSigningFields(){
		
		DecimalFormat df = new DecimalFormat("0.00");
		
		List<String> fields = new ArrayList<>();
		fields.add(merchantId);
		fields.add(transactionId);
		fields.add(df.format(amount));
		fields.add(productDescription);
		fields.add(customerUsername);
		fields.add(customerEmail);
		fields.add(returnUrl);
		fields.add(String.valueOf(bankId));

		return combineFields(fields);
	}

	@Override
	public String toString() {
		return "FpxPaymentRequest [bankId=" + bankId + ", merchantId=" + merchantId + ", transactionId=" + transactionId
				+ ", amount=" + amount + ", productDescription=" + productDescription + ", customerUsername="
				+ customerUsername + ", customerEmail=" + customerEmail + ", returnUrl=" + returnUrl + ", signature="
				+ signature + "]";
	}
}
