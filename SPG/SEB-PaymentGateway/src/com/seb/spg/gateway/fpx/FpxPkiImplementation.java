package com.seb.spg.gateway.fpx;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import javax.security.auth.x500.X500Principal;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.pkcs.PKCS10CertificationRequestBuilder;
import org.bouncycastle.pkcs.jcajce.JcaPKCS10CertificationRequestBuilder;

import com.anteater.library.api.utility.DateUtil;
import com.anteater.library.api.utility.StringUtil;
import com.anteater.library.jpa.DatabaseFacade;
import com.seb.spg.gateway.exception.InvalidFpxPkiException;
import com.seb.spg.jpa.helper.GatewayHelper;
import com.seb.spg.util.CryptographyUtil;

public class FpxPkiImplementation {
	
	static {
		Security.addProvider(new BouncyCastleProvider());
	}
	
	private static final Logger logger = LogManager.getLogger(FpxPkiImplementation.class);

	public static final String SIGN_ALGORITHM = "SHA1withRSA";
	public static final String CSR_SIGN_ALGORITHM = "SHA256withRSA";
	public static final String SIGN_PROVIDER = "BC";
	public static final String CERTIFICATE_FACTORY = "X.509";
	
	public static String signData(String data) throws InvalidFpxPkiException {

		try {
			PrivateKey privateKey = getPrivateKey();
			
			Signature signature = Signature.getInstance(SIGN_ALGORITHM, SIGN_PROVIDER);
			signature.initSign(privateKey);

			signature.update(data.getBytes());
			byte[] signatureBytes = signature.sign();
			return CryptographyUtil.toHex(signatureBytes);
			
		} catch (NoSuchAlgorithmException | NoSuchProviderException | SignatureException | InvalidKeyException ex) {
			logger.error("error signing data", ex);
			throw new InvalidFpxPkiException("error signing data", ex.getMessage());
		} 
	}

	public static boolean verifyData(String computeChecksum, String checksumFromMessage) throws InvalidFpxPkiException {

		boolean result = false;
		try {
			Signature verifier = Signature.getInstance(SIGN_ALGORITHM, SIGN_PROVIDER);
			X509Certificate publicKey = getPublicKey();
			verifier.initVerify(publicKey);
			verifier.update(computeChecksum.getBytes());
			
			byte[] checksumFromMessageBytes = CryptographyUtil.fromHex(checksumFromMessage);
			
			result = verifier.verify(checksumFromMessageBytes);
			return result; 
			
		} catch (DecoderException | SignatureException | InvalidKeyException | NoSuchAlgorithmException | NoSuchProviderException ex) {
			logger.error("error verify data - " + ex.getMessage(), ex);
			throw new InvalidFpxPkiException("error verify data", ex.getMessage());
		} finally {
			if(!result){
				logger.error("Compute Checksum '{}'", computeChecksum);
				logger.error("Checksum from Message '{}'", checksumFromMessage);
			}
		}
	}
	
	public static String generateCsr() throws InvalidFpxPkiException{
		KeyPair privateKeyPair = getPrivateKeyPair();
		
		PrivateKey privateKey = privateKeyPair.getPrivate();
		PublicKey publicKey = privateKeyPair.getPublic();
		
		try(DatabaseFacade db = new DatabaseFacade()){
			GatewayHelper gHelper = new GatewayHelper(db);
			Map<String, String> configs = gHelper.getGatewayConfigValueLike(FpxPaymentProcessor.GATEWAY_ID, "CSR%");
			
			String subjectDescription = getCsrSubject(configs);
			
			logger.info("Subject Description {}", subjectDescription);
			
			X500Principal subject = new X500Principal(subjectDescription);
			ContentSigner signGen = new JcaContentSignerBuilder(SIGN_ALGORITHM).build(privateKey);
			
			PKCS10CertificationRequestBuilder builder = new JcaPKCS10CertificationRequestBuilder(subject, publicKey);
			PKCS10CertificationRequest csr = builder.build(signGen);
			
			try(ByteArrayOutputStream baos = new ByteArrayOutputStream()){
				try(OutputStreamWriter output = new OutputStreamWriter(baos)){
					try(JcaPEMWriter pem = new JcaPEMWriter(output)){		
						pem.writeObject(csr);
						pem.flush();
						output.flush();
						return new String(baos.toByteArray(), "UTF-8");
					}
				} 
			}
		} catch (IOException | OperatorCreationException ex) {
			logger.error("unable to generate CSR from private key - " + ex.getMessage(), ex);
			throw new InvalidFpxPkiException("unable to generate CSR", ex.getMessage());
		}
	}

	private static KeyPair getPrivateKeyPair() throws InvalidFpxPkiException {
		
		try(DatabaseFacade db = new DatabaseFacade()){
			GatewayHelper gHelper = new GatewayHelper(db);
			String b64Certificate = gHelper.getGatewayConfigValue(FpxPaymentProcessor.GATEWAY_ID, FpxPaymentProcessor.FPX_PRIVATE_KEY_NAME);
			
			if(StringUtil.isEmpty(b64Certificate)){
				throw new InvalidFpxPkiException("error signing data", "Private Key empty or not found");
			}else{
				byte[] privateKeyBytes = Base64.getDecoder().decode(b64Certificate);
				ByteArrayInputStream bais = new ByteArrayInputStream(privateKeyBytes);
				
				try(Reader isReader = new InputStreamReader(bais)){

					try(PEMParser pemParser = new PEMParser(isReader)){
						
						PEMKeyPair keyPair = (PEMKeyPair)pemParser.readObject();
						
						JcaPEMKeyConverter converter = new JcaPEMKeyConverter();
						return converter.getKeyPair(keyPair);
					}
				}
			}
		}catch (IOException e) {
			logger.error(e.getMessage(), e);
			throw new InvalidFpxPkiException("error signing data", e.getMessage());
		}
	}

	private static PrivateKey getPrivateKey() throws InvalidFpxPkiException {
		return getPrivateKeyPair().getPrivate();
	}
	
	public static X509Certificate getPrivateKeyIdentityCertificate() throws InvalidFpxPkiException{
		try(DatabaseFacade db = new DatabaseFacade()){
			GatewayHelper gHelper = new GatewayHelper(db);
			String b64Certificate = gHelper.getGatewayConfigValue(FpxPaymentProcessor.GATEWAY_ID, FpxPaymentProcessor.FPX_PRIVATE_KEY_IDENTITY_CERT_NAME);
			
			if(StringUtil.isEmpty(b64Certificate)){
				return null;
			}else{
				byte[] privateKeyBytes = Base64.getDecoder().decode(b64Certificate);
				
				try(ByteArrayInputStream bais = new ByteArrayInputStream(privateKeyBytes)){
					CertificateFactory certificateFactory = CertificateFactory.getInstance(CERTIFICATE_FACTORY);
					Collection<? extends Certificate> list = certificateFactory.generateCertificates(bais);
					return extractPrivateKeyIdentityCertificate(list);
				} 
			}
		}catch (IOException | CertificateException e) {
			logger.error(e.getMessage(), e);
			throw new InvalidFpxPkiException("error getting x509 certificate", e.getMessage());
		}
	}
	
	private static X509Certificate extractPrivateKeyIdentityCertificate(Collection<? extends Certificate> certificates){
		
		X509Certificate first = null;
		for(Certificate c : certificates){
			
			if(c instanceof X509Certificate){
				
				X509Certificate x509 = (X509Certificate)c;
				
				if(first == null) first = x509;
				
				if(x509.getSubjectDN() != null && x509.getSubjectDN().getName() != null){
					if(x509.getSubjectDN().getName().toLowerCase().indexOf("sesco") != -1){
						return x509;
					}
				}
			}
		}
		
		return first;
	}
	
	public static X509Certificate getPublicCertificate() throws InvalidFpxPkiException{
		try(DatabaseFacade db = new DatabaseFacade()){
			GatewayHelper gHelper = new GatewayHelper(db);
			String b64Certificate = gHelper.getGatewayConfigValue(FpxPaymentProcessor.GATEWAY_ID, FpxPaymentProcessor.FPX_PUBLIC_KEY_NAME);
			
			if(StringUtil.isEmpty(b64Certificate)){
				throw new InvalidFpxPkiException("error verify data", "Public Key empty or not found");
			}else{
				byte[] privateKeyBytes = Base64.getDecoder().decode(b64Certificate);
				
				try(ByteArrayInputStream bais = new ByteArrayInputStream(privateKeyBytes)){
					CertificateFactory certificateFactory = CertificateFactory.getInstance(CERTIFICATE_FACTORY);
					return (X509Certificate) certificateFactory.generateCertificate(bais);
				} 
			}
		}catch (IOException | CertificateException e) {
			logger.error(e.getMessage(), e);
			throw new InvalidFpxPkiException("error getting x509 certificate", e.getMessage());
		}
	}
	
	private static X509Certificate getPublicKey() throws InvalidFpxPkiException{
		
		X509Certificate x509Cert = getPublicCertificate();
		
		Date today = DateUtil.getStartDate(new Date());

		Calendar expiryDate = Calendar.getInstance();
		expiryDate.setTime(DateUtil.getStartDate(x509Cert.getNotAfter()));
		expiryDate.add(Calendar.DAY_OF_MONTH, -1);
		
		Date expiry = expiryDate.getTime();

		logger.debug("Certificate expiry date {}", expiry);
		
		if(expiry.compareTo(today) >= 0){
			return x509Cert;
		}else{
			logger.error("FPX Certificate expired {}, {}", x509Cert.getNotAfter(), expiry);
			throw new InvalidFpxPkiException("error verify data", "FPX Certificate expired");
		}
	}
	
	private static String getCsrSubject(Map<String, String> configs){
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("C=").append(configs.get(FpxPaymentProcessor.FPX_CSR_COUNTRY_NAME_NAME)).append(", ");
		sb.append("ST=").append(configs.get(FpxPaymentProcessor.FPX_CSR_STATE_OR_PROVINCE_NAME_NAME)).append(", ");
		sb.append("L=").append(configs.get(FpxPaymentProcessor.FPX_CSR_LOCALITY_NAME_NAME)).append(", ");
		sb.append("O=").append(configs.get(FpxPaymentProcessor.FPX_CSR_ORGANIZATION_NAME_NAME)).append(", ");
		sb.append("OU=").append(configs.get(FpxPaymentProcessor.FPX_CSR_ORGANIZATION_UNIT_NAME)).append(", ");
		sb.append("CN=").append(configs.get(FpxPaymentProcessor.FPX_CSR_COMMON_NAME_NAME)).append(", ");
		sb.append("EMAILADDRESS=").append(configs.get(FpxPaymentProcessor.FPX_CSR_EMAIL_NAME));
		
		return sb.toString();
	}
	
	public static void main(String [] args) throws IOException{
		
		File file = new File("C:\\Users\\PeirHwa\\Desktop\\SEB\\Fpx_Certs\\fpxprod_current.cer");
	    String encoded = Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(file));
	    System.out.println(encoded);
		
	}
}
