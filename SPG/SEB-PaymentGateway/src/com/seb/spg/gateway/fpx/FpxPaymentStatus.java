package com.seb.spg.gateway.fpx;

import javax.ws.rs.FormParam;

import com.seb.spg.gateway.PaymentStatus;

public class FpxPaymentStatus extends PaymentStatus {

	@FormParam("fpx_sellerOrderNo")
	protected String sellerOrderNo;

	@FormParam("fpx_checkSum")
	protected String checkSum;

	@FormParam("fpx_buyerBankBranch")
	protected String buyerBankBranch;

	@FormParam("fpx_buyerBankId")
	protected String buyerBankId;

	@FormParam("fpx_buyerIban")
	protected String buyerIban;

	@FormParam("fpx_buyerId")
	protected String buyerId;

	@FormParam("fpx_buyerName")
	protected String buyerName;

	@FormParam("fpx_creditAuthCode")
	protected String creditAuthCode;

	@FormParam("fpx_creditAuthNo")
	protected String creditAuthNo;

	@FormParam("fpx_debitAuthCode")
	protected String debitAuthCode;

	@FormParam("fpx_debitAuthNo")
	protected String debitAuthNo;

	@FormParam("fpx_fpxTxnId")
	protected String fpxTxnId;

	@FormParam("fpx_fpxTxnTime")
	protected String fpxTxnTime;

	@FormParam("fpx_makerName")
	protected String makerName;

	@FormParam("fpx_msgToken")
	protected String msgToken;

	@FormParam("fpx_msgType")
	protected String msgType;

	@FormParam("fpx_sellerExId")
	protected String sellerExId;

	@FormParam("fpx_sellerExOrderNo")
	protected String sellerExOrderNo;

	@FormParam("fpx_sellerId")
	protected String sellerId;

	@FormParam("fpx_sellerTxnTime")
	protected String sellerTxnTime;

	@FormParam("fpx_txnAmount")
	protected String txnAmount;

	@FormParam("fpx_txnCurrency")
	protected String txnCurrency;

	public String getSellerOrderNo() {
		return sellerOrderNo;
	}

	public void setSellerOrderNo(String sellerOrderNo) {
		this.sellerOrderNo = sellerOrderNo;
	}

	public String getCheckSum() {
		return checkSum;
	}

	public void setCheckSum(String checkSum) {
		this.checkSum = checkSum;
	}

	public String getBuyerBankBranch() {
		return buyerBankBranch;
	}

	public void setBuyerBankBranch(String buyerBankBranch) {
		this.buyerBankBranch = buyerBankBranch;
	}

	public String getBuyerBankId() {
		return buyerBankId;
	}

	public void setBuyerBankId(String buyerBankId) {
		this.buyerBankId = buyerBankId;
	}

	public String getBuyerIban() {
		return buyerIban;
	}

	public void setBuyerIban(String buyerIban) {
		this.buyerIban = buyerIban;
	}

	public String getBuyerId() {
		return buyerId;
	}

	public void setBuyerId(String buyerId) {
		this.buyerId = buyerId;
	}

	public String getBuyerName() {
		return buyerName;
	}

	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}

	public String getCreditAuthCode() {
		return creditAuthCode;
	}

	public void setCreditAuthCode(String creditAuthCode) {
		this.creditAuthCode = creditAuthCode;
	}

	public String getCreditAuthNo() {
		return creditAuthNo;
	}

	public void setCreditAuthNo(String creditAuthNo) {
		this.creditAuthNo = creditAuthNo;
	}

	public String getDebitAuthCode() {
		return debitAuthCode;
	}

	public void setDebitAuthCode(String debitAuthCode) {
		this.debitAuthCode = debitAuthCode;
	}

	public String getDebitAuthNo() {
		return debitAuthNo;
	}

	public void setDebitAuthNo(String debitAuthNo) {
		this.debitAuthNo = debitAuthNo;
	}

	public String getFpxTxnId() {
		return fpxTxnId;
	}

	public void setFpxTxnId(String fpxTxnId) {
		this.fpxTxnId = fpxTxnId;
	}

	public String getFpxTxnTime() {
		return fpxTxnTime;
	}

	public void setFpxTxnTime(String fpxTxnTime) {
		this.fpxTxnTime = fpxTxnTime;
	}

	public String getMakerName() {
		return makerName;
	}

	public void setMakerName(String makerName) {
		this.makerName = makerName;
	}

	public String getMsgToken() {
		return msgToken;
	}

	public void setMsgToken(String msgToken) {
		this.msgToken = msgToken;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getSellerExId() {
		return sellerExId;
	}

	public void setSellerExId(String sellerExId) {
		this.sellerExId = sellerExId;
	}

	public String getSellerExOrderNo() {
		return sellerExOrderNo;
	}

	public void setSellerExOrderNo(String sellerExOrderNo) {
		this.sellerExOrderNo = sellerExOrderNo;
	}

	public String getSellerId() {
		return sellerId;
	}

	public void setSellerId(String sellerId) {
		this.sellerId = sellerId;
	}

	public String getSellerTxnTime() {
		return sellerTxnTime;
	}

	public void setSellerTxnTime(String sellerTxnTime) {
		this.sellerTxnTime = sellerTxnTime;
	}

	public String getTxnAmount() {
		return txnAmount;
	}

	public void setTxnAmount(String txnAmount) {
		this.txnAmount = txnAmount;
	}

	public String getTxnCurrency() {
		return txnCurrency;
	}

	public void setTxnCurrency(String txnCurrency) {
		this.txnCurrency = txnCurrency;
	}

	@Override
	public String toString() {
		return "FpxPaymentStatus [sellerOrderNo=" + sellerOrderNo + ", checkSum=" + checkSum + ", buyerBankBranch="
				+ buyerBankBranch + ", buyerBankId=" + buyerBankId + ", buyerIban=" + buyerIban + ", buyerId=" + buyerId
				+ ", buyerName=" + buyerName + ", creditAuthCode=" + creditAuthCode + ", creditAuthNo=" + creditAuthNo
				+ ", debitAuthCode=" + debitAuthCode + ", debitAuthNo=" + debitAuthNo + ", fpxTxnId=" + fpxTxnId
				+ ", fpxTxnTime=" + fpxTxnTime + ", makerName=" + makerName + ", msgToken=" + msgToken + ", msgType="
				+ msgType + ", sellerExId=" + sellerExId + ", sellerExOrderNo=" + sellerExOrderNo + ", sellerId="
				+ sellerId + ", sellerTxnTime=" + sellerTxnTime + ", txnAmount=" + txnAmount + ", txnCurrency="
				+ txnCurrency + "]";
	}
}
