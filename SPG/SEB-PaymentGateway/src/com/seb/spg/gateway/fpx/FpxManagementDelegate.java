package com.seb.spg.gateway.fpx;

import java.io.IOException;
import java.security.cert.X509Certificate;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.anteater.library.api.utility.StringUtil;
import com.anteater.library.delegate.BaseDelegate;
import com.anteater.library.jpa.DatabaseFacade;
import com.anteater.library.jpa.DatabaseFacadeException;
import com.seb.spg.consts.FpxBankType;
import com.seb.spg.consts.PaymentStage;
import com.seb.spg.gateway.FormBuilder;
import com.seb.spg.gateway.PaymentGateway;
import com.seb.spg.gateway.exception.InvalidFpxPkiException;
import com.seb.spg.jpa.dao.FpxBank;
import com.seb.spg.jpa.dao.Payment;
import com.seb.spg.jpa.helper.ConfigHelper;
import com.seb.spg.jpa.helper.GatewayHelper;
import com.seb.spg.jpa.helper.PaymentHelper;
import com.seb.spg.notification.email.EmailManager;
import com.seb.spg.util.DateUtil;
import com.seb.spg.util.HttpUtil;

public class FpxManagementDelegate implements BaseDelegate {

	private static final Logger logger = LogManager.getLogger(FpxManagementDelegate.class);

	private static ScheduledExecutorService bankUpdateScheduler;
	private static ScheduledExecutorService retryScheduler;
	private static ScheduledExecutorService certificateExpiryCheckingScheduler;

	@Override
	public void start() {
		logger.info("FPX Management Delegate Started");
		
		createBankUpdateScheduler();
		createRetryScheduler();
		createCertificateCheckingScheduler();
	}
	
	private void createBankUpdateScheduler(){
		Runnable run = new Runnable() {
			@Override
			public void run() {
				performBankUpdate();
			}
		};
		
		Long first = LocalDateTime.now().until(
				LocalDateTime.now().plusHours(1).truncatedTo(ChronoUnit.HOURS), 
				ChronoUnit.MINUTES); // Next Hour
		
		long bankUpdateSchedulerIntervalMinutes = TimeUnit.HOURS.toMinutes(1);
		
		bankUpdateScheduler = Executors.newSingleThreadScheduledExecutor();
		bankUpdateScheduler.scheduleAtFixedRate(run, first, 
				bankUpdateSchedulerIntervalMinutes, TimeUnit.MINUTES);
		
		logger.info("FPX Bank Update scheduler configured at {} minutes interval", bankUpdateSchedulerIntervalMinutes);
		logger.debug("next execution in {} minutes", first);
		
		if(first >= 5){
			performBankUpdate();
		}
	}
	
	private void createRetryScheduler(){
		Runnable run = new Runnable() {
			@Override
			public void run() {
				performServerToServerUpdateRetry();				
			}
		};
		
		int retrySchedulerIntervalMinutes = 2;
		
		retryScheduler = Executors.newSingleThreadScheduledExecutor();
		retryScheduler.scheduleAtFixedRate(run, retrySchedulerIntervalMinutes, 
				retrySchedulerIntervalMinutes, TimeUnit.MINUTES);
		
		logger.info("FPX S2S Retry scheduler configured at {} minutes interval", retrySchedulerIntervalMinutes);
	}
	
	private void createCertificateCheckingScheduler(){
		Runnable run = new Runnable() {
			@Override
			public void run() {
				performCertificateExpiryChecking();
			}
		};
		
		Long first = LocalDateTime.now().until(
				LocalDate.now().plusDays(1).atStartOfDay().plusHours(9), 
				ChronoUnit.HOURS); // 9AM
		
		long certificateExpiryCheckingIntervalHours =  TimeUnit.DAYS.toHours(5);
		
		certificateExpiryCheckingScheduler = Executors.newSingleThreadScheduledExecutor();
		certificateExpiryCheckingScheduler.scheduleAtFixedRate(run, first, 
				certificateExpiryCheckingIntervalHours, TimeUnit.HOURS);
		
		logger.info("FPX Certificate expiry checking scheduler configured at {} hours interval", certificateExpiryCheckingIntervalHours);
		logger.debug("next execution in {} hours", first);
	}

	@Override
	public void stop() {
		
		if(bankUpdateScheduler != null){
			bankUpdateScheduler.shutdown();
		}
		if(retryScheduler != null){
			retryScheduler.shutdown();
		}
		if(certificateExpiryCheckingScheduler != null){
			certificateExpiryCheckingScheduler.shutdown();
		}
		logger.info("FPX Management Delegate Stopped");
	}
	
	public int performBankUpdate(){

		int bankCount = 0;
		
		try(DatabaseFacade db = new DatabaseFacade()){
			ConfigHelper cHelper = new ConfigHelper(db);
			GatewayHelper gHelper = new GatewayHelper(db);
			
			int timeout = cHelper.getGatewayQueryTimeoutInSeconds() * 1000;
			
			String bankListingUrl = gHelper.getGatewayConfigValue(FpxPaymentProcessor.GATEWAY_ID, 
					FpxPaymentProcessor.FPX_BANK_LISTING_URL_NAME);
			
			String msgType = "BE";
			String exchangeId = gHelper.getGatewayConfigValue(FpxPaymentProcessor.GATEWAY_ID, FpxPaymentProcessor.FPX_EXCHANGE_ID_KEY_NAME);
			String version = gHelper.getGatewayConfigValue(FpxPaymentProcessor.GATEWAY_ID, FpxPaymentProcessor.FPX_VERSION_KEY_NAME);
			
			for(FpxBankType bankType : FpxBankType.values()){
				
				try {
					String msgToken = bankType.getToken();
					
					String signatureFields = FpxPaymentProcessor.combineSignatureFieldsWithDelimiter('|', 
							msgToken, msgType, exchangeId, version);
					String signatureChecksum = FpxPkiImplementation.signData(signatureFields);
					
					Map<String, String> map = new HashMap<>();
					map.put("fpx_msgType", msgType);
					map.put("fpx_msgToken", msgToken);
					map.put("fpx_sellerExId", exchangeId);
					map.put("fpx_checkSum", signatureChecksum);
					map.put("fpx_version", version);
					
					FormBuilder form = new FormBuilder(bankListingUrl, map);
					
					String response = HttpUtil.post(form, timeout);
					Map<String, String> responseMap = PaymentGateway.getPostResponseMap(response);
					
					String bankList = responseMap.get("fpx_bankList");
					if(StringUtil.isEmpty(bankList)){
						logger.error("error getting bank list for type - {}, fpx_bankList empty", bankType.toString());
					}else{
						String[] rawBanks = bankList.split(",");
						logger.info("Total {} banks loaded for Type {}", rawBanks.length, bankType);
						
						bankCount = bankCount + rawBanks.length;
						
						Map<String, String> newBanks = new HashMap<>();
						for(String bank : rawBanks){
							
							String[] idStatus = bank.split("~");
							if(idStatus.length != 2){
								logger.error("bank error {}", bank);
							}else{
								newBanks.put(idStatus[0], idStatus[1].toUpperCase().toUpperCase());
							}
						}
						
						try(DatabaseFacade dbBank = new DatabaseFacade()){
							dbBank.beginTransaction();
							
							GatewayHelper helper = new GatewayHelper(dbBank);
							Map<String, FpxBank> existingBanks = helper.getFpxBank(bankType);
							
							logger.info("Total {} existing bank found from DB", existingBanks.size());
							
							for(String newBankId : newBanks.keySet()){
								
								boolean newBankStatus = "A".equals(newBanks.get(newBankId));
								FpxBank existingBank = existingBanks.get(newBankId);
								
								if(existingBank == null){
									// Create Mode
									FpxBank nb = new FpxBank();
									nb.setBankFpxId(newBankId);
									nb.setBankType(bankType);
									nb.setEnabled(newBankStatus);
									nb.setDeleted(false);
									nb.setCreatedBy("fpx-management-job");
									
									logger.debug("Creating new bank {} with status {}", newBankId, newBankStatus);
									dbBank.insert(nb);
									
								}else{
									existingBanks.remove(newBankId);
									
									// Update Mode
									if(existingBank.isEnabled() != newBankStatus || existingBank.isDeleted()){
										existingBank.setEnabled(newBankStatus);
										existingBank.setDeleted(false);
										existingBank.setLastUpdatedBy("fpx-management-job");
										
										logger.debug("Updating bank {} from status {} to status {}", newBankId, 
												existingBank.isEnabled(), newBankStatus);
										dbBank.update(existingBank);
									}
								}
							}
							
							if(!existingBanks.isEmpty()){
								
								int count = 0;
								for(String bankId : existingBanks.keySet()){
									FpxBank bank = existingBanks.get(bankId);
									if(bank.isEnabled()){
										bank.setEnabled(false);
										bank.setDeleted(true);
										bank.setLastUpdatedBy("fpx-management-job");
										
										dbBank.update(bank);
										count++;
										
										logger.debug("Updated obsolete bank {} to disable and deleted", bankId);
									}
								}
								
								if(count > 0){
									logger.info("Total {} banks were disabled due to obsolete", count);	
								}
							}
							
							dbBank.commit();
						}
					}
				} catch (IOException | HttpException e) {
					logger.error("error getting bank list for type - " + bankType.toString(), e);
				} catch (DatabaseFacadeException e) {
					logger.error("error insert/update bank - " + bankType.toString(), e);
				}
			}
		} catch (InvalidFpxPkiException e) {
			logger.error("error listing bank list from FPX - " + e.getMessage(), e);
		} catch(Throwable t){
			logger.error("generic error listing bank list from FPX - " + t.getMessage(), t);
		}
		
		logger.info("Total {} banks downloaded from FPX", bankCount);
		return bankCount;
	}
	
	private void performServerToServerUpdateRetry(){
		try(DatabaseFacade db = new DatabaseFacade()){
			ConfigHelper cHelper = new ConfigHelper(db);
			PaymentHelper pHelper = new PaymentHelper(db);
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			cal.add(Calendar.MINUTE, cHelper.getServerToServerUpdateRetryWithinMinutes() * -1);
			
			List<Payment> payments = pHelper.getUnnotifiedPayments(FpxPaymentProcessor.GATEWAY_ID, cal.getTime());
			
			for(Payment p : payments){
			
				try(DatabaseFacade dbLock = new DatabaseFacade()){
					PaymentHelper lockedPaymentHelper = new PaymentHelper(dbLock);
					
					dbLock.beginTransaction();
					Payment payment = lockedPaymentHelper.getLockedPayment(p.getPaymentId());
					
					if(payment.getStatus() != PaymentStage.RECEIVED_FAILED && 
							payment.getStatus() != PaymentStage.RECEIVED_SUCCESS){
						logger.info("This payment {} was successfully notified in another process", p.getPaymentId());
					}else{
						
						String debitAuthCode = payment.getGatewayStatus().getStatus();
						if(FpxPaymentProcessor.handlingServerToServerPaymentResponse(dbLock, payment, debitAuthCode)){
							dbLock.update(payment);
						}
					}
					
					dbLock.commit();
				} catch (DatabaseFacadeException e) {
					logger.error("error processing payment - " + p.getPaymentId(), e);
				}
			}	
		} catch(Throwable t){
			logger.error("generic error perform server to server update retry - " + t.getMessage(), t);
		}
	}
	
	public void performCertificateExpiryChecking(){
		logger.info("performCertificateExpiryChecking() - Start");
		
		try(DatabaseFacade db = new DatabaseFacade()){

			Calendar calendar = Calendar.getInstance();
			Date today = new Date();
			GatewayHelper gHelper = new GatewayHelper(db);
			Map<String, String> configs = gHelper.getGatewayConfigValueLike(FpxPaymentProcessor.GATEWAY_ID, "CERTIFICATE_EXPIRY_WARNING%");
			
			boolean notify = false;
			int warningDaysBeforeExpire = Integer.parseInt(configs.get(FpxPaymentProcessor.FPX_CERTIFICATE_EXPIRY_WARNING_DAYS_BEFORE_NAME));
			
			Date privateIdentityCertificateExpiry = null;
			String privateIdentityCertificateExpiryRemark = null;
			
			Date fpxPublicCertificateExpiry = null;
			String fpxPublicCertificateExpiryRemark = null;
			
			try{
				X509Certificate x509PrivateIdentityCert = FpxPkiImplementation.getPrivateKeyIdentityCertificate();
				
				if(x509PrivateIdentityCert == null){
					notify = true;
					privateIdentityCertificateExpiryRemark = "Exchange certificate not uploaded into SPG, unable to trace expiration date";
				}else{
					calendar.setTime(DateUtil.getStartDate(x509PrivateIdentityCert.getNotAfter()));
					calendar.add(Calendar.DAY_OF_MONTH, -1);
					privateIdentityCertificateExpiry = calendar.getTime();
					
					long privateIdentityCertificateExpiredInDays = DateUtil.getDateDiff(today, privateIdentityCertificateExpiry, TimeUnit.DAYS);
					logger.info("FPX Exchange Certificate {} <> {}", privateIdentityCertificateExpiry, privateIdentityCertificateExpiredInDays);
					
					if(privateIdentityCertificateExpiredInDays <= warningDaysBeforeExpire){
						notify = true;
						
						if(privateIdentityCertificateExpiredInDays > 0){
							privateIdentityCertificateExpiryRemark = "Expire in " + privateIdentityCertificateExpiredInDays + " day(s)";	
						}else{
							privateIdentityCertificateExpiryRemark = "Expired";
						}
					}else{
						privateIdentityCertificateExpiryRemark = "Active";
					}
				}
			}catch(InvalidFpxPkiException e){
				logger.error(e.getMessage(), e);
			}
			
			try{
				X509Certificate x509Cert = FpxPkiImplementation.getPublicCertificate();
				
				calendar.setTime(DateUtil.getStartDate(x509Cert.getNotAfter()));
				calendar.add(Calendar.DAY_OF_MONTH, -1);
				fpxPublicCertificateExpiry = calendar.getTime();
				
				long fpxPublicCertificateExpiredInDays = DateUtil.getDateDiff(today, fpxPublicCertificateExpiry, TimeUnit.DAYS);
				logger.info("FPX Public Certificate {} <> {}", fpxPublicCertificateExpiry, fpxPublicCertificateExpiredInDays);
				
				if(fpxPublicCertificateExpiredInDays <= warningDaysBeforeExpire){
					notify = true;
					
					if(fpxPublicCertificateExpiredInDays > 0){
						fpxPublicCertificateExpiryRemark = "Expire in " + fpxPublicCertificateExpiredInDays + " day(s)";	
					}else{
						fpxPublicCertificateExpiryRemark = "Expired";
					}
				}else{
					fpxPublicCertificateExpiryRemark = "Active";
				}
			}catch(InvalidFpxPkiException e){
				logger.error(e.getMessage(), e);
			}
			
			if(notify){
				String exchangeCertificateName = gHelper.getGatewayConfigValue(
						FpxPaymentProcessor.GATEWAY_ID, FpxPaymentGateway.FPX_EXCHANGE_ID_KEY_NAME)+ ".cer";
				
				EmailManager em = new EmailManager();
				String[] recipients = em.getRecipients(configs.get(
						FpxPaymentProcessor.FPX_CERTIFICATE_EXPIRY_WARNING_EMAIL_NAME));
				
				em.sendFpxCertificateExpiryWarningEmail(recipients, exchangeCertificateName, 
						privateIdentityCertificateExpiry, privateIdentityCertificateExpiryRemark, 
						fpxPublicCertificateExpiry, fpxPublicCertificateExpiryRemark);
			}
			
		} catch(Throwable t){
			logger.error("generic error perform certification expiry checking - " + t.getMessage(), t);
		}
		
		logger.info("performCertificateExpiryChecking() - End");
	}
}
