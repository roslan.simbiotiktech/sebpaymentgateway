package com.seb.spg.gateway;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.anteater.library.api.utility.StringUtil;

public class FormBuilder implements HttpPostable {

	private static final Logger logger = LogManager.getLogger(FormBuilder.class);
	
	private String action;
	
	private Map<String, String> inputs;

	public FormBuilder(String action, Map<String, String> inputs) {
		super();
		this.action = action;
		this.inputs = inputs;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Map<String, String> getInputs() {
		return inputs;
	}

	public void setInputs(Map<String, String> inputs) {
		this.inputs = inputs;
	}
	
	public String toFormUrlEncoded() throws UnsupportedEncodingException{
		Iterator<Entry<String, String>> iterator = this.inputs.entrySet().iterator();
		
		StringBuilder sb = new StringBuilder();
		
		while(iterator.hasNext()){
			
			Entry<String, String> next = iterator.next();
			String name = URLEncoder.encode(next.getKey(), "UTF-8");
			String value;
			
			if(StringUtil.isEmpty(next.getValue())){
				value = "";
			}else{
				value = URLEncoder.encode(next.getValue(), "UTF-8");
			}
			
			sb.append(name).append("=").append(value);
			if(iterator.hasNext()){
				sb.append("&");
			}
		}
		
		return sb.toString();
	}
	
	public Response toResponse(boolean print){
		String html = toString();
		
		if(print && logger.isDebugEnabled()) logger.debug(html);
		
		return Response.status(200)
				.entity(html).type("text/html")
				.build();
	}
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		
		sb.append("<html>");
		sb.append("<body onload=\"document.forms['_form'].submit()\">");
		sb.append("<form method=\"post\" name=\"_form\" action=\"").append(action).append("\">");
		
		if(inputs != null && !inputs.isEmpty()){
			for(Entry<String, String> input : inputs.entrySet()){
				sb.append("<input type=\"hidden\" name=\"").append(input.getKey());
				sb.append("\" value=\"").append(StringEscapeUtils.escapeHtml4(input.getValue())).append("\"/>");
			}
		}
		
		sb.append("</form>");
		sb.append("</body>");
		sb.append("</html>");
		
		return sb.toString();
	}

	@Override
	public String getPostUrl() {
		return action;
	}

	@Override
	public Map<String, String> getPostParameter() {
		return inputs;
	}
}
