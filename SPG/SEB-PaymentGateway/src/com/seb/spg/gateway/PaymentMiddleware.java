package com.seb.spg.gateway;

import javax.validation.Valid;
import javax.ws.rs.core.Response;

import com.seb.spg.gateway.exception.InvalidPaymentRequestException;

public interface PaymentMiddleware<P extends PaymentRequest, Q extends PaymentQuery> {

	public Response makePayment(@Valid P request) throws InvalidPaymentRequestException;
	
	public Response queryPayment(@Valid Q query) throws InvalidPaymentRequestException;
	
	public Response test();
}
