package com.seb.spg.gateway;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.anteater.library.api.utility.StringUtil;
import com.anteater.library.jpa.DatabaseFacade;
import com.seb.spg.consts.PaymentResponseStatus;
import com.seb.spg.consts.PaymentStage;
import com.seb.spg.consts.StatusDirection;
import com.seb.spg.gateway.exception.InvalidPaymentRequestException;
import com.seb.spg.gateway.exception.InvalidPaymentStatusException;
import com.seb.spg.jpa.dao.Gateway;
import com.seb.spg.jpa.dao.GatewayConfig;
import com.seb.spg.jpa.dao.Merchant;
import com.seb.spg.jpa.dao.Payment;
import com.seb.spg.jpa.dao.Status;
import com.seb.spg.jpa.helper.ConfigHelper;
import com.seb.spg.jpa.helper.GatewayHelper;
import com.seb.spg.jpa.helper.MerchantHelper;
import com.seb.spg.util.CryptographyUtil;

public abstract class PaymentGateway<P extends PaymentRequest, Q extends PaymentQuery, S extends PaymentStatus>
		implements PaymentMiddleware<P, Q>, PaymentManager<S> {

	private static final Logger logger = LogManager.getLogger(PaymentGateway.class);
	
	private Gateway gateway;
	private Map<String, GatewayConfig> gatewayConfigs;
	
	protected Merchant merchant;

	protected Payment preparePayment(P request){
		
		Payment payment = new Payment();
		payment.setMerchantId(merchant.getMerchantId());
		payment.setMerchantTransactionId(request.getTransactionId());
		payment.setAmount(request.getAmount());
		payment.setCustomerUsername(request.getCustomerUsername());
		payment.setCustomerEmail(request.getCustomerEmail());
		payment.setStatus(PaymentStage.NEW);
		payment.setProductDescription(request.getProductDescription());
		
		if(StringUtil.isEmpty(request.getReturnUrl())){
			payment.setClientReturnUrl(merchant.getClientPaymentUpdateUrl());
		}else{
			payment.setClientReturnUrl(request.getReturnUrl());
		}
		
		payment.setGatewayId(gateway.getGatewayId());
		payment.setCreatedBy(merchant.getMerchantName());
		
		return payment;
	}
	
	protected void validatePaymentRequest(P request) throws InvalidPaymentRequestException {
		
		try(DatabaseFacade db = new DatabaseFacade()){
			MerchantHelper mHelper = new MerchantHelper(db);
			Merchant merchant = mHelper.getMerchant(request.getMerchantId());
			
			if(gateway.getMinimumPayment() != null 
					&& request.getAmount().compareTo(gateway.getMinimumPayment()) < 0){
				
				logger.error("Gateway {} minimum payment is {}, payment {} rejected", 
						gateway.getGatewayId(), gateway.getMinimumPayment(), request.getAmount());
				throw new InvalidPaymentRequestException("invalid amount " + request.getAmount());
				
			}else if(gateway.getMaximumPayment() != null && request.getAmount().compareTo(gateway.getMaximumPayment()) > 0){
				
				logger.error("Gateway {} maximum payment is {}, payment {} rejected", 
						gateway.getGatewayId(), gateway.getMaximumPayment(), request.getAmount());
				throw new InvalidPaymentRequestException("invalid amount " + request.getAmount());
				
			}
			
			validateMerchant(request, merchant);
			
			String signingFields = request.getSigningFields() + merchant.getSignatureSecret();	
			String signature = CryptographyUtil.toSha512HexadecimalLowercase(signingFields);
			
			if(!signature.equals(request.getSignature())){
				logger.error("Transaction signature mismatch");
				logger.error(" Request Signature : {}", request.getSignature());
				logger.error(" Compute Signature : {}", signature);
				throw new InvalidPaymentRequestException("invalid request signature");
			}
			
			this.merchant = merchant;
		}
	}
	
	protected void validatePaymentQuery(Q query) throws InvalidPaymentRequestException {
		
		try(DatabaseFacade db = new DatabaseFacade()){
			MerchantHelper mHelper = new MerchantHelper(db);
			Merchant merchant = mHelper.getMerchant(query.getMerchantId());
			
			validateMerchant(merchant);
			
			String signingFields = query.getSigningFields() + merchant.getSignatureSecret();	
			String signature = CryptographyUtil.toSha512HexadecimalLowercase(signingFields);
			
			if(!signature.equals(query.getSignature())){
				logger.error("Transaction signature mismatch");
				logger.error(" Request Signature : {}", query.getSignature());
				logger.error(" Compute Signature : {}", signature);
				throw new InvalidPaymentRequestException("invalid request signature");
			}
			
			this.merchant = merchant;
		}
	}
	
	protected void validateMerchant(Merchant merchant) throws InvalidPaymentRequestException {

		if(merchant == null){
			logger.error("Merchant not found");
			throw new InvalidPaymentRequestException("merchant not found");
		}else if(!merchant.isEnabled()){
			logger.error("Merchant was suspended");
			throw new InvalidPaymentRequestException("merchant was suspended");
		}
	}
	
	protected void validateMerchant(P request, Merchant merchant) throws InvalidPaymentRequestException {

		validateMerchant(merchant);
		
		if(merchant.getMinimumPayment() != null 
				&& request.getAmount().compareTo(merchant.getMinimumPayment()) < 0){
			
			logger.error("Merchant {} minimum payment is {}, payment {} rejected", 
					request.getMerchantId(), merchant.getMinimumPayment(), request.getAmount());
			throw new InvalidPaymentRequestException("invalid amount " + request.getAmount());
			
		}else if(merchant.getMaximumPayment() != null && request.getAmount().compareTo(merchant.getMaximumPayment()) > 0){
			
			logger.error("Merchant {} maximum payment is {}, payment {} rejected", 
					request.getMerchantId(), merchant.getMaximumPayment(), request.getAmount());
			throw new InvalidPaymentRequestException("invalid amount " + request.getAmount());
			
		}else if(StringUtil.isEmpty(merchant.getClientPaymentUpdateUrl()) &&
				StringUtil.isEmpty(request.getReturnUrl())){
			
			logger.error("Merchant {} did not configure client return URL, and the request Return URL also not defined", 
					request.getMerchantId());
			throw new InvalidPaymentRequestException("return_url may not be null");
		}
	}
	
	protected abstract void validatePaymentStatus(S status) throws InvalidPaymentStatusException;
	
	protected abstract String getGatewayId();
	
	protected String getGatewayConfig(String configKey){
		return gatewayConfigs.get(configKey).getConfigValue();
	}
	
	@PostConstruct
    private void initializeGateway() {
		String gatewayId = getGatewayId();
        
        try(DatabaseFacade db = new DatabaseFacade()){
        	GatewayHelper gHelper = new GatewayHelper(db);
        	
        	Gateway gw = gHelper.getGateway(gatewayId);
        	validateGateway(gw);
        	
        	gateway = gw;
        	
        	gatewayConfigs = new HashMap<>();
        	if(gateway.getConfigs() != null){
        		for(GatewayConfig gc : gateway.getConfigs()){
        			gatewayConfigs.put(gc.getConfigKey(), gc);
        		}
        	}
        }
    }
		
	private void validateGateway(Gateway gw){
		if(gw == null){
			logger.error("Gateway {} not found in database", getGatewayId());
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}else if(!gw.isEnabled()){
			logger.error("Gateway {} was disabled at {} by {}", getGatewayId(), gw.getLastUpdatedDatetime(), gw.getLastUpdatedBy());
			throw new WebApplicationException(Response.Status.FORBIDDEN);
		}
	}
	
	public static String combineSignatureFieldsWithDelimiter(String ... fields){
		return combineSignatureFieldsWithDelimiter(null, fields);
	}
	
	public static String combineSignatureFieldsWithDelimiter(Character delimiter, String ... fields){
		StringBuilder sb = new StringBuilder();
		
		if(fields != null && fields.length > 0){
			
			for(int i = 0; i < fields.length; i++){
				String field = fields[i];
				if(field != null)
					sb.append(field);
				
				if(delimiter != null && i + 1 < fields.length){
					sb.append(delimiter);
				}
			}
		}
		
		return sb.toString();
	}
	
	public static FormBuilder generateResponseForm(String secret, String url, 
			String transactionId, BigDecimal amount, Long paymentId, 
			String gatewayTransactionId, String gatewayTransctionStatus,
			PaymentResponseStatus status, Date transactionAuthorizedDate, String message){
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		DecimalFormat df = new DecimalFormat("0.00");
		
		String amountString = df.format(amount);
		String transactionAuthorizedDateString = transactionAuthorizedDate == null ? "" : sdf.format(transactionAuthorizedDate);
		String spgTransactionId = null;
		if(paymentId != null){
			spgTransactionId = String.valueOf(paymentId);
		}
		
		String signingFields = combineSignatureFieldsWithDelimiter(transactionId, spgTransactionId, 
				gatewayTransactionId, gatewayTransctionStatus, status.getCode(), 
				amountString, message, transactionAuthorizedDateString, secret);
		
		String signature = CryptographyUtil.toSha512HexadecimalLowercase(signingFields);
		
		Map<String, String> inputs = new HashMap<>();
		inputs.put("transaction_id", transactionId);
		inputs.put("spg_transaction_id", spgTransactionId);
		inputs.put("gateway_transaction_id", gatewayTransactionId);
		inputs.put("gateway_transaction_status", gatewayTransctionStatus);
		inputs.put("status_code", status.getCode());
		inputs.put("amount", amountString);
		inputs.put("message", message);
		inputs.put("transaction_auth_date", transactionAuthorizedDateString);
		inputs.put("signature", signature);
		
		return new FormBuilder(url, inputs);
	}
	
	protected FormBuilder generateResponseForm(P request, PaymentResponseStatus status, Merchant merchant, String message){
		
		String actionUrl = request.getReturnUrl();
		if(StringUtil.isEmpty(actionUrl)) actionUrl = merchant.getClientPaymentUpdateUrl();
		
		return generateResponseForm(merchant.getSignatureSecret(), 
				actionUrl, request.getTransactionId(), request.getAmount(), 
				null, null, null, status, null, message);
	}
	
	public static Map<String, String> getPostResponseMap(String response) throws UnsupportedEncodingException {

		Map<String, String> map = new HashMap<>();

		if (!StringUtil.isEmpty(response)) {
			String[] valuePairs = response.split("&");

			for (String vps : valuePairs) {

				String[] vp = vps.split("=");
				String name = vp.length >= 1 ? URLDecoder.decode(vp[0], "UTF-8") : null;
				String value = vp.length >= 2 ? URLDecoder.decode(vp[1], "UTF-8") : null;

				map.put(name, value);
			}
		}

		return map;
	}
	
	protected static Status getStatusForGatewayToSpg(long paymentId, String statusCode, String remark){
		Status status = new Status();
		
		status.setStatus(statusCode);
		status.setDirection(StatusDirection.GATEWAT_TO_SPG);
		status.setPaymentId(paymentId);
		status.setRemark(remark);
		
		return status;
	}
	
	protected static Status getStatusForSpgToMerchant(long paymentId, String statusCode, String remark){
		Status status = new Status();
		
		status.setStatus(statusCode);
		status.setDirection(StatusDirection.SPG_TO_MERCHANT);
		status.setPaymentId(paymentId);
		status.setRemark(remark);
		
		return status;
	}
	
	protected FormBuilder clientPaymentResponse(PaymentResponseStatus responseStatus, Payment payment, Merchant merchant,
			String gatewayTransactionStatus) {
		
		return generateResponseForm(merchant.getSignatureSecret(), payment.getClientReturnUrl(), 
				payment.getMerchantTransactionId(), payment.getAmount(), payment.getPaymentId(), 
				payment.getGatewayTransactionId(), gatewayTransactionStatus,  
				responseStatus, payment.getGatewayAuthorizationDatetime(), responseStatus.getDefaultMessage());
	}
	
	protected void logPaymentDetails(Payment payment){
		logger.debug("locks obtained, payment status is '{}'", payment.getStatus().toString());
		logger.debug("gateway status - {}", payment.getGatewayStatus());
		logger.debug("merchant client status - {}", payment.getMerchantClientStatus());
		logger.debug("merchant s2s status - {}", payment.getMerchantS2sStatus());
		logger.debug("gateway transaction id - {}", payment.getGatewayTransactionId());
		logger.debug("gateway authorization date - {}", payment.getGatewayAuthorizationDatetime());
	}
	
	/**
	 * For testing environment
	 */
	protected boolean isBypassGatewayRounting(){
		try(DatabaseFacade db = new DatabaseFacade()){
			ConfigHelper cHelper = new ConfigHelper(db);
			return cHelper.isSPGBypassGatewayRounting();
		}
	}
	
	protected boolean isTestModeEnabled(){
		try(DatabaseFacade db = new DatabaseFacade()){
			ConfigHelper cHelper = new ConfigHelper(db);
			return cHelper.isSPGTestModeEnabled();
		}
	}
}