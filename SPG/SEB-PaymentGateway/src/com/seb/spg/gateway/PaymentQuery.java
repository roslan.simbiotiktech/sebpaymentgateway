package com.seb.spg.gateway;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.FormParam;

import com.anteater.library.api.utility.StringUtil;

/**
 * Payment Query Base class
 *  - From Payment sub-system to SPG
 */
public class PaymentQuery {

	@NotNull(message = "merchant_id may not be null")
	@Size(min = 1, max = 50, message = "merchant_id must be between 1 to 50 characters")
	@FormParam("merchant_id")
	protected String merchantId;
	
	@NotNull(message = "transaction_id may not be null")
	@Size(min = 1, max = 25, message = "transaction_id must be between 1 to 64 characters")
	@FormParam("transaction_id")
	protected String transactionId;
	
	@NotNull(message = "amount may not be null")
	@FormParam("amount")
	@Digits(integer = 9, fraction = 2, message = "amount must be in Numeric(9,2) format")
	protected BigDecimal amount;
	
	@NotNull(message = "signature may not be null")
	@Size(min = 128, max = 128, message = "signature must be 128 characters")
	@FormParam("signature")
	protected String signature;

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	@Override
	public String toString() {
		return "PaymentQuery [merchantId=" + merchantId + ", transactionId=" + transactionId + ", amount=" + amount
				+ ", signature=" + signature + "]";
	}
	
	public String getSigningFields(){
		
		DecimalFormat df = new DecimalFormat("0.00");
		
		List<String> fields = new ArrayList<>();
		fields.add(merchantId);
		fields.add(transactionId);
		fields.add(df.format(amount));

		return combineFields(fields);
	}
	
	private String combineFields(List<String> list){ 
		
		StringBuilder sb = new StringBuilder();
		
		for(String s : list){
			if(!StringUtil.isEmpty(s)){
				sb.append(s);
			}
		}
		
		return sb.toString();
	}
}
