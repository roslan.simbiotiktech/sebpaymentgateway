package com.seb.spg.gateway;

import java.util.Map;

public interface HttpPostable {

	public String getPostUrl();
	public Map<String, String> getPostParameter();
	
}
