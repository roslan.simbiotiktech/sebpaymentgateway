package com.seb.spg.api.object;

import java.util.Date;

import com.anteater.library.json.JsonField;

public class OMerchant extends OMerchantUpdate{
	
	@JsonField(
			name = "last_updated_by", 
			description = "Record Last Updated By",
			maxLength = 50)
	private String lastUpdatedBy;
	
	@JsonField(
			name = "last_updated_datetime", 
			description = "Record Last Updated On")
	private Date lastUpdatedDatetime;
	
	@JsonField(
			name = "created_by", 
			description = "Record Created By")
	private String createdBy;
	
	@JsonField(
			name = "created_datetime", 
			description = "Record Created On")
	private Date createdDatetime;

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}
	
}
