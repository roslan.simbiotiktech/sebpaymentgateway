package com.seb.spg.api.object;

import java.util.Date;

import com.anteater.library.json.JsonField;

public class OPaymentReport extends ODateBase {
	
	@JsonField(
			name="payment_id",
			description = "Payment Id")
	private long paymentId;
	
	@JsonField(name="merchant_id",
			description = "Merchant Id")
	private int merchantId;
	
	@JsonField(name="merchant_transaction_id",
			description = "Merchant Transaction Id")
	private String merchantTransactionId;
	
	@JsonField(name="amount",
			description = "Amount")
	private double amount;
	
	@JsonField(name="customer_email",
			description = "Customer Email")
	private String customerEmail;
	
	@JsonField(name="customer_username",
			description = "Customer Username")
	private String customerUsername;
	
	@JsonField(name="status",
			description = "Status")
	private String status;
	
	@JsonField(name="product_description",
			description = "Product Description")
	private String productDescription;
	
	@JsonField(name="client_return_url",
			description = "Client Return Url")
	private String clientReturnUrl;
	
	@JsonField(name="gateway_id",
			description = "Gateway Id")
	private String gatewayId;
	
	@JsonField(name="gateway_transaction_id",
			description = "Gateway Transaction Id")
	private String gatewayTransactionId;
	
	@JsonField(name="gateway_authorization_datetime",
			description = "Gateway Authorization Datetime")
	private Date gatewayAuthorizationDatetime;
	
	@JsonField(name="gateway_status_id",
			description = "Gateway Status Id")
	private Long gatewayStatusId;
	
	@JsonField(name="merchant_s2s_status_id",
			description = "Merchant Server to Server Status Id")
	private Long merchantS2sStatusId;
	
	@JsonField(name="merchant_client_status_id",
			description = "Merchant Client Status Id")
	private Long merchantClientStatusId;

	public long getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(long paymentId) {
		this.paymentId = paymentId;
	}

	public int getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}

	public String getMerchantTransactionId() {
		return merchantTransactionId;
	}

	public void setMerchantTransactionId(String merchantTransactionId) {
		this.merchantTransactionId = merchantTransactionId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getCustomerUsername() {
		return customerUsername;
	}

	public void setCustomerUsername(String customerUsername) {
		this.customerUsername = customerUsername;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public String getClientReturnUrl() {
		return clientReturnUrl;
	}

	public void setClientReturnUrl(String clientReturnUrl) {
		this.clientReturnUrl = clientReturnUrl;
	}

	public String getGatewayId() {
		return gatewayId;
	}

	public void setGatewayId(String gatewayId) {
		this.gatewayId = gatewayId;
	}

	public String getGatewayTransactionId() {
		return gatewayTransactionId;
	}

	public void setGatewayTransactionId(String gatewayTransactionId) {
		this.gatewayTransactionId = gatewayTransactionId;
	}

	public Date getGatewayAuthorizationDatetime() {
		return gatewayAuthorizationDatetime;
	}

	public void setGatewayAuthorizationDatetime(Date gatewayAuthorizationDatetime) {
		this.gatewayAuthorizationDatetime = gatewayAuthorizationDatetime;
	}

	public Long getGatewayStatusId() {
		return gatewayStatusId;
	}

	public void setGatewayStatusId(Long gatewayStatusId) {
		this.gatewayStatusId = gatewayStatusId;
	}

	public Long getMerchantS2sStatusId() {
		return merchantS2sStatusId;
	}

	public void setMerchantS2sStatusId(Long merchantS2sStatusId) {
		this.merchantS2sStatusId = merchantS2sStatusId;
	}

	public Long getMerchantClientStatusId() {
		return merchantClientStatusId;
	}

	public void setMerchantClientStatusId(Long merchantClientStatusId) {
		this.merchantClientStatusId = merchantClientStatusId;
	}
	
}
