package com.seb.spg.api.object;

import java.util.Date;

import com.anteater.library.json.JsonField;

public class OPaymentDetailReport {
	
	@JsonField(
			name="status",
			description="Payment Detail Status")
	private String status;
	
	@JsonField(
			name="direction",
			description="Payment Detail Direction")
	private String direction;
	
	@JsonField(
			name="remark",
			description="Payment Detail Remark")
	private String remark;

	@JsonField(
			name="created_datetime")
	private Date createdDatetime;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}
	
}
