package com.seb.spg.api.object;

import com.anteater.library.json.JsonField;

public class OFpxBankUpdate {
	
	@JsonField(
			name="bank_id",
			description = "Bank Id",
			mandatory = true)
	private int bankId;

	@JsonField(
			name="bank_name", 
			description = "Bank Name",
			maxLength = 200)
	private String bankName;
	
	@JsonField(
			name="logo",
			description = "Logo encoded as base64 string",
			validateLength = false)
	private String logo;

	public int getBankId() {
		return bankId;
	}

	public void setBankId(int bankId) {
		this.bankId = bankId;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}
	
}
