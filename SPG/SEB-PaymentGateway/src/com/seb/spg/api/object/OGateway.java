package com.seb.spg.api.object;

import java.util.Date;

import com.anteater.library.json.JsonField;

public class OGateway extends OGatewayBase{

	@JsonField(
			name="id",
			description = "Gateway Id", mandatory = true)
	private String gatewayId;
	
	@JsonField(
			name="description", 
			description = "Gateway Description", 
			maxLength = 500)
	private String description;
	
	@JsonField(
			name="last_updated_by", 
			description = "Record Last Updated By", 
			maxLength = 50)
	private String lastUpdatedBy;
	
	@JsonField(
			name="last_updated_datetime",
			description = "Record Last Updated On")
	private Date lastUpdatedDatetime;

	public String getGatewayId() {
		return gatewayId;
	}

	public void setGatewayId(String gatewayId) {
		this.gatewayId = gatewayId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

}
