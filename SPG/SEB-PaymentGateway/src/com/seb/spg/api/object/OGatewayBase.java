package com.seb.spg.api.object;

import com.anteater.library.json.JsonField;

public class OGatewayBase {
	
	@JsonField(
			name="minimun_payment_amount", 
			description = "Minimum Payment Amount / No limit if null")
	private Double minimunPaymentAmount;
	
	@JsonField(
			name="maximum_payment_amount", 
			description = "Maximum Payment Amount / No limit if null")
	private Double maximumPaymentAmount;
	
	@JsonField(
			name="enabled",
			description = "Gateway Status (Active/Inactive)")
	private boolean enabled;

	public Double getMinimunPaymentAmount() {
		return minimunPaymentAmount;
	}

	public void setMinimunPaymentAmount(Double minimunPaymentAmount) {
		this.minimunPaymentAmount = minimunPaymentAmount;
	}

	public Double getMaximumPaymentAmount() {
		return maximumPaymentAmount;
	}

	public void setMaximumPaymentAmount(Double maximumPaymentAmount) {
		this.maximumPaymentAmount = maximumPaymentAmount;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
}
