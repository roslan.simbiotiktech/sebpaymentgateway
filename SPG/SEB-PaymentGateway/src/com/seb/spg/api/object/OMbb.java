package com.seb.spg.api.object;

import com.anteater.library.json.JsonField;

public class OMbb extends OGatewayBase{
	
	@JsonField(
			name="merchant_id",
			description = "Merchant Id")
	private String merchantId;
	
	@JsonField(
			name="submit_url", 
			description = "Submit Url")
	private String submitUrl;
	
	@JsonField(
			name="query_url", 
			description = "Query Url")
	private String queryUrl;
	
	@JsonField(
			name="hash_key", 
			description = "Hash Key")
	private String hashKey;
	
	@JsonField(
			name="amex_merchant_id", 
			description = "Amex Merchant Id")
	private String amexMerchantId;
	
	@JsonField(
			name="amex_hash_key", 
			description = "Amex Hash Key")
	private String amexHashKey;

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getSubmitUrl() {
		return submitUrl;
	}

	public void setSubmitUrl(String submitUrl) {
		this.submitUrl = submitUrl;
	}

	public String getQueryUrl() {
		return queryUrl;
	}

	public void setQueryUrl(String queryUrl) {
		this.queryUrl = queryUrl;
	}

	public String getHashKey() {
		return hashKey;
	}

	public void setHashKey(String hashKey) {
		this.hashKey = hashKey;
	}

	public String getAmexMerchantId() {
		return amexMerchantId;
	}

	public void setAmexMerchantId(String amexMerchantId) {
		this.amexMerchantId = amexMerchantId;
	}

	public String getAmexHashKey() {
		return amexHashKey;
	}

	public void setAmexHashKey(String amexHashKey) {
		this.amexHashKey = amexHashKey;
	}

}
