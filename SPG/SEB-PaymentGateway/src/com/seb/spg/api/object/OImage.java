package com.seb.spg.api.object;

import com.anteater.library.json.JsonField;

public class OImage {
	
	@JsonField(
			name="image_base64",
			description = "Image encoded as base64 string",
			mandatory = true,
			validateLength = false)
	private String imageBase64;
	
	@JsonField(
			name="image_name",
			description = "Image Name",
			mandatory = true)
	private String imageName;
	
	@JsonField(
			name="image_extension",
			description = "Image Extension",
			mandatory = true)
	private String imageExtension;

	public String getImageBase64() {
		return imageBase64;
	}

	public void setImageBase64(String imageBase64) {
		this.imageBase64 = imageBase64;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getImageExtension() {
		return imageExtension;
	}

	public void setImageExtension(String imageExtension) {
		this.imageExtension = imageExtension;
	}
	
}
