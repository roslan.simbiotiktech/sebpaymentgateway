package com.seb.spg.api.object;

import java.util.Date;

import com.anteater.library.json.JsonField;

public class OFpx extends OGatewayBase{
	
	@JsonField(
			name="merchant_id",
			description = "Merchant Id")
	private String merchantId;

	@JsonField(
			name="exchange_id",
			description = "Exchange Id")
	private String exchangeId;
	
	@JsonField(
			name="seller_bank_code", 
			description = "Seller Bank Code")
	private String sellerBankCode;
	
	@JsonField(
			name="version", 
			description = "FPX Version")
	private String version;
	
	@JsonField(
			name="submit_url", 
			description = "Submit Url")
	private String submitUrl;
	
	@JsonField(
			name="query_url", 
			description = "Query Url")
	private String queryUrl;
	
	@JsonField(
			name="bank_listing_url", 
			description = "Bank Listing Url")
	private String bankListingUrl;
	
	@JsonField(
			name="csr_country_name", 
			description = "Csr Country Name")
	private String csrCountryName;
	
	@JsonField(
			name="csr_state_or_province_name", 
			description = "Csr State Or Province Name")
	private String csrStateOrProvinceName;
	
	@JsonField(
			name="csr_locality_name", 
			description = "Csr Locality Name")
	private String csrLocalityName;
	
	@JsonField(
			name="csr_organization_name", 
			description = "Csr Organization Name")
	private String csrOrganizationName;
	
	@JsonField(
			name="csr_organization_unit", 
			description = "Csr Organization Unit")
	private String csrOrganizationUnit;
	
	@JsonField(
			name="csr_common_name", 
			description = "Csr Common Name")
	private String csrCommonName;
	
	@JsonField(
			name="csr_email", 
			description = "Csr Email")
	private String csrEmail;
	
	@JsonField(
			name="certificate_expiry_warning_days_before", 
			description = "Certificate Expiry Warning Days Before")
	private int certificateExpiryWarningDaysBefore;
	
	@JsonField(
			name="certificate_expiry_warning_email", 
			description = "Certificate Expiry Warning Email")
	private String certificateExpiryWarningEmail;
	
	@JsonField(
			name="public_key_file_name", 
			description = "FPX Public Key File Name")
	private String publicKeyFileName;
	
	@JsonField(
			name="public_key", 
			description = "FPX Public Key File (e.g. fpxprod.cer encoded Base64) ",
			validateLength = false)
	private String publicKey;
	
	@JsonField(
			name="public_key_expiry_date", 
			description = "FPX Public Key Expiry Date ")
	private Date publicKeyExpiryDate;
	
	@JsonField(
			name="private_key_file_name", 
			description = "Exchange Certificate - Private key File Name")
	private String privateKeyFileName;
	
	@JsonField(
			name="private_key", 
			description = "Exchange Certificate - Private key File (e.g. <Exchange_Id>.key encoded Base64)",
			validateLength = false)
	private String privateKey;
	
	@JsonField(
			name="private_key_expiry_date", 
			description = "Exchange Certificate - Private key Expiry Date ")
	private Date privateKeyExpiryDate;
	
	@JsonField(
			name="private_key_identity_cert_file_name", 
			description = "Exchange Certificate - Signing Reply File Name")
	private String privateKeyIdentityCertFileName;
	
	@JsonField(
			name="private_key_identity_cert", 
			description = "Exchange Certificate - Signing Reply (e.g. <Exchange_id>.cer encoded Base64)",
			validateLength = false)
	private String privateKeyIdentityCert;

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getExchangeId() {
		return exchangeId;
	}

	public void setExchangeId(String exchangeId) {
		this.exchangeId = exchangeId;
	}

	public String getSellerBankCode() {
		return sellerBankCode;
	}

	public void setSellerBankCode(String sellerBankCode) {
		this.sellerBankCode = sellerBankCode;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getSubmitUrl() {
		return submitUrl;
	}

	public void setSubmitUrl(String submitUrl) {
		this.submitUrl = submitUrl;
	}

	public String getQueryUrl() {
		return queryUrl;
	}

	public void setQueryUrl(String queryUrl) {
		this.queryUrl = queryUrl;
	}

	public String getBankListingUrl() {
		return bankListingUrl;
	}

	public void setBankListingUrl(String bankListingUrl) {
		this.bankListingUrl = bankListingUrl;
	}

	public String getCsrCountryName() {
		return csrCountryName;
	}

	public void setCsrCountryName(String csrCountryName) {
		this.csrCountryName = csrCountryName;
	}

	public String getCsrStateOrProvinceName() {
		return csrStateOrProvinceName;
	}

	public void setCsrStateOrProvinceName(String csrStateOrProvinceName) {
		this.csrStateOrProvinceName = csrStateOrProvinceName;
	}

	public String getCsrLocalityName() {
		return csrLocalityName;
	}

	public void setCsrLocalityName(String csrLocalityName) {
		this.csrLocalityName = csrLocalityName;
	}

	public String getCsrOrganizationName() {
		return csrOrganizationName;
	}

	public void setCsrOrganizationName(String csrOrganizationName) {
		this.csrOrganizationName = csrOrganizationName;
	}

	public String getCsrOrganizationUnit() {
		return csrOrganizationUnit;
	}

	public void setCsrOrganizationUnit(String csrOrganizationUnit) {
		this.csrOrganizationUnit = csrOrganizationUnit;
	}

	public String getCsrCommonName() {
		return csrCommonName;
	}

	public void setCsrCommonName(String csrCommonName) {
		this.csrCommonName = csrCommonName;
	}

	public String getCsrEmail() {
		return csrEmail;
	}

	public void setCsrEmail(String csrEmail) {
		this.csrEmail = csrEmail;
	}

	public int getCertificateExpiryWarningDaysBefore() {
		return certificateExpiryWarningDaysBefore;
	}

	public void setCertificateExpiryWarningDaysBefore(int certificateExpiryWarningDaysBefore) {
		this.certificateExpiryWarningDaysBefore = certificateExpiryWarningDaysBefore;
	}

	public String getCertificateExpiryWarningEmail() {
		return certificateExpiryWarningEmail;
	}

	public void setCertificateExpiryWarningEmail(String certificateExpiryWarningEmail) {
		this.certificateExpiryWarningEmail = certificateExpiryWarningEmail;
	}

	public String getPublicKeyFileName() {
		return publicKeyFileName;
	}

	public void setPublicKeyFileName(String publicKeyFileName) {
		this.publicKeyFileName = publicKeyFileName;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

	public String getPrivateKeyFileName() {
		return privateKeyFileName;
	}

	public void setPrivateKeyFileName(String privateKeyFileName) {
		this.privateKeyFileName = privateKeyFileName;
	}

	public String getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}

	public String getPrivateKeyIdentityCertFileName() {
		return privateKeyIdentityCertFileName;
	}

	public void setPrivateKeyIdentityCertFileName(String privateKeyIdentityCertFileName) {
		this.privateKeyIdentityCertFileName = privateKeyIdentityCertFileName;
	}

	public String getPrivateKeyIdentityCert() {
		return privateKeyIdentityCert;
	}

	public void setPrivateKeyIdentityCert(String privateKeyIdentityCert) {
		this.privateKeyIdentityCert = privateKeyIdentityCert;
	}

	public Date getPublicKeyExpiryDate() {
		return publicKeyExpiryDate;
	}

	public void setPublicKeyExpiryDate(Date publicKeyExpiryDate) {
		this.publicKeyExpiryDate = publicKeyExpiryDate;
	}

	public Date getPrivateKeyExpiryDate() {
		return privateKeyExpiryDate;
	}

	public void setPrivateKeyExpiryDate(Date privateKeyExpiryDate) {
		this.privateKeyExpiryDate = privateKeyExpiryDate;
	}

}
