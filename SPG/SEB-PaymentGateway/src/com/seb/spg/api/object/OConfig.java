package com.seb.spg.api.object;

import com.anteater.library.json.JsonField;

public class OConfig {
	
	@JsonField(
			name="server_to_server_update_retry_within_minutes",
			description = "Server To Server Update Retry Within Minutes")
	private int s2sUpdateRetryWithinMinutes;
	
	@JsonField(
			name="server_to_server_update_timeout_seconds",
			description = "Server To Server Update Timeout Seconds")
	private int s2sUpdateTimeoutSeconds;
	
	@JsonField(
			name="gateway_query_timeout_seconds", 
			description = "Gateway Query Timeout Seconds")
	private int gatewayQueryTimeoutSeconds;
	
	@JsonField(
			name="email_sender", 
			description = "Email Sender")
	private String emailSender;
	
	@JsonField(
			name="public_web_path", 
			description = "Public Web Path")
	private String publicWebPath;

	public int getS2sUpdateRetryWithinMinutes() {
		return s2sUpdateRetryWithinMinutes;
	}

	public void setS2sUpdateRetryWithinMinutes(int s2sUpdateRetryWithinMinutes) {
		this.s2sUpdateRetryWithinMinutes = s2sUpdateRetryWithinMinutes;
	}

	public int getS2sUpdateTimeoutSeconds() {
		return s2sUpdateTimeoutSeconds;
	}

	public void setS2sUpdateTimeoutSeconds(int s2sUpdateTimeoutSeconds) {
		this.s2sUpdateTimeoutSeconds = s2sUpdateTimeoutSeconds;
	}

	public int getGatewayQueryTimeoutSeconds() {
		return gatewayQueryTimeoutSeconds;
	}

	public void setGatewayQueryTimeoutSeconds(int gatewayQueryTimeoutSeconds) {
		this.gatewayQueryTimeoutSeconds = gatewayQueryTimeoutSeconds;
	}

	public String getEmailSender() {
		return emailSender;
	}

	public void setEmailSender(String emailSender) {
		this.emailSender = emailSender;
	}

	public String getPublicWebPath() {
		return publicWebPath;
	}

	public void setPublicWebPath(String publicWebPath) {
		this.publicWebPath = publicWebPath;
	}
	
}
