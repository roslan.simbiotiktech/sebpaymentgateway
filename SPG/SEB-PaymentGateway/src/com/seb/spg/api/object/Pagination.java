package com.seb.spg.api.object;

import com.anteater.library.json.JsonField;

public class Pagination {

	@JsonField(
			name = "paging",
			description = "Paging Details")
	private Paging paging;
	
	@JsonField(
			name = "max_page",
			description = "Max Page of Records")
	private int maxPage;
	
	@JsonField(
			name = "total_records",
			description = "Total Records")
	private int totalRecords;

	public int calculateMaxPage(int totalRecord, int recordPerPage){
		double tr = totalRecord;
		double rpp = recordPerPage;
		return (int)Math.ceil(tr/rpp);
	}
	
	public Paging getPaging() {
		return paging;
	}

	public void setPaging(Paging paging) {
		this.paging = paging;
	}

	public int getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(int maxPage) {
		this.maxPage = maxPage;
	}

	public int getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}

	@Override
	public String toString() {
		return "Pagination [paging=" + paging + ", maxPage=" + maxPage + ", totalRecords=" + totalRecords + "]";
	}	
}
