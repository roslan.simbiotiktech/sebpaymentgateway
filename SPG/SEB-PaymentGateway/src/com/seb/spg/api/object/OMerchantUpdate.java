package com.seb.spg.api.object;

import com.anteater.library.json.JsonField;

public class OMerchantUpdate extends OMerchantBase {
	
	@JsonField(
			name = "id", 
			description = "Merchant Id", mandatory = true)
	private int merchantId;

	public int getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}

}
