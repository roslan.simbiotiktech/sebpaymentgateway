package com.seb.spg.api.object;

import com.anteater.library.api.formatter.UCaseFormatter;
import com.anteater.library.json.JsonField;

public class OMerchantBase {
	
	@JsonField(
			name = "name", 
			description = "Merchant Name (Used in Payment Request)", 
			minLength = 1, maxLength = 50, mandatory = true, customFormatter = UCaseFormatter.class)
	private String merchantName;
	
	@JsonField(
			name = "description", 
			description = "Merchant Description", 
			maxLength = 500)
	private String description;
	
	@JsonField(
			name = "signature_secret", 
			description = "Merchant Signature Secret", 
			minLength = 1, maxLength = 64, mandatory = true)
	private String signatureSecret;
	
	@JsonField(
			name = "minimum_payment", 
			description = "Merchant Minimum Payment")
	private double minimumPayment;
	
	@JsonField(
			name = "maximum_payment", 
			description = "Merchant Maximum Payment")
	private double maximumPayment;
	
	@JsonField(
			name = "server_to_server_payment_update_url", 
			description = "Merchant Server To Server Payment Update Url",
			minLength = 1, maxLength = 1000, mandatory = true)
	private String serverToServerPaymentUpdateUrl;
	
	@JsonField(
			name = "client_payment_update_url", 
			description = "Merchant Client Payment Update Url",
			minLength = 1, maxLength = 1000)
	private String clientPaymentUpdateUrl;
	
	@JsonField(
			name = "enabled", 
			description = "Merchant Enable Status")
	private boolean enabled;
	
	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSignatureSecret() {
		return signatureSecret;
	}

	public void setSignatureSecret(String signatureSecret) {
		this.signatureSecret = signatureSecret;
	}

	public double getMinimumPayment() {
		return minimumPayment;
	}

	public void setMinimumPayment(double minimumPayment) {
		this.minimumPayment = minimumPayment;
	}

	public double getMaximumPayment() {
		return maximumPayment;
	}

	public void setMaximumPayment(double maximumPayment) {
		this.maximumPayment = maximumPayment;
	}

	public String getServerToServerPaymentUpdateUrl() {
		return serverToServerPaymentUpdateUrl;
	}

	public void setServerToServerPaymentUpdateUrl(String serverToServerPaymentUpdateUrl) {
		this.serverToServerPaymentUpdateUrl = serverToServerPaymentUpdateUrl;
	}

	public String getClientPaymentUpdateUrl() {
		return clientPaymentUpdateUrl;
	}

	public void setClientPaymentUpdateUrl(String clientPaymentUpdateUrl) {
		this.clientPaymentUpdateUrl = clientPaymentUpdateUrl;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
}
