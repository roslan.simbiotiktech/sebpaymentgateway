package com.seb.spg.api.object;

import com.anteater.library.json.JsonField;

public class Paging {
	
	@JsonField(
			name = "page",
			description = "Current Page Number",
			mandatory = false)
	private int page = 1;
	
	@JsonField(
			name = "records_per_page",
			description = "Record to Display Per Page",
			mandatory = false, minNumber = 1)
	private int recordsPerPage = 50;

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRecordsPerPage() {
		return recordsPerPage;
	}

	public void setRecordsPerPage(int recordsPerPage) {
		this.recordsPerPage = recordsPerPage;
	}

}
