package com.seb.spg.api.object;

import com.anteater.library.json.JsonField;

public class OFile {
	
	@JsonField(
			name="name",
			description = "File Name ( include extension )")
	private String name;

	@JsonField(
			name="content",
			description = "File Content ( return file content as ascii, download file content and rename to file name )",
			validateLength = false)
	private String content;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
}
