package com.seb.spg.api.service.gateway;

import java.util.ArrayList;
import java.util.List;

import com.anteater.library.api.APIRequest;
import com.anteater.library.api.Service;
import com.anteater.library.api.ServiceException;
import com.anteater.library.api.ServiceMeta;
import com.anteater.library.jpa.DatabaseFacade;
import com.seb.spg.api.object.OGateway;
import com.seb.spg.api.request.SecureRequest;
import com.seb.spg.api.response.gateway.GatewayListingResponse;
import com.seb.spg.api.service.SecureService;
import com.seb.spg.api.service.ServiceConstants;
import com.seb.spg.jpa.dao.Gateway;
import com.seb.spg.jpa.helper.GatewayHelper;

@Service(
		name = ServiceConstants.API_SERVICE_GATEWAY_LISTING,
		description = "List all the gateways available in the SPG (both active/inactive)")
public class GatewayListingService extends SecureService<SecureRequest, GatewayListingResponse> {

	public GatewayListingService(APIRequest rawRequest, ServiceMeta meta, SecureRequest request) {
		super(rawRequest, meta, request);
	}

	@Override
	public GatewayListingResponse perform() throws ServiceException {
		
		GatewayListingResponse response = new GatewayListingResponse();
		
		try(DatabaseFacade db = new DatabaseFacade()){
			GatewayHelper gatewayHelper = new GatewayHelper(db);
			
			List<Gateway> gateways = gatewayHelper.getGateways();
			List<OGateway> ogateways = new ArrayList<OGateway>();
			
			if(gateways != null){
				for(Gateway gateway : gateways){
					ogateways.add(gatewayHelper.constructGateway(gateway));
				}
			}
			
			OGateway[] ogatewayArr = new OGateway[ogateways.size()];
			ogateways.toArray(ogatewayArr);
			
			response.setGateways(ogatewayArr);
		}
		
		response.getResponseStatus().setSuccess(true);
		return response;
	}
}
