package com.seb.spg.api.service.fpxbank;

import java.util.ArrayList;
import java.util.List;

import com.anteater.library.api.APIRequest;
import com.anteater.library.api.Service;
import com.anteater.library.api.ServiceException;
import com.anteater.library.api.ServiceMeta;
import com.anteater.library.jpa.DatabaseFacade;
import com.seb.spg.api.object.OFpxBank;
import com.seb.spg.api.request.fpxbank.FpxBankListingRequest;
import com.seb.spg.api.response.fpxbank.FpxBankListingResponse;
import com.seb.spg.api.service.SecureService;
import com.seb.spg.api.service.ServiceConstants;
import com.seb.spg.jpa.dao.FpxBank;
import com.seb.spg.jpa.helper.GatewayHelper;

@Service(
		name = ServiceConstants.API_SERVICE_FPX_BANK_LISTING,
		description = "List all the bank available in the FPX (both active/inactive)")
public class FpxBankListingService extends SecureService<FpxBankListingRequest, FpxBankListingResponse> {

	public FpxBankListingService(APIRequest rawRequest, ServiceMeta meta, FpxBankListingRequest request) {
		super(rawRequest, meta, request);
	}

	@Override
	public FpxBankListingResponse perform() throws ServiceException {
		
		FpxBankListingResponse response = new FpxBankListingResponse();
		
		try(DatabaseFacade db = new DatabaseFacade()){
			GatewayHelper gatewayHelper = new GatewayHelper(db);
			
			List<FpxBank> fpxBanks = gatewayHelper.getFpxBanks(request);
			List<OFpxBank> ofpxBanks = new ArrayList<OFpxBank>();
			
			if(fpxBanks != null){
				for(FpxBank fpxBank : fpxBanks){
					ofpxBanks.add(gatewayHelper.constructFpxBank(fpxBank, true));
				}
			}
			
			OFpxBank[] ofpxBankArr = new OFpxBank[ofpxBanks.size()];
			ofpxBanks.toArray(ofpxBankArr);
			
			response.setFpxBanks(ofpxBankArr);
		}
		
		response.getResponseStatus().setSuccess(true);
		return response;
	}
}
