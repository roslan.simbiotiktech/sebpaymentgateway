package com.seb.spg.api.service.report;

import java.util.ArrayList;
import java.util.List;

import com.anteater.library.api.APIRequest;
import com.anteater.library.api.Service;
import com.anteater.library.api.ServiceException;
import com.anteater.library.api.ServiceMeta;
import com.anteater.library.jpa.DatabaseFacade;
import com.anteater.library.jpa.PaginatedList;
import com.seb.spg.api.object.OPaymentReport;
import com.seb.spg.api.request.report.PaymentReportRequest;
import com.seb.spg.api.response.report.PaymentReportResponse;
import com.seb.spg.api.service.SecureService;
import com.seb.spg.api.service.ServiceConstants;
import com.seb.spg.jpa.dao.Payment;
import com.seb.spg.jpa.helper.ReportHelper;

@Service(
		name = ServiceConstants.API_SERVICE_PAYMENT_REPORT,
		description = "Payment Report")
public class PaymentReportService extends SecureService<PaymentReportRequest, PaymentReportResponse> {

	public PaymentReportService(APIRequest rawRequest, ServiceMeta meta, PaymentReportRequest request) {
		super(rawRequest, meta, request);
	}

	@Override
	public PaymentReportResponse perform() throws ServiceException {
		
		PaymentReportResponse response = new PaymentReportResponse();
		
		try(DatabaseFacade db = new DatabaseFacade()){
			ReportHelper reportHelper = new ReportHelper(db);
			
			PaginatedList<Payment> paymentReports = reportHelper.getPaymentReport(request);
			List<OPaymentReport> opaymentReports = new ArrayList<OPaymentReport>();
			
			if(paymentReports != null & paymentReports.getList() != null){
				for(Payment payment : paymentReports.getList()){
					opaymentReports.add(reportHelper.constructPaymentReport(payment, getRequestLocale()));
				}
			}
			
			OPaymentReport[] opaymentReportArr = new OPaymentReport[opaymentReports.size()];
			opaymentReports.toArray(opaymentReportArr);
			
			response.setPaymentReports(opaymentReportArr);
			response.setPagination(request.getPaging(), paymentReports.getTotalRecord());
		}
		
		response.getResponseStatus().setSuccess(true);
		return response;
	}
}
