package com.seb.spg.api.service.gateway;

import java.util.List;

import com.anteater.library.api.APIRequest;
import com.anteater.library.api.Service;
import com.anteater.library.api.ServiceException;
import com.anteater.library.api.ServiceMeta;
import com.anteater.library.jpa.DatabaseFacade;
import com.seb.spg.api.object.OConfig;
import com.seb.spg.api.request.SecureRequest;
import com.seb.spg.api.response.gateway.ConfigQueryResponse;
import com.seb.spg.api.service.SecureService;
import com.seb.spg.api.service.ServiceConstants;
import com.seb.spg.jpa.dao.Config;
import com.seb.spg.jpa.helper.GatewayHelper;

@Service(
		name = ServiceConstants.API_SERVICE_CONFIG_QUERY,
		description = "SPG gateway config query")
public class ConfigQueryService extends SecureService<SecureRequest, ConfigQueryResponse> {

	public ConfigQueryService(APIRequest rawRequest, ServiceMeta meta, SecureRequest request) {
		super(rawRequest, meta, request);
	}

	@Override
	public ConfigQueryResponse perform() throws ServiceException {
		
		ConfigQueryResponse response = new ConfigQueryResponse();
		
		try(DatabaseFacade db = new DatabaseFacade()){
			GatewayHelper gatewayHelper = new GatewayHelper(db);
			
			List<Config> configs = gatewayHelper.getConfigs();
			
			OConfig oconfig = gatewayHelper.constructConfig(configs);
			
			response.setConfig(oconfig);
		}
		
		response.getResponseStatus().setSuccess(true);
		return response;
	}
}
