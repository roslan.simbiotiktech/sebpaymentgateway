package com.seb.spg.api.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.anteater.library.api.APIRequest;
import com.anteater.library.api.ServiceBase;
import com.anteater.library.api.ServiceMeta;
import com.anteater.library.api.service.response.ServiceResponse;
import com.anteater.library.jpa.DatabaseFacade;
import com.seb.spg.api.request.SecureRequest;
import com.seb.spg.jpa.dao.ApiUser;
import com.seb.spg.util.CryptographyUtil;

public abstract class SecureService <T extends SecureRequest, K extends ServiceResponse> extends ServiceBase<T, K>{

	private static final Logger logger = LogManager.getLogger(SecureService.class);
	
	protected ApiUser apiUser;
	
	public SecureService(APIRequest rawRequest, ServiceMeta meta, T request) {
		super(rawRequest, meta, request);
	}
	
	@Override
	public boolean isAuthorized(){
		try(DatabaseFacade db = new DatabaseFacade()){
			
			Session session = db.getSession();
			Criteria criteria = session.createCriteria(ApiUser.class);
			criteria.add(Restrictions.eq("userId", request.getUserId()));
			criteria.add(Restrictions.eq("password", CryptographyUtil.toSha512Hexadecimal(request.getPassword())));
			apiUser = db.queryUnique(criteria);
			
			if(apiUser == null){
				logger.error("incorrect user_id or password");
				return false;
			}else{
				return true;
			}
		}
	}
	
	@Override
	protected void finalize(ServiceResponse k) {
	}
}
