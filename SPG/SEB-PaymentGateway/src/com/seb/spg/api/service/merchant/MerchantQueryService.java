package com.seb.spg.api.service.merchant;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.anteater.library.api.APIRequest;
import com.anteater.library.api.Service;
import com.anteater.library.api.ServiceException;
import com.anteater.library.api.ServiceMeta;
import com.anteater.library.api.utility.ResourceUtil;
import com.anteater.library.jpa.DatabaseFacade;
import com.seb.spg.api.object.OMerchant;
import com.seb.spg.api.request.merchant.MerchantQueryRequest;
import com.seb.spg.api.response.merchant.MerchantQueryResponse;
import com.seb.spg.api.service.SecureService;
import com.seb.spg.api.service.ServiceConstants;
import com.seb.spg.jpa.dao.Merchant;
import com.seb.spg.jpa.helper.MerchantHelper;

@Service(
		name = ServiceConstants.API_SERVICE_MERCHANT_QUERY,
		description = "Merchant query")
public class MerchantQueryService extends SecureService<MerchantQueryRequest, MerchantQueryResponse> {

	private static final Logger logger = LogManager.getLogger(MerchantQueryService.class);
	
	public MerchantQueryService(APIRequest rawRequest, ServiceMeta meta, MerchantQueryRequest request) {
		super(rawRequest, meta, request);
	}

	@Override
	public MerchantQueryResponse perform() throws ServiceException {
		
		MerchantQueryResponse response = new MerchantQueryResponse();
		
		try(DatabaseFacade db = new DatabaseFacade()){
			MerchantHelper merchantHelper = new MerchantHelper(db);
			
			int merchantId = request.getMerchantId();
			
			Merchant merchant = merchantHelper.getMerchant(merchantId);
			
			if(merchant == null){
				logger.error("Record Not Found Merchant Id - " + merchantId);
				ServiceException ex = new ServiceException(
						ResourceUtil.get("error.record_not_found", 
								getRequestLocale()));
				throw ex;
			}
			
			OMerchant omerchant = merchantHelper.constructMerchant(merchant);
			
			response.setMerchant(omerchant);
		}
		
		response.getResponseStatus().setSuccess(true);
		return response;
	}
}
