package com.seb.spg.api.service.merchant;

import java.util.ArrayList;
import java.util.List;

import com.anteater.library.api.APIRequest;
import com.anteater.library.api.Service;
import com.anteater.library.api.ServiceException;
import com.anteater.library.api.ServiceMeta;
import com.anteater.library.jpa.DatabaseFacade;
import com.seb.spg.api.object.OMerchant;
import com.seb.spg.api.request.SecureRequest;
import com.seb.spg.api.response.merchant.MerchantListingResponse;
import com.seb.spg.api.service.SecureService;
import com.seb.spg.api.service.ServiceConstants;
import com.seb.spg.jpa.dao.Merchant;
import com.seb.spg.jpa.helper.MerchantHelper;

@Service(
		name = ServiceConstants.API_SERVICE_MERCHANT_LISTING,
		description = "List all the merchants available in the SPG (both active/inactive)")
public class MerchantListingService extends SecureService<SecureRequest, MerchantListingResponse> {

	public MerchantListingService(APIRequest rawRequest, ServiceMeta meta, SecureRequest request) {
		super(rawRequest, meta, request);
	}

	@Override
	public MerchantListingResponse perform() throws ServiceException {
		
		MerchantListingResponse response = new MerchantListingResponse();
		
		try(DatabaseFacade db = new DatabaseFacade()){
			MerchantHelper merchantHelper = new MerchantHelper(db);
			
			List<Merchant> merchants = merchantHelper.listMerchant();
			List<OMerchant> omerchants = new ArrayList<OMerchant>();
			
			for(Merchant merchant : merchants){
				omerchants.add(merchantHelper.constructMerchant(merchant));
			}
			
			OMerchant[] omerchantArr = new OMerchant[omerchants.size()];
			omerchants.toArray(omerchantArr);
			
			response.setMerchants(omerchantArr);
		}
		
		response.getResponseStatus().setSuccess(true);
		return response;
	}
}
