package com.seb.spg.api.service.gateway;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.anteater.library.api.APIRequest;
import com.anteater.library.api.Service;
import com.anteater.library.api.ServiceException;
import com.anteater.library.api.ServiceMeta;
import com.anteater.library.api.utility.ResourceUtil;
import com.anteater.library.jpa.DatabaseFacade;
import com.anteater.library.jpa.DatabaseFacadeException;
import com.seb.spg.api.request.gateway.ConfigUpdateRequest;
import com.seb.spg.api.response.gateway.ConfigQueryResponse;
import com.seb.spg.api.service.SecureService;
import com.seb.spg.api.service.ServiceConstants;
import com.seb.spg.jpa.dao.Config;
import com.seb.spg.jpa.helper.GatewayHelper;

@Service(
		name = ServiceConstants.API_SERVICE_CONFIG_UPDATE,
		description = "SPG gateway config update")
public class ConfigUpdateService extends SecureService<ConfigUpdateRequest, ConfigQueryResponse> {

	private static final Logger logger = LogManager.getLogger(ConfigUpdateService.class);
	
	public ConfigUpdateService(APIRequest rawRequest, ServiceMeta meta, ConfigUpdateRequest request) {
		super(rawRequest, meta, request);
	}

	@Override
	public ConfigQueryResponse perform() throws ServiceException {
		
		ConfigQueryResponse response = new ConfigQueryResponse();
		
		try(DatabaseFacade db = new DatabaseFacade()){
			db.beginTransaction();
			
			GatewayHelper gatewayHelper = new GatewayHelper(db);
			gatewayHelper.manageConfig(request.getConfig(), request.getPerformerId());
			
			db.commit();
			
			List<Config> configs = gatewayHelper.getConfigs();
			
			response.setConfig(gatewayHelper.constructConfig(configs));
			response.getResponseStatus().setSuccess(true);
		}catch(DatabaseFacadeException e){
			logger.error("Error update config", e);
			ServiceException ex = new ServiceException(e, 
					ResourceUtil.get("error.general_500", 
							getRequestLocale()));
			throw ex;
		}
		
		return response;
	}
}
