package com.seb.spg.api.service.merchant;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.anteater.library.api.APIRequest;
import com.anteater.library.api.Service;
import com.anteater.library.api.ServiceException;
import com.anteater.library.api.ServiceMeta;
import com.anteater.library.api.utility.ResourceUtil;
import com.anteater.library.jpa.DatabaseFacade;
import com.anteater.library.jpa.DatabaseFacadeException;
import com.seb.spg.api.request.merchant.MerchantCreateRequest;
import com.seb.spg.api.response.merchant.MerchantQueryResponse;
import com.seb.spg.api.service.SecureService;
import com.seb.spg.api.service.ServiceConstants;
import com.seb.spg.jpa.dao.Merchant;
import com.seb.spg.jpa.helper.MerchantHelper;

@Service(
		name = ServiceConstants.API_SERVICE_MERCHANT_CREATE,
		description = "Create merchant in the SPG")
public class MerchantCreateService extends SecureService<MerchantCreateRequest, MerchantQueryResponse> {

	private static final Logger logger = LogManager.getLogger(MerchantCreateService.class);
	
	public MerchantCreateService(APIRequest rawRequest, ServiceMeta meta, MerchantCreateRequest request) {
		super(rawRequest, meta, request);
	}

	@Override
	public MerchantQueryResponse perform() throws ServiceException {
		
		MerchantQueryResponse response = new MerchantQueryResponse();
		
		try(DatabaseFacade db = new DatabaseFacade()){
			db.beginTransaction();
			
			MerchantHelper merchantHelper = new MerchantHelper(db);
			
			String merchantName = request.getOmerchant().getMerchantName();
			Merchant merchantExist = merchantHelper.getMerchant(merchantName);
			if(merchantExist != null){
				logger.error("Merchant {} exist! ", merchantName);
				ServiceException ex = new ServiceException(
						ResourceUtil.get("error.merchant_exist", 
								getRequestLocale(), merchantName));
				throw ex;
			}
			
			Merchant merchant = merchantHelper.createMerchant(request.getOmerchant(), request.getPerformerId());
			db.insert(merchant);
			db.commit();
			
			response.setMerchant(merchantHelper.constructMerchant(merchant));
			response.getResponseStatus().setSuccess(true);
		}catch(DatabaseFacadeException e){
			logger.error("Error create merchant ", e);
			ServiceException ex = new ServiceException(e, 
					ResourceUtil.get("error.general_500", 
							getRequestLocale()));
			throw ex;
		}
		
		return response;
	}
}
