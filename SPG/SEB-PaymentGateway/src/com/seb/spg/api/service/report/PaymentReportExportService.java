package com.seb.spg.api.service.report;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.anteater.library.api.APIRequest;
import com.anteater.library.api.Service;
import com.anteater.library.api.ServiceException;
import com.anteater.library.api.ServiceMeta;
import com.anteater.library.api.utility.ResourceUtil;
import com.anteater.library.jpa.DatabaseFacade;
import com.seb.spg.api.object.OFileEncode;
import com.seb.spg.api.request.report.PaymentReportExportRequest;
import com.seb.spg.api.response.report.PaymentReportExportResponse;
import com.seb.spg.api.service.SecureService;
import com.seb.spg.api.service.ServiceConstants;
import com.seb.spg.jpa.dao.Payment;
import com.seb.spg.jpa.helper.GatewayHelper;
import com.seb.spg.jpa.helper.ReportHelper;
import com.seb.spg.util.StringUtil;

@Service(
		name = ServiceConstants.API_SERVICE_PAYMENT_REPORT_EXPORT,
		description = "Payment Report Export")
public class PaymentReportExportService extends SecureService<PaymentReportExportRequest, PaymentReportExportResponse> {
	
	private static final Logger logger = LogManager.getLogger(PaymentReportExportService.class);
	
	public PaymentReportExportService(APIRequest rawRequest, ServiceMeta meta, PaymentReportExportRequest request) {
		super(rawRequest, meta, request);
	}

	@Override
	public PaymentReportExportResponse perform() throws ServiceException {
		
		PaymentReportExportResponse response = new PaymentReportExportResponse();
		
		try(DatabaseFacade db = new DatabaseFacade()){
			ReportHelper reportHelper = new ReportHelper(db);
			
			List<Payment> paymentReports = reportHelper.getPaymentReportExport(request);
			
			if(paymentReports != null){
				String header = "Created Date,Payment Method,Payment Status,Client Name,Payment Reference ID,User Email,FPX Transaction ID,FPX Transaction Status,Maybank Reference No,Maybank Status";
				StringBuffer sb = new StringBuffer();
				sb.append(header);
				
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
				
				for(Payment payment : paymentReports){
					
					String paymentMethod = payment.getGatewayId();
					String transactionId = !StringUtil.isEmpty(payment.getGatewayTransactionId()) ? payment.getGatewayTransactionId() : "";
					String transactionStatus = payment.getGatewayStatus() != null ? payment.getGatewayStatus().getStatus() : "";
					String fpxTransactionId = "";
					String fpxTransactionStatus = "";
					String mbbTransactionId = "";
					String mbbTransactionStatus = "";
					boolean isFpx = GatewayHelper.isFpx(payment.getGatewayId());
					boolean isMbb = GatewayHelper.isMbb(payment.getGatewayId());
					
					if(isMbb){
						paymentMethod = "Credit/Debit Card";
						mbbTransactionId = transactionId;
						mbbTransactionStatus = transactionStatus;
					}else if(isFpx){
						fpxTransactionId = transactionId;
						fpxTransactionStatus = transactionStatus;
					}
					
					sb.append("\n");
					sb.append(sdf.format(payment.getCreatedDatetime()) + ",");
					sb.append(paymentMethod + ",");
					sb.append(payment.getStatus() + ",");
					sb.append(payment.getCustomerUsername() + ",");
					sb.append(payment.getMerchantTransactionId() + ",");
					sb.append(payment.getCustomerEmail() + ",");
					sb.append(fpxTransactionId + ",");
					sb.append(fpxTransactionStatus + ",");
					sb.append(mbbTransactionId + ",");
					sb.append(mbbTransactionStatus);
				}
				
				OFileEncode ofile = new OFileEncode();
				ofile.setName("SPG Transaction Report.csv");
				try {
					ofile.setContent(new String(Base64.encodeBase64(sb.toString().getBytes()), "UTF-8"));
				} catch (UnsupportedEncodingException e) {
					logger.error("Error Generate SPG Transaction Report", e);
					
					ServiceException ex = new ServiceException(
							ResourceUtil.get("error.generate_spg_transaction_report", 
									getRequestLocale()));
					throw ex;
				}
				
				response.setPaymentReportExcelFile(ofile);
			}
			
		}
		
		response.getResponseStatus().setSuccess(true);
		return response;
	}
}
