package com.seb.spg.api.service.fpxbank;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.anteater.library.api.APIRequest;
import com.anteater.library.api.Service;
import com.anteater.library.api.ServiceException;
import com.anteater.library.api.ServiceMeta;
import com.anteater.library.api.utility.ResourceUtil;
import com.anteater.library.jpa.DatabaseFacade;
import com.anteater.library.jpa.DatabaseFacadeException;
import com.seb.spg.api.request.fpxbank.FpxBankUpdateRequest;
import com.seb.spg.api.response.fpxbank.FpxBankQueryResponse;
import com.seb.spg.api.service.SecureService;
import com.seb.spg.api.service.ServiceConstants;
import com.seb.spg.jpa.dao.FpxBank;
import com.seb.spg.jpa.helper.GatewayHelper;

@Service(
		name = ServiceConstants.API_SERVICE_FPX_BANK_UPDATE,
		description = "Update FPX Bank in the SPG")
public class FpxBankUpdateService extends SecureService<FpxBankUpdateRequest, FpxBankQueryResponse> {

	private static final Logger logger = LogManager.getLogger(FpxBankUpdateService.class);
	
	public FpxBankUpdateService(APIRequest rawRequest, ServiceMeta meta, FpxBankUpdateRequest request) {
		super(rawRequest, meta, request);
	}

	@Override
	public FpxBankQueryResponse perform() throws ServiceException {
		
		FpxBankQueryResponse response = new FpxBankQueryResponse();
		
		try(DatabaseFacade db = new DatabaseFacade()){
			db.beginTransaction();
			
			GatewayHelper gatewayHelper = new GatewayHelper(db);
			
			FpxBank fpxBank = gatewayHelper.getFpxBank(request.getOfpxBank().getBankId());

			if(fpxBank != null){
				fpxBank = gatewayHelper.manageFpxBank(request.getOfpxBank(), fpxBank, request.getPerformerId());
				db.update(fpxBank);
			}
			
			db.commit();
			
			response.setFpxBank(gatewayHelper.constructFpxBank(fpxBank));
			response.getResponseStatus().setSuccess(true);
		}catch(DatabaseFacadeException e){
			logger.error("Error update fpxBank - " + request.getOfpxBank().getBankId(), e);
			ServiceException ex = new ServiceException(e, 
					ResourceUtil.get("error.general_500", 
							getRequestLocale()));
			throw ex;
		}
		
		return response;
	}
	
}
