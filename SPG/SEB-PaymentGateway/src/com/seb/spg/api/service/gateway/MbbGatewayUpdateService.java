package com.seb.spg.api.service.gateway;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.anteater.library.api.APIRequest;
import com.anteater.library.api.Service;
import com.anteater.library.api.ServiceException;
import com.anteater.library.api.ServiceMeta;
import com.anteater.library.api.utility.ResourceUtil;
import com.anteater.library.jpa.DatabaseFacade;
import com.anteater.library.jpa.DatabaseFacadeException;
import com.seb.spg.api.request.gateway.MbbGatewayUpdateRequest;
import com.seb.spg.api.response.gateway.MbbGatewayQueryResponse;
import com.seb.spg.api.service.SecureService;
import com.seb.spg.api.service.ServiceConstants;
import com.seb.spg.gateway.mbb.MaybankPaymentGateway;
import com.seb.spg.jpa.dao.Gateway;
import com.seb.spg.jpa.helper.GatewayHelper;

@Service(
		name = ServiceConstants.API_SERVICE_GATEWAY_MBB_UPDATE,
		description = "Update Maybank gateway details in the SPG")
public class MbbGatewayUpdateService extends SecureService<MbbGatewayUpdateRequest, MbbGatewayQueryResponse> {

	private static final Logger logger = LogManager.getLogger(MbbGatewayUpdateService.class);
	
	public MbbGatewayUpdateService(APIRequest rawRequest, ServiceMeta meta, MbbGatewayUpdateRequest request) {
		super(rawRequest, meta, request);
	}

	@Override
	public MbbGatewayQueryResponse perform() throws ServiceException {
		
		MbbGatewayQueryResponse response = new MbbGatewayQueryResponse();
		
		try(DatabaseFacade db = new DatabaseFacade()){
			db.beginTransaction();
			
			GatewayHelper gatewayHelper = new GatewayHelper(db);
			
			Gateway gateway = gatewayHelper.getGateway(MaybankPaymentGateway.GATEWAY_ID);

			if(gateway == null){
				logger.error("Record Not Found Gateway Id - " + MaybankPaymentGateway.GATEWAY_ID);
				ServiceException ex = new ServiceException(
						ResourceUtil.get("error.record_not_found", 
								getRequestLocale()));
				throw ex;
			}
			
			gatewayHelper.manageMbbGateway(MaybankPaymentGateway.GATEWAY_ID, request.getMbb(), request.getPerformerId());
			gatewayHelper.manageGateway(gateway, request.getMbb(), request.getPerformerId());
			
			db.commit();
			
			gateway = gatewayHelper.getGateway(MaybankPaymentGateway.GATEWAY_ID);
			
			response.setMbb(gatewayHelper.constructMbbGateway(gateway));
			response.getResponseStatus().setSuccess(true);
		}catch(DatabaseFacadeException e){
			logger.error("Error update gateway - " + MaybankPaymentGateway.GATEWAY_ID, e);
			ServiceException ex = new ServiceException(e, 
					ResourceUtil.get("error.general_500", 
							getRequestLocale()));
			throw ex;
		}
		
		return response;
	}
}
