package com.seb.spg.api.service;

public class ServiceConstants {

	public static final String API_SERVICE_MERCHANT_LISTING = "merchant_list";
	
	public static final String API_SERVICE_MERCHANT_QUERY = "merchant_query";
	
	public static final String API_SERVICE_MERCHANT_UPDATE = "merchant_update";
	
	public static final String API_SERVICE_MERCHANT_CREATE = "merchant_create";
	
	public static final String API_SERVICE_GATEWAY_LISTING = "gateway_list";
	
	public static final String API_SERVICE_GATEWAY_FPX_QUERY = "gateway_fpx_query";
	
	public static final String API_SERVICE_GATEWAY_FPX_UPDATE = "gateway_fpx_update";
	
	public static final String API_SERVICE_GATEWAY_FPX_GENERATE_CSR = "gateway_fpx_generate_csr";
	
	public static final String API_SERVICE_GATEWAY_MBB_QUERY = "gateway_mbb_query";
	
	public static final String API_SERVICE_GATEWAY_MBB_UPDATE = "gateway_mbb_update";
	
	public static final String API_SERVICE_FPX_BANK_LISTING = "fpx_bank_list";
	
	public static final String API_SERVICE_FPX_BANK_QUERY = "fpx_bank_query";
	
	public static final String API_SERVICE_FPX_BANK_UPDATE = "fpx_bank_update";
	
	public static final String API_SERVICE_CONFIG_QUERY = "config_query";
	
	public static final String API_SERVICE_CONFIG_UPDATE = "config_update";
	
	public static final String API_SERVICE_PAYMENT_REPORT = "payment_report";
	
	public static final String API_SERVICE_PAYMENT_REPORT_EXPORT = "payment_report_export";
	
	public static final String API_SERVICE_PAYMENT_DETAIL_REPORT = "payment_detail_report";
	
}
