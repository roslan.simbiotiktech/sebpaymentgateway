package com.seb.spg.api.service.gateway;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.anteater.library.api.APIRequest;
import com.anteater.library.api.Service;
import com.anteater.library.api.ServiceException;
import com.anteater.library.api.ServiceMeta;
import com.anteater.library.api.utility.ResourceUtil;
import com.anteater.library.jpa.DatabaseFacade;
import com.seb.spg.api.object.OFpx;
import com.seb.spg.api.request.SecureRequest;
import com.seb.spg.api.response.gateway.FpxGatewayQueryResponse;
import com.seb.spg.api.service.SecureService;
import com.seb.spg.api.service.ServiceConstants;
import com.seb.spg.gateway.fpx.FpxPaymentGateway;
import com.seb.spg.jpa.dao.Gateway;
import com.seb.spg.jpa.helper.GatewayHelper;

@Service(
		name = ServiceConstants.API_SERVICE_GATEWAY_FPX_QUERY,
		description = "FPX gateway query")
public class FpxGatewayQueryService extends SecureService<SecureRequest, FpxGatewayQueryResponse> {

	private static final Logger logger = LogManager.getLogger(FpxGatewayQueryService.class);
	
	public FpxGatewayQueryService(APIRequest rawRequest, ServiceMeta meta, SecureRequest request) {
		super(rawRequest, meta, request);
	}

	@Override
	public FpxGatewayQueryResponse perform() throws ServiceException {
		
		FpxGatewayQueryResponse response = new FpxGatewayQueryResponse();
		
		try(DatabaseFacade db = new DatabaseFacade()){
			GatewayHelper gatewayHelper = new GatewayHelper(db);			
			Gateway gateway = gatewayHelper.getGateway(FpxPaymentGateway.GATEWAY_ID);
			
			if(gateway == null){
				logger.error("Record Not Found Gateway Id - " + FpxPaymentGateway.GATEWAY_ID);
				ServiceException ex = new ServiceException(
						ResourceUtil.get("error.record_not_found", 
								getRequestLocale()));
				throw ex;
			}
			
			OFpx ofpx = gatewayHelper.constructFpxGateway(gateway);
			
			response.setFpx(ofpx);
		}
		
		response.getResponseStatus().setSuccess(true);
		return response;
	}
}
