package com.seb.spg.api.service.gateway;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.anteater.library.api.APIRequest;
import com.anteater.library.api.Service;
import com.anteater.library.api.ServiceException;
import com.anteater.library.api.ServiceMeta;
import com.anteater.library.api.utility.ResourceUtil;
import com.anteater.library.jpa.DatabaseFacade;
import com.seb.spg.api.object.OMbb;
import com.seb.spg.api.request.SecureRequest;
import com.seb.spg.api.response.gateway.MbbGatewayQueryResponse;
import com.seb.spg.api.service.SecureService;
import com.seb.spg.api.service.ServiceConstants;
import com.seb.spg.gateway.mbb.MaybankPaymentGateway;
import com.seb.spg.jpa.dao.Gateway;
import com.seb.spg.jpa.helper.GatewayHelper;

@Service(
		name = ServiceConstants.API_SERVICE_GATEWAY_MBB_QUERY,
		description = "Maybank gateway query")
public class MbbGatewayQueryService extends SecureService<SecureRequest, MbbGatewayQueryResponse> {

	private static final Logger logger = LogManager.getLogger(MbbGatewayQueryService.class);
	
	public MbbGatewayQueryService(APIRequest rawRequest, ServiceMeta meta, SecureRequest request) {
		super(rawRequest, meta, request);
	}

	@Override
	public MbbGatewayQueryResponse perform() throws ServiceException {
		
		MbbGatewayQueryResponse response = new MbbGatewayQueryResponse();
		
		try(DatabaseFacade db = new DatabaseFacade()){
			GatewayHelper gatewayHelper = new GatewayHelper(db);
			
			Gateway gateway = gatewayHelper.getGateway(MaybankPaymentGateway.GATEWAY_ID);
			
			if(gateway == null){
				logger.error("Record Not Found Gateway Id - " + MaybankPaymentGateway.GATEWAY_ID);
				ServiceException ex = new ServiceException(
						ResourceUtil.get("error.record_not_found", 
								getRequestLocale()));
				throw ex;
			}
			
			OMbb ombb = gatewayHelper.constructMbbGateway(gateway);
			
			response.setMbb(ombb);
		}
		
		response.getResponseStatus().setSuccess(true);
		return response;
	}
}
