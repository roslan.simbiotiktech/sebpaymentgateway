package com.seb.spg.api.service.merchant;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.anteater.library.api.APIRequest;
import com.anteater.library.api.Service;
import com.anteater.library.api.ServiceException;
import com.anteater.library.api.ServiceMeta;
import com.anteater.library.api.utility.ResourceUtil;
import com.anteater.library.jpa.DatabaseFacade;
import com.anteater.library.jpa.DatabaseFacadeException;
import com.seb.spg.api.request.merchant.MerchantUpdateRequest;
import com.seb.spg.api.response.merchant.MerchantQueryResponse;
import com.seb.spg.api.service.SecureService;
import com.seb.spg.api.service.ServiceConstants;
import com.seb.spg.jpa.dao.Merchant;
import com.seb.spg.jpa.helper.MerchantHelper;

@Service(
		name = ServiceConstants.API_SERVICE_MERCHANT_UPDATE,
		description = "Update merchant in the SPG")
public class MerchantUpdateService extends SecureService<MerchantUpdateRequest, MerchantQueryResponse> {

	private static final Logger logger = LogManager.getLogger(MerchantUpdateService.class);
	
	public MerchantUpdateService(APIRequest rawRequest, ServiceMeta meta, MerchantUpdateRequest request) {
		super(rawRequest, meta, request);
	}

	@Override
	public MerchantQueryResponse perform() throws ServiceException {
		
		MerchantQueryResponse response = new MerchantQueryResponse();
		
		try(DatabaseFacade db = new DatabaseFacade()){
			db.beginTransaction();
			
			MerchantHelper merchantHelper = new MerchantHelper(db);
			
			Merchant merchant = merchantHelper.getMerchant(request.getOmerchant().getMerchantId());

			if(merchant != null){
				merchant = merchantHelper.manageMerchant(request.getOmerchant(), merchant, request.getPerformerId());
				db.update(merchant);
			}
			
			db.commit();
			
			response.setMerchant(merchantHelper.constructMerchant(merchant));
			response.getResponseStatus().setSuccess(true);
		}catch(DatabaseFacadeException e){
			logger.error("Error update merchant - " + request.getOmerchant().getMerchantId(), e);
			ServiceException ex = new ServiceException(e, 
					ResourceUtil.get("error.general_500", 
							getRequestLocale()));
			throw ex;
		}
		
		return response;
	}
}
