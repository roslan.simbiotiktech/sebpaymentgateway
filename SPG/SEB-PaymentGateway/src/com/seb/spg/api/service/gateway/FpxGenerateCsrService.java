package com.seb.spg.api.service.gateway;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.anteater.library.api.APIRequest;
import com.anteater.library.api.Service;
import com.anteater.library.api.ServiceException;
import com.anteater.library.api.ServiceMeta;
import com.anteater.library.api.utility.ResourceUtil;
import com.anteater.library.jpa.DatabaseFacade;
import com.seb.spg.api.object.OFile;
import com.seb.spg.api.request.SecureRequest;
import com.seb.spg.api.response.gateway.FpxGenerateCsrResponse;
import com.seb.spg.api.service.SecureService;
import com.seb.spg.api.service.ServiceConstants;
import com.seb.spg.gateway.exception.InvalidFpxPkiException;
import com.seb.spg.gateway.fpx.FpxPaymentGateway;
import com.seb.spg.gateway.fpx.FpxPkiImplementation;
import com.seb.spg.jpa.helper.GatewayHelper;

@Service(
		name = ServiceConstants.API_SERVICE_GATEWAY_FPX_GENERATE_CSR,
		description = "FPX generate CSR")
public class FpxGenerateCsrService extends SecureService<SecureRequest, FpxGenerateCsrResponse> {

	private static final Logger logger = LogManager.getLogger(FpxGenerateCsrService.class);
	
	public FpxGenerateCsrService(APIRequest rawRequest, ServiceMeta meta, SecureRequest request) {
		super(rawRequest, meta, request);
	}

	@Override
	public FpxGenerateCsrResponse perform() throws ServiceException {
		
		FpxGenerateCsrResponse response = new FpxGenerateCsrResponse();
		
		try(DatabaseFacade db = new DatabaseFacade()){
			GatewayHelper gatewayHelper = new GatewayHelper(db);
			
			OFile ofile = new OFile();
			try {
				ofile.setContent(FpxPkiImplementation.generateCsr());
			} catch (InvalidFpxPkiException e) {
				logger.error("Error Generate FPX CSR", e);
				
				ServiceException ex = new ServiceException(
						ResourceUtil.get("error.generate_fpx_csr", 
								getRequestLocale()));
				throw ex;
			}
			ofile.setName(gatewayHelper.getGatewayConfigValue(FpxPaymentGateway.GATEWAY_ID,
					GatewayHelper.FPX_GATEWAY_CONFIG_MERCHANT_ID) + ".csr");
			
			response.setFile(ofile);
		}
		
		response.getResponseStatus().setSuccess(true);
		return response;
	}
}
