package com.seb.spg.api.service.gateway;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.anteater.library.api.APIRequest;
import com.anteater.library.api.Service;
import com.anteater.library.api.ServiceException;
import com.anteater.library.api.ServiceMeta;
import com.anteater.library.api.utility.ResourceUtil;
import com.anteater.library.jpa.DatabaseFacade;
import com.anteater.library.jpa.DatabaseFacadeException;
import com.seb.spg.api.request.gateway.FpxGatewayUpdateRequest;
import com.seb.spg.api.response.gateway.FpxGatewayQueryResponse;
import com.seb.spg.api.service.SecureService;
import com.seb.spg.api.service.ServiceConstants;
import com.seb.spg.gateway.fpx.FpxPaymentGateway;
import com.seb.spg.jpa.dao.Gateway;
import com.seb.spg.jpa.helper.GatewayHelper;

@Service(
		name = ServiceConstants.API_SERVICE_GATEWAY_FPX_UPDATE,
		description = "Update FPX gateway details in the SPG")
public class FpxGatewayUpdateService extends SecureService<FpxGatewayUpdateRequest, FpxGatewayQueryResponse> {

	private static final Logger logger = LogManager.getLogger(FpxGatewayUpdateService.class);
	
	public FpxGatewayUpdateService(APIRequest rawRequest, ServiceMeta meta, FpxGatewayUpdateRequest request) {
		super(rawRequest, meta, request);
	}

	@Override
	public FpxGatewayQueryResponse perform() throws ServiceException {
		
		FpxGatewayQueryResponse response = new FpxGatewayQueryResponse();
		
		String gatewayId = FpxPaymentGateway.GATEWAY_ID;
		
		try(DatabaseFacade db = new DatabaseFacade()){
			db.beginTransaction();
			
			GatewayHelper gatewayHelper = new GatewayHelper(db);
			
			Gateway gateway = gatewayHelper.getGateway(gatewayId);

			if(gateway == null){
				logger.error("Record Not Found Gateway Id - " + gatewayId);
				ServiceException ex = new ServiceException(
						ResourceUtil.get("error.record_not_found", 
								getRequestLocale()));
				throw ex;
			}
			
			gatewayHelper.manageFpxGateway(gatewayId, request.getFpx(), request.getPerformerId());
			gatewayHelper.manageGateway(gateway, request.getFpx(), request.getPerformerId());
			
			db.commit();
			
			gateway = gatewayHelper.getGateway(gatewayId);
			
			response.setFpx(gatewayHelper.constructFpxGateway(gateway));
			response.getResponseStatus().setSuccess(true);
		}catch(DatabaseFacadeException e){
			logger.error("Error update gateway - " + gatewayId, e);
			ServiceException ex = new ServiceException(e, 
					ResourceUtil.get("error.general_500", 
							getRequestLocale()));
			throw ex;
		}
		
		return response;
	}
}
