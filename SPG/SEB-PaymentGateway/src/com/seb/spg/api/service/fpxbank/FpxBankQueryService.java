package com.seb.spg.api.service.fpxbank;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.anteater.library.api.APIRequest;
import com.anteater.library.api.Service;
import com.anteater.library.api.ServiceException;
import com.anteater.library.api.ServiceMeta;
import com.anteater.library.api.utility.ResourceUtil;
import com.anteater.library.jpa.DatabaseFacade;
import com.seb.spg.api.object.OFpxBank;
import com.seb.spg.api.request.fpxbank.FpxBankDetailRequest;
import com.seb.spg.api.response.fpxbank.FpxBankQueryResponse;
import com.seb.spg.api.service.SecureService;
import com.seb.spg.api.service.ServiceConstants;
import com.seb.spg.jpa.dao.FpxBank;
import com.seb.spg.jpa.helper.GatewayHelper;

@Service(
		name = ServiceConstants.API_SERVICE_FPX_BANK_QUERY,
		description = "FPX bank query")
public class FpxBankQueryService extends SecureService<FpxBankDetailRequest, FpxBankQueryResponse> {

	private static final Logger logger = LogManager.getLogger(FpxBankQueryService.class);
	
	public FpxBankQueryService(APIRequest rawRequest, ServiceMeta meta, FpxBankDetailRequest request) {
		super(rawRequest, meta, request);
	}

	@Override
	public FpxBankQueryResponse perform() throws ServiceException {
		
		FpxBankQueryResponse response = new FpxBankQueryResponse();
		
		try(DatabaseFacade db = new DatabaseFacade()){
			
			GatewayHelper gatewayHelper = new GatewayHelper(db);
			
			int bankId = request.getBankId();
			
			FpxBank fpxBank = gatewayHelper.getFpxBank(bankId);
			
			if(fpxBank == null){
				logger.error("Record Not Found Bank Id - " + bankId);
				ServiceException ex = new ServiceException(
						ResourceUtil.get("error.record_not_found", 
								getRequestLocale()));
				throw ex;
			}
			
			OFpxBank ofpxBank = gatewayHelper.constructFpxBank(fpxBank);
			
			response.setFpxBank(ofpxBank);
		}
		
		response.getResponseStatus().setSuccess(true);
		return response;
	}
}
