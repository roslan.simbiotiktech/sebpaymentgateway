package com.seb.spg.api.service.report;

import java.util.ArrayList;
import java.util.List;

import com.anteater.library.api.APIRequest;
import com.anteater.library.api.Service;
import com.anteater.library.api.ServiceException;
import com.anteater.library.api.ServiceMeta;
import com.anteater.library.jpa.DatabaseFacade;
import com.seb.spg.api.object.OPaymentDetailReport;
import com.seb.spg.api.request.report.PaymentDetailReportRequest;
import com.seb.spg.api.response.report.PaymentDetailReportResponse;
import com.seb.spg.api.service.SecureService;
import com.seb.spg.api.service.ServiceConstants;
import com.seb.spg.jpa.dao.Status;
import com.seb.spg.jpa.helper.ReportHelper;

@Service(
		name = ServiceConstants.API_SERVICE_PAYMENT_DETAIL_REPORT,
		description = "Payment Detail Report")
public class PaymentDetailReportService extends SecureService<PaymentDetailReportRequest, PaymentDetailReportResponse> {

	public PaymentDetailReportService(APIRequest rawRequest, ServiceMeta meta, PaymentDetailReportRequest request) {
		super(rawRequest, meta, request);
	}

	@Override
	public PaymentDetailReportResponse perform() throws ServiceException {
		
		PaymentDetailReportResponse response = new PaymentDetailReportResponse();
		
		try(DatabaseFacade db = new DatabaseFacade()){
			ReportHelper reportHelper = new ReportHelper(db);

			List<Status> paymentDetailReports = reportHelper.getPaymentDetailReport(request.getPaymentId());
			List<OPaymentDetailReport> opaymentDetailReports = new ArrayList<OPaymentDetailReport>();
			
			if(paymentDetailReports != null){
				for(Status status : paymentDetailReports){
					opaymentDetailReports.add(reportHelper.constructPaymentDetailReport(status, getRequestLocale()));
				}
			}
			
			OPaymentDetailReport[] opaymentDetailReportArr = new OPaymentDetailReport[opaymentDetailReports.size()];
			opaymentDetailReports.toArray(opaymentDetailReportArr);
			
			response.setPaymentDetailReports(opaymentDetailReportArr);
		}
		
		response.getResponseStatus().setSuccess(true);
		return response;
	}
}
