package com.seb.spg.api.response.fpxbank;

import com.anteater.library.api.service.response.ServiceResponse;
import com.anteater.library.json.JsonField;
import com.seb.spg.api.object.OFpxBank;

public class FpxBankQueryResponse extends ServiceResponse {

	@JsonField(
			name = "fpx_bank", 
			description = "FPX Bank Details", 
			mandatory = true)
	private OFpxBank fpxBank;

	public OFpxBank getFpxBank() {
		return fpxBank;
	}

	public void setFpxBank(OFpxBank fpxBank) {
		this.fpxBank = fpxBank;
	}

}
