package com.seb.spg.api.response.report;

import com.anteater.library.api.service.response.ServiceResponse;
import com.anteater.library.json.JsonField;
import com.seb.spg.api.object.OPaymentDetailReport;

public class PaymentDetailReportResponse extends ServiceResponse {

	@JsonField(
			name = "payment_detail_reports", 
			description = "Payment Detail Reports")
	private OPaymentDetailReport [] paymentDetailReports;

	public OPaymentDetailReport[] getPaymentDetailReports() {
		return paymentDetailReports;
	}

	public void setPaymentDetailReports(OPaymentDetailReport[] paymentDetailReports) {
		this.paymentDetailReports = paymentDetailReports;
	}
	
}
