package com.seb.spg.api.response.report;

import com.anteater.library.api.service.response.ServiceResponse;
import com.anteater.library.json.JsonField;
import com.seb.spg.api.object.OFileEncode;

public class PaymentReportExportResponse extends ServiceResponse {

	@JsonField(
			name = "payment_report_excel_file", 
			description = "Payment Report Excel File")
	private OFileEncode paymentReportExcelFile;

	public OFileEncode getPaymentReportExcelFile() {
		return paymentReportExcelFile;
	}

	public void setPaymentReportExcelFile(OFileEncode paymentReportExcelFile) {
		this.paymentReportExcelFile = paymentReportExcelFile;
	}
	
}
