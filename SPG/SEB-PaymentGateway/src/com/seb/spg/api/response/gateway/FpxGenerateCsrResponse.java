package com.seb.spg.api.response.gateway;

import com.anteater.library.api.service.response.ServiceResponse;
import com.anteater.library.json.JsonField;
import com.seb.spg.api.object.OFile;

public class FpxGenerateCsrResponse extends ServiceResponse {

	@JsonField(
			name = "file", 
			description = "FPX Csr File", mandatory = true)
	private OFile file;

	public OFile getFile() {
		return file;
	}

	public void setFile(OFile file) {
		this.file = file;
	}
	
}
