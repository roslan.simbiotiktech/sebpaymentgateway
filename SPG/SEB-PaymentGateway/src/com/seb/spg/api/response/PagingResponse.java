package com.seb.spg.api.response;

import com.anteater.library.api.service.response.ServiceResponse;
import com.anteater.library.json.JsonField;
import com.seb.spg.api.object.Pagination;
import com.seb.spg.api.object.Paging;

public class PagingResponse extends ServiceResponse {

	@JsonField(
			name = "pagination", 
			description = "Pagination")
	private Pagination pagination;

	public Pagination getPagination() {
		return pagination;
	}

	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
	}

	public void setPagination(Paging paging, int totalRecords){
		Pagination pagination = new Pagination();
		pagination.setPaging(paging);
		pagination.setTotalRecords(totalRecords);
		pagination.setMaxPage(pagination.calculateMaxPage(totalRecords, paging.getRecordsPerPage()));
		this.pagination = pagination;
	}
}
