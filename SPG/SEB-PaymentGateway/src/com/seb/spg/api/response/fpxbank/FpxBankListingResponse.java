package com.seb.spg.api.response.fpxbank;

import com.anteater.library.api.service.response.ServiceResponse;
import com.anteater.library.json.JsonField;
import com.seb.spg.api.object.OFpxBank;

public class FpxBankListingResponse extends ServiceResponse {

	@JsonField(
			name = "fpx_banks", 
			description = "FPX Bank available", mandatory = true)
	private OFpxBank [] fpxBanks;

	public OFpxBank[] getFpxBanks() {
		return fpxBanks;
	}

	public void setFpxBanks(OFpxBank[] fpxBanks) {
		this.fpxBanks = fpxBanks;
	}
	
}
