package com.seb.spg.api.response;

import com.anteater.library.api.service.response.ServiceResponse;
import com.anteater.library.json.JsonField;

public abstract class ListingResponse<T> extends ServiceResponse {

	@JsonField(
			name = "records", 
			description = "Records available", mandatory = true)
	private T [] records;

	public T[] getRecords() {
		return records;
	}

	public void setRecords(T[] records) {
		this.records = records;
	}

}
