package com.seb.spg.api.response.gateway;

import com.anteater.library.api.service.response.ServiceResponse;
import com.anteater.library.json.JsonField;
import com.seb.spg.api.object.OFpx;

public class FpxGatewayQueryResponse extends ServiceResponse {

	@JsonField(
			name = "fpx", 
			description = "FPX Gateway Details", mandatory = true)
	private OFpx fpx;

	public OFpx getFpx() {
		return fpx;
	}

	public void setFpx(OFpx fpx) {
		this.fpx = fpx;
	}

}
