package com.seb.spg.api.response.merchant;

import java.util.Arrays;

import com.anteater.library.api.service.response.ServiceResponse;
import com.anteater.library.json.JsonField;
import com.seb.spg.api.object.OMerchant;

public class MerchantListingResponse extends ServiceResponse {

	@JsonField(
			name = "merchants", 
			description = "Merchants available", mandatory = true)
	private OMerchant [] merchants;

	public OMerchant[] getMerchants() {
		return merchants;
	}

	public void setMerchants(OMerchant[] merchants) {
		this.merchants = merchants;
	}

	@Override
	public String toString() {
		return "MerchantListingResponse [merchants=" + Arrays.toString(merchants) + ", responseStatus=" + responseStatus
				+ "]";
	}
}
