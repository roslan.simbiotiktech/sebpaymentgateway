package com.seb.spg.api.response.report;

import com.anteater.library.json.JsonField;
import com.seb.spg.api.object.OPaymentReport;
import com.seb.spg.api.response.PagingResponse;

public class PaymentReportResponse extends PagingResponse {

	@JsonField(
			name = "payment_reports", 
			description = "Payment Reports")
	private OPaymentReport [] paymentReports;

	public OPaymentReport[] getPaymentReports() {
		return paymentReports;
	}

	public void setPaymentReports(OPaymentReport[] paymentReports) {
		this.paymentReports = paymentReports;
	}

}
