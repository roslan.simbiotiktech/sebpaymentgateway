package com.seb.spg.api.response.gateway;

import com.anteater.library.api.service.response.ServiceResponse;
import com.anteater.library.json.JsonField;
import com.seb.spg.api.object.OConfig;

public class ConfigQueryResponse extends ServiceResponse {

	@JsonField(
			name = "config", 
			description = "Gateways Config Details", mandatory = true)
	private OConfig config;

	public OConfig getConfig() {
		return config;
	}

	public void setConfig(OConfig config) {
		this.config = config;
	}
	
}
