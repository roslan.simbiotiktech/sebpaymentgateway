package com.seb.spg.api.response.gateway;

import com.anteater.library.api.service.response.ServiceResponse;
import com.anteater.library.json.JsonField;
import com.seb.spg.api.object.OMbb;

public class MbbGatewayQueryResponse extends ServiceResponse {

	@JsonField(
			name = "mbb", 
			description = "mbb Gateways Details", mandatory = true)
	private OMbb mbb;

	public OMbb getMbb() {
		return mbb;
	}

	public void setMbb(OMbb mbb) {
		this.mbb = mbb;
	}

}
