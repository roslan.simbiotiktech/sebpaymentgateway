package com.seb.spg.api.response.merchant;

import com.anteater.library.api.service.response.ServiceResponse;
import com.anteater.library.json.JsonField;
import com.seb.spg.api.object.OMerchant;

public class MerchantQueryResponse extends ServiceResponse {

	@JsonField(
			name = "merchant", 
			description = "Merchant Details", mandatory = true)
	private OMerchant merchant;

	public OMerchant getMerchant() {
		return merchant;
	}

	public void setMerchant(OMerchant merchant) {
		this.merchant = merchant;
	}

}
