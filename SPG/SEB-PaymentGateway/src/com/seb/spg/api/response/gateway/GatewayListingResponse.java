package com.seb.spg.api.response.gateway;

import com.anteater.library.api.service.response.ServiceResponse;
import com.anteater.library.json.JsonField;
import com.seb.spg.api.object.OGateway;

public class GatewayListingResponse extends ServiceResponse {

	@JsonField(
			name = "gateways", 
			description = "Gateways available", mandatory = true)
	private OGateway [] gateways;

	public OGateway[] getGateways() {
		return gateways;
	}

	public void setGateways(OGateway[] gateways) {
		this.gateways = gateways;
	}
}
