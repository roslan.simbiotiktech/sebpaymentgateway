package com.seb.spg.api.request.fpxbank;

import com.anteater.library.json.JsonField;
import com.seb.spg.api.request.SecureRequest;

public class FpxBankDetailRequest extends SecureRequest {

	@JsonField(
			name="bank_id",
			description = "Bank Id",
			mandatory = true)
	private int bankId;

	public int getBankId() {
		return bankId;
	}

	public void setBankId(int bankId) {
		this.bankId = bankId;
	}

}
