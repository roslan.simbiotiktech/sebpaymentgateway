package com.seb.spg.api.request.gateway;

import com.anteater.library.json.JsonField;
import com.seb.spg.api.object.OFpx;
import com.seb.spg.api.request.SecureRequest;

public class FpxGatewayUpdateRequest extends SecureRequest {

	@JsonField(
			name = "fpx", 
			description = "FPX Gateway Details",
			mandatory = true)
	private OFpx fpx;

	public OFpx getFpx() {
		return fpx;
	}

	public void setFpx(OFpx fpx) {
		this.fpx = fpx;
	}
	
}
