package com.seb.spg.api.request.report;

import com.anteater.library.json.JsonField;
import com.seb.spg.api.request.SecureRequest;

public class PaymentDetailReportRequest extends SecureRequest {
	
	@JsonField(
			name = "payment_id",
			description = "Payment Id",
			mandatory = true)
	private long paymentId;

	public long getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(long paymentId) {
		this.paymentId = paymentId;
	}
	
}
