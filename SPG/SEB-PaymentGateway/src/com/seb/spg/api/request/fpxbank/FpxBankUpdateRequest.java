package com.seb.spg.api.request.fpxbank;

import com.anteater.library.json.JsonField;
import com.seb.spg.api.object.OFpxBankUpdate;
import com.seb.spg.api.request.SecureRequest;

public class FpxBankUpdateRequest extends SecureRequest {

	@JsonField(
			name = "fpx_bank", 
			description = "FPX Bank details",
			mandatory = true)
	private OFpxBankUpdate ofpxBank;

	public OFpxBankUpdate getOfpxBank() {
		return ofpxBank;
	}

	public void setOfpxBank(OFpxBankUpdate ofpxBank) {
		this.ofpxBank = ofpxBank;
	}
	
}
