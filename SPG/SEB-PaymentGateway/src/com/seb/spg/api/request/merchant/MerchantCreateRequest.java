package com.seb.spg.api.request.merchant;

import com.anteater.library.json.JsonField;
import com.seb.spg.api.object.OMerchantBase;
import com.seb.spg.api.request.SecureRequest;

public class MerchantCreateRequest extends SecureRequest {

	@JsonField(
			name = "merchant", 
			description = "Merchant details",
			mandatory = true)
	private OMerchantBase omerchant;

	public OMerchantBase getOmerchant() {
		return omerchant;
	}

	public void setOmerchant(OMerchantBase omerchant) {
		this.omerchant = omerchant;
	}

}
