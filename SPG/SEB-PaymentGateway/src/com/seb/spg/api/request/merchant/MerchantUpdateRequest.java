package com.seb.spg.api.request.merchant;

import com.anteater.library.json.JsonField;
import com.seb.spg.api.object.OMerchantUpdate;
import com.seb.spg.api.request.SecureRequest;

public class MerchantUpdateRequest extends SecureRequest {

	@JsonField(
			name = "merchant", 
			description = "Merchant details",
			mandatory = true)
	private OMerchantUpdate omerchant;

	public OMerchantUpdate getOmerchant() {
		return omerchant;
	}

	public void setOmerchant(OMerchantUpdate omerchant) {
		this.omerchant = omerchant;
	}
	
}
