package com.seb.spg.api.request.merchant;

import com.anteater.library.json.JsonField;
import com.seb.spg.api.request.SecureRequest;

public class MerchantQueryRequest extends SecureRequest {

	@JsonField(
			name="merchant_id",
			description = "Merchant Id",
			mandatory = true)
	private int merchantId;

	public int getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}

}
