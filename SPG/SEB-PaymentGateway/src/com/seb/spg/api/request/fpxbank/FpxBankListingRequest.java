package com.seb.spg.api.request.fpxbank;

import com.anteater.library.json.JsonField;
import com.seb.spg.api.request.SecureRequest;

public class FpxBankListingRequest extends SecureRequest {
	
	@JsonField(
			name="fpx_bank_id",
			description = "FPX Bank Id")
	private String fpxBankId;
	
	@JsonField(
			name="bank_name",
			description = "FPX Bank Name")
	private String fpxBankName;

	@JsonField(
			name="enabled",
			description = "FPX Bank Status (Active/Inactive)")
	private Boolean enabled;

	public String getFpxBankId() {
		return fpxBankId;
	}

	public void setFpxBankId(String fpxBankId) {
		this.fpxBankId = fpxBankId;
	}

	public String getFpxBankName() {
		return fpxBankName;
	}

	public void setFpxBankName(String fpxBankName) {
		this.fpxBankName = fpxBankName;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	
}
