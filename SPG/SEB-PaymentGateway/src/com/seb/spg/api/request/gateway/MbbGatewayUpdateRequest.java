package com.seb.spg.api.request.gateway;

import com.anteater.library.json.JsonField;
import com.seb.spg.api.object.OMbb;
import com.seb.spg.api.request.SecureRequest;

public class MbbGatewayUpdateRequest extends SecureRequest {

	@JsonField(
			name = "mbb", 
			description = "Mbb Gateway Details",
			mandatory = true)
	private OMbb mbb;

	public OMbb getMbb() {
		return mbb;
	}

	public void setMbb(OMbb mbb) {
		this.mbb = mbb;
	}
	
}
