package com.seb.spg.api.request;

import com.anteater.library.api.formatter.UCaseFormatter;
import com.anteater.library.api.service.request.ServiceRequest;
import com.anteater.library.api.utility.StringUtil;
import com.anteater.library.json.JsonField;

public class SecureRequest extends ServiceRequest {
	
	@JsonField(
			name = "uid", 
			description = "API user Id",
			maxLength = 20, minLength = 4, mandatory = true, 
			customFormatter = UCaseFormatter.class)
	private String userId;

	@JsonField(name = "pw", minLength = 5, maxLength = 12, mandatory = true, 
			description = "API password")
	private String password;
	
	@JsonField(
			name = "admin_id", 
			description = "Admin Id (For Audit Trail Only)",
			maxLength = 50)
	private String adminId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAdminId() {
		return adminId;
	}

	public void setAdminId(String adminId) {
		this.adminId = adminId;
	}
	
	public String getPerformerId(){
		return StringUtil.isEmpty(getAdminId()) ? getUserId() : getAdminId();
	}
}
