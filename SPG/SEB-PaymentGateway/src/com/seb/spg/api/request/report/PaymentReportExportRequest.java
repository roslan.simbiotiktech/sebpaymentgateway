package com.seb.spg.api.request.report;

import com.anteater.library.json.JsonField;
import com.seb.spg.api.request.SecureRequest;
import com.seb.spg.consts.PaymentGatewayType;
import com.seb.spg.consts.SpgReferenceStatus;

public class PaymentReportExportRequest extends SecureRequest {
	
	@JsonField(
			name = "payment_gateway",
			description = "Payment Gateway Type")
	private PaymentGatewayType paymentGatewayType;
	
	@JsonField(
			name = "payment_gateway_reference_id",
			description = "Payment Gateway Reference Id",
			maxLength = 25)
	private String paymentGatewayReferenceId;
	
	@JsonField(
			name = "spg_reference_status",
			description = "SPG Reference Status")
	private SpgReferenceStatus spgReferenceStatus;
	
	@JsonField(
			name = "cutomer_username",
			description = "Customer Username",
			maxLength = 50)
	private String customerUsername;
	
	@JsonField(
			name = "customer_email",
			description = "Customer Email",
			maxLength = 254)
	private String customerEmail;
	
	@JsonField(
			name = "merchant_id",
			description = "Merchant Id")
	private Integer merchantId;

	@JsonField(
			name = "merchant_reference_id",
			description = "Merchant Reference Id",
			maxLength = 25)
	private String merchantReferenceId;

	public PaymentGatewayType getPaymentGatewayType() {
		return paymentGatewayType;
	}

	public void setPaymentGatewayType(PaymentGatewayType paymentGatewayType) {
		this.paymentGatewayType = paymentGatewayType;
	}

	public String getPaymentGatewayReferenceId() {
		return paymentGatewayReferenceId;
	}

	public void setPaymentGatewayReferenceId(String paymentGatewayReferenceId) {
		this.paymentGatewayReferenceId = paymentGatewayReferenceId;
	}

	public SpgReferenceStatus getSpgReferenceStatus() {
		return spgReferenceStatus;
	}

	public void setSpgReferenceStatus(SpgReferenceStatus spgReferenceStatus) {
		this.spgReferenceStatus = spgReferenceStatus;
	}

	public String getCustomerUsername() {
		return customerUsername;
	}

	public void setCustomerUsername(String customerUsername) {
		this.customerUsername = customerUsername;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public Integer getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(Integer merchantId) {
		this.merchantId = merchantId;
	}

	public String getMerchantReferenceId() {
		return merchantReferenceId;
	}

	public void setMerchantReferenceId(String merchantReferenceId) {
		this.merchantReferenceId = merchantReferenceId;
	}

}
