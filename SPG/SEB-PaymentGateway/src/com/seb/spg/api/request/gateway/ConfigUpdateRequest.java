package com.seb.spg.api.request.gateway;

import com.anteater.library.json.JsonField;
import com.seb.spg.api.object.OConfig;
import com.seb.spg.api.request.SecureRequest;

public class ConfigUpdateRequest extends SecureRequest {

	@JsonField(
			name = "config", 
			description = "SPG Config Details",
			mandatory = true)
	private OConfig config;

	public OConfig getConfig() {
		return config;
	}

	public void setConfig(OConfig config) {
		this.config = config;
	}

}
