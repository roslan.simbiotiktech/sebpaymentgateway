package com.seb.spg.api.request;

import com.anteater.library.json.JsonField;
import com.seb.spg.api.object.Paging;
import com.seb.spg.api.request.SecureRequest;

public class PagingRequest extends SecureRequest {

	@JsonField(name = "PAGING")
	private Paging paging;

	public Paging getPaging() {
		if(paging == null){
			paging = new Paging();
		}
		return paging;
	}

	public void setPaging(Paging paging) {
		this.paging = paging;
	}

}
