package com.seb.spg.api.request.report;

import com.anteater.library.json.JsonField;
import com.seb.spg.api.object.Paging;

public class PaymentReportRequest extends PaymentReportExportRequest {
	
	@JsonField(name = "PAGING")
	private Paging paging;

	public Paging getPaging() {
		return paging;
	}

	public void setPaging(Paging paging) {
		this.paging = paging;
	}
	
}
