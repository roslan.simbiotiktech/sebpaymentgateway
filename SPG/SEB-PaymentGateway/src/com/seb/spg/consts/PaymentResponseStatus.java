package com.seb.spg.consts;

public enum PaymentResponseStatus {

	SUCCESS("00", "SUCCESS", PaymentStage.RECEIVED_SUCCESS),
	FAILED("01", "FAILED", PaymentStage.RECEIVED_FAILED), // Failed at Gateway
	WAITING("02", "WAITING", PaymentStage.NEW),
	PENDING_AUTH("03", "B2B PENDING AUTHORIZATION", PaymentStage.PENDING_AUTH),
	REQUEST_DUPLICATE("11", "DUPLICATE TRANSACTION ID", null),
	REQUEST_ERROR("12", "REQUEST ERROR", null),
	SERVER_ERROR("20", "SERVER ERROR", null);
	
	String code;
	String message;
	PaymentStage stage;
	
	PaymentResponseStatus(String code, String defaultMessage, PaymentStage stage){
		this.code = code;
		this.message = defaultMessage;
		this.stage = stage;
	}
	
	public String getCode(){
		return code;
	}
	
	public String getDefaultMessage(){
		return message;
	}
	
	public PaymentStage getPaymentStage(){
		return stage;
	}
	
	public static PaymentResponseStatus getResponseStatusByStage(PaymentStage stage){
		for(PaymentResponseStatus status : PaymentResponseStatus.values()){
			if(status.getPaymentStage() == stage){
				return status;
			}
		}
		
		return null;
	}
}
