package com.seb.spg.consts;

public enum PaymentStage {

	NEW(false, null),
	PENDING_AUTH(false, null),
	NOTIFIED_SUCCESS(true, null),
	NOTIFIED_FAILED(false, null),
	RECEIVED_SUCCESS(true, NOTIFIED_SUCCESS),
	RECEIVED_FAILED(false, NOTIFIED_FAILED);

	boolean success;
	PaymentStage notifiedStatus;
	
	PaymentStage(boolean success, PaymentStage notifiedStatus){
		this.success = success;
		this.notifiedStatus = notifiedStatus;
	}
	
	public boolean isSuccess(){
		return success;
	}
	
	public PaymentStage getNotifiedStatus(){
		return notifiedStatus;
	}
}
