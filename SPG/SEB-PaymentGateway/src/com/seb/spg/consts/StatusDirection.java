package com.seb.spg.consts;

public enum StatusDirection {

	GATEWAT_TO_SPG,
	SPG_TO_MERCHANT,
	QUERY,
}
