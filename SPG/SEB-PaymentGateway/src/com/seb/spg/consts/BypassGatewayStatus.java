package com.seb.spg.consts;

public enum BypassGatewayStatus {

	PENDING_AUTH,
	SUCCESS,
	FAILED
}
