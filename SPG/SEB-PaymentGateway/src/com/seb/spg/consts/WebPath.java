package com.seb.spg.consts;

public enum WebPath {
	
	FPX_ROOT(WebPathConstants.FPX_ROOT, null),
	FPX_BANK_LOGO(WebPathConstants.SPG_BANKLOGO, FPX_ROOT),
	MBB_ROOT(WebPathConstants.MBB_ROOT, null),
	MBB_INDIRECT(WebPathConstants.SPG_INDIRECT, MBB_ROOT);
	
	String path;
	WebPath parent;
	
	WebPath(String path, WebPath parent) {
		this.path = path;
		this.parent = parent;
	}
	
	/**
	 * Context Root is expected to not end with slash
	 * - e.g http://localhost:8080/SPG
	 */
	public String getWebFullPath(String contextRoot){
		
		StringBuilder sb = new StringBuilder();
		sb.append(contextRoot);
		appendPathRecursively(sb, this);
		return sb.toString();
	}
	
	private void appendPathRecursively(StringBuilder sb, WebPath target){
		
		if(target.parent != null){
			appendPathRecursively(sb, target.parent);
		}
		
		sb.append("/").append(target.path);
	}
}
