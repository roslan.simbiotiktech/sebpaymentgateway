package com.seb.spg.consts;

public enum MbbCardType {

	VISA("V", 16, 3),
	MASTER("M", 16, 3),
	AMEX("A", 15, 4);
	
	String code;
	int cardNumberLength;
	int cvcLength;
	
	private MbbCardType(String code, int cardNumberLength, int cvcLength) {
		this.code = code;
		this.cardNumberLength = cardNumberLength;
		this.cvcLength = cvcLength;
	}
	
	public String getCode(){
		return code;
	}
	
	public int getCardNumberLength(){
		return cardNumberLength;
	}
	
	public int getCvcLength(){
		return cvcLength;
	}
	
	public static MbbCardType getCardType(String code){
		for(MbbCardType type : MbbCardType.values()){
			if(type.getCode().equals(code)){
				return type;
			}
		}
		
		return null;
	}
}
