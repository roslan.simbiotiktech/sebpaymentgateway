package com.seb.spg.consts;

public enum FpxBankType {

	B2C("01"),
	B2B1("02");
//	B2B2("03"); //B2B2 not supported
	
	String token;
	
	FpxBankType(String token){
		this.token = token;
	}
	
	public String getToken(){
		return token;
	}
}
