package com.seb.spg.consts;

public class WebPathConstants {

	public static final String FPX_ROOT = "fpx";
	public static final String MBB_ROOT = "maybank";
	
	public static final String SPG_PAYMENT = "payment";
	public static final String SPG_QUERY = "query";
	public static final String SPG_DIRECT = "direct";
	public static final String SPG_INDIRECT = "indirect";
	public static final String SPG_BANK = "bank";
	public static final String SPG_BANKS = "banks";
	public static final String SPG_BANKLOGO = "banklogo";
	
	public static final String SPG_TEST = "test";
	
}
