package com.seb.spg.consts;

public enum SpgReferenceStatus {

	PENDING_AUTH(new PaymentStage[] { PaymentStage.PENDING_AUTH }),
	FAILED(new PaymentStage[] { PaymentStage.RECEIVED_FAILED, PaymentStage.NOTIFIED_FAILED }),
	PENDING(new PaymentStage[] { PaymentStage.NEW }),
	SUCCESS(new PaymentStage[] { PaymentStage.NOTIFIED_SUCCESS, PaymentStage.RECEIVED_SUCCESS });
	
	PaymentStage[] paymentStages;

	private SpgReferenceStatus(PaymentStage[] paymentStages) {
		this.paymentStages = paymentStages;
	}

	public PaymentStage[] getPaymentStages() {
		return paymentStages;
	}

	public void setPaymentStages(PaymentStage[] paymentStages) {
		this.paymentStages = paymentStages;
	}

}
