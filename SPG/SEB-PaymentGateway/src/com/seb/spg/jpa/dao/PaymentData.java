package com.seb.spg.jpa.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "spg.payment_data")
public class PaymentData {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="payment_data_id")
	private long paymentDataId;
	
	@Column(name="payment_id")
	private long paymentId;
	
	@Column(name="payment_data_name", nullable = false, length = 50)
	private String paymentDataName;
	
	@Column(name="payment_data_value", nullable = true, length = 1024)
	private String paymentDataValue;

	public long getPaymentDataId() {
		return paymentDataId;
	}

	public void setPaymentDataId(long paymentDataId) {
		this.paymentDataId = paymentDataId;
	}

	public long getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(long paymentId) {
		this.paymentId = paymentId;
	}

	public String getPaymentDataName() {
		return paymentDataName;
	}

	public void setPaymentDataName(String paymentDataName) {
		this.paymentDataName = paymentDataName;
	}

	public String getPaymentDataValue() {
		return paymentDataValue;
	}

	public void setPaymentDataValue(String paymentDataValue) {
		this.paymentDataValue = paymentDataValue;
	}

	@Override
	public String toString() {
		return "PaymentData [paymentDataId=" + paymentDataId + ", paymentId=" + paymentId + ", paymentDataName="
				+ paymentDataName + ", paymentDataValue=" + paymentDataValue + "]";
	}
}
