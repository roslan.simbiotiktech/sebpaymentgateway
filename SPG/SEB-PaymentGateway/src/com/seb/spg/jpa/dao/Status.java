package com.seb.spg.jpa.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import com.seb.spg.consts.StatusDirection;

@Entity
@Table(name = "spg.status")
public class Status {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="status_id")
	private long statusId;
	
	@Column(name="payment_id")
	private long paymentId;
	
	@Column(name="status", nullable = false, length = 50)
	private String status;
	
	@Enumerated(EnumType.STRING)
	@Column(name="direction", nullable = false)
	private StatusDirection direction;
	
	@Column(name="remark", nullable = true, length = 500)
	private String remark;

	@Column(name="created_datetime", updatable = false, insertable = false)
	@Generated(value=GenerationTime.INSERT)
	private Date createdDatetime;

	public long getStatusId() {
		return statusId;
	}

	public void setStatusId(long statusId) {
		this.statusId = statusId;
	}

	public long getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(long paymentId) {
		this.paymentId = paymentId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public StatusDirection getDirection() {
		return direction;
	}

	public void setDirection(StatusDirection direction) {
		this.direction = direction;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	@Override
	public String toString() {
		return "Status [statusId=" + statusId + ", paymentId=" + paymentId + ", status=" + status + ", direction="
				+ direction + ", remark=" + remark + ", createdDatetime=" + createdDatetime + "]";
	}
}
