package com.seb.spg.jpa.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import com.seb.spg.consts.PaymentStage;

@Entity
@Table(name = "spg.payment")
public class Payment {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="payment_id")
	private long paymentId;
	
	@Column(name="merchant_id")
	private int merchantId;
	
	@Column(name="merchant_transaction_id", nullable = false, length = 25)
	private String merchantTransactionId;
	
	@Column(name="amount", nullable = false)
	private BigDecimal amount;
	
	@Column(name="customer_email", nullable = true, length = 254)
	private String customerEmail;
	
	@Column(name="customer_username", nullable = true, length = 50)
	private String customerUsername;
	
	@Enumerated(EnumType.STRING)
	@Column(name="status", nullable = false)
	private PaymentStage status;
	
	@Column(name="product_description", nullable = false, length = 30)
	private String productDescription;
	
	@Column(name="client_return_url", nullable = false, length = 1000)
	private String clientReturnUrl;
	
	@Column(name="gateway_id", length = 50)
	private String gatewayId;
	
	@Column(name="gateway_transaction_id", nullable = true, length = 25)
	private String gatewayTransactionId;
	
	@Column(name="gateway_authorization_datetime", nullable = true)
	private Date gatewayAuthorizationDatetime;
	
	@Column(name="gateway_status_id", nullable = true)
	private Long gatewayStatusId;
	
	@Column(name="merchant_s2s_status_id", nullable = true)
	private Long merchantS2sStatusId;
	
	@Column(name="merchant_client_status_id", nullable = true)
	private Long merchantClientStatusId;
	
	@Column(name="last_updated_by", nullable = true, length = 50)
	private String lastUpdatedBy;
	
	@Column(name="last_updated_datetime", nullable = true, updatable = false, insertable = false)
	@Generated(value=GenerationTime.ALWAYS)
	private Date lastUpdatedDatetime;

	@Column(name="created_by", nullable = false, length = 50)
	private String createdBy;
	
	@Column(name="created_datetime", updatable = false, insertable = false)
	@Generated(value=GenerationTime.INSERT)
	private Date createdDatetime;
	
	@ManyToOne
	@JoinColumn(name="merchant_id", referencedColumnName = "merchant_id", updatable = false, insertable = false)
	private Merchant merchant;
	
	@ManyToOne
	@JoinColumn(name="gateway_status_id", referencedColumnName = "status_id", updatable = false, insertable = false)
	private Status gatewayStatus;
	
	@ManyToOne
	@JoinColumn(name="merchant_s2s_status_id", referencedColumnName = "status_id", updatable = false, insertable = false)
	private Status merchantS2sStatus;
	
	@ManyToOne
	@JoinColumn(name="merchant_client_status_id", referencedColumnName = "status_id", updatable = false, insertable = false)
	private Status merchantClientStatus;
	
	@OneToMany
	@JoinColumn(name="payment_id", updatable = false, insertable = false)
	private Set<PaymentData> paymentDatas;

	public long getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(long paymentId) {
		this.paymentId = paymentId;
	}

	public int getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}

	public String getMerchantTransactionId() {
		return merchantTransactionId;
	}

	public void setMerchantTransactionId(String merchantTransactionId) {
		this.merchantTransactionId = merchantTransactionId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getCustomerUsername() {
		return customerUsername;
	}

	public void setCustomerUsername(String customerUsername) {
		this.customerUsername = customerUsername;
	}

	public PaymentStage getStatus() {
		return status;
	}

	public void setStatus(PaymentStage status) {
		this.status = status;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public String getClientReturnUrl() {
		return clientReturnUrl;
	}

	public void setClientReturnUrl(String clientReturnUrl) {
		this.clientReturnUrl = clientReturnUrl;
	}

	public String getGatewayId() {
		return gatewayId;
	}

	public void setGatewayId(String gatewayId) {
		this.gatewayId = gatewayId;
	}

	public String getGatewayTransactionId() {
		return gatewayTransactionId;
	}

	public void setGatewayTransactionId(String gatewayTransactionId) {
		this.gatewayTransactionId = gatewayTransactionId;
	}

	public Date getGatewayAuthorizationDatetime() {
		return gatewayAuthorizationDatetime;
	}

	public void setGatewayAuthorizationDatetime(Date gatewayAuthorizationDatetime) {
		this.gatewayAuthorizationDatetime = gatewayAuthorizationDatetime;
	}

	public Long getGatewayStatusId() {
		return gatewayStatusId;
	}

	public void setGatewayStatusId(Long gatewayStatusId) {
		this.gatewayStatusId = gatewayStatusId;
	}

	public Long getMerchantS2sStatusId() {
		return merchantS2sStatusId;
	}

	public void setMerchantS2sStatusId(Long merchantS2sStatusId) {
		this.merchantS2sStatusId = merchantS2sStatusId;
	}

	public Long getMerchantClientStatusId() {
		return merchantClientStatusId;
	}

	public void setMerchantClientStatusId(Long merchantClientStatusId) {
		this.merchantClientStatusId = merchantClientStatusId;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public Merchant getMerchant() {
		return merchant;
	}

	public void setMerchant(Merchant merchant) {
		this.merchant = merchant;
	}

	public Status getGatewayStatus() {
		return gatewayStatus;
	}

	public void setGatewayStatus(Status gatewayStatus) {
		this.gatewayStatus = gatewayStatus;
	}

	public Status getMerchantS2sStatus() {
		return merchantS2sStatus;
	}

	public void setMerchantS2sStatus(Status merchantS2sStatus) {
		this.merchantS2sStatus = merchantS2sStatus;
	}

	public Status getMerchantClientStatus() {
		return merchantClientStatus;
	}

	public void setMerchantClientStatus(Status merchantClientStatus) {
		this.merchantClientStatus = merchantClientStatus;
	}

	public Set<PaymentData> getPaymentDatas() {
		return paymentDatas;
	}

	public void setPaymentDatas(Set<PaymentData> paymentDatas) {
		this.paymentDatas = paymentDatas;
	}

	@Override
	public String toString() {
		return "Payment [paymentId=" + paymentId + ", merchantId=" + merchantId + ", merchantTransactionId="
				+ merchantTransactionId + ", amount=" + amount + ", customerEmail=" + customerEmail
				+ ", customerUsername=" + customerUsername + ", status=" + status + ", productDescription="
				+ productDescription + ", clientReturnUrl=" + clientReturnUrl + ", gatewayId=" + gatewayId
				+ ", gatewayTransactionId=" + gatewayTransactionId + ", gatewayAuthorizationDatetime="
				+ gatewayAuthorizationDatetime + ", gatewayStatusId=" + gatewayStatusId + ", merchantS2sStatusId="
				+ merchantS2sStatusId + ", merchantClientStatusId=" + merchantClientStatusId + ", lastUpdatedBy="
				+ lastUpdatedBy + ", lastUpdatedDatetime=" + lastUpdatedDatetime + ", createdBy=" + createdBy
				+ ", createdDatetime=" + createdDatetime + ", merchant=" + merchant + ", gatewayStatus=" + gatewayStatus
				+ ", merchantS2sStatus=" + merchantS2sStatus + ", merchantClientStatus=" + merchantClientStatus
				+ ", paymentDatas=" + paymentDatas + "]";
	}
}
