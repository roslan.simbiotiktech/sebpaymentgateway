package com.seb.spg.jpa.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name = "spg.gateway")
public class Gateway {

	@Id
	@Column(name="gateway_id", length = 50)
	private String gatewayId;
	
	@Column(name="description", nullable = true, length = 500)
	private String description;
	
	@Column(name="minimum_payment", nullable = true)
	private BigDecimal minimumPayment;
	
	@Column(name="maximum_payment", nullable = true)
	private BigDecimal maximumPayment;
	
	@Column(name="enabled")
	private boolean enabled;
	
	@Column(name="last_updated_by", nullable = true, length = 50)
	private String lastUpdatedBy;
	
	@Column(name="last_updated_datetime", nullable = true, updatable = false, insertable = false)
	@Generated(value=GenerationTime.ALWAYS)
	private Date lastUpdatedDatetime;

	@OneToMany
	@JoinColumn(name="gateway_id", updatable = false, insertable = false)
	private Set<GatewayConfig> configs;

	public String getGatewayId() {
		return gatewayId;
	}

	public void setGatewayId(String gatewayId) {
		this.gatewayId = gatewayId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getMinimumPayment() {
		return minimumPayment;
	}

	public void setMinimumPayment(BigDecimal minimumPayment) {
		this.minimumPayment = minimumPayment;
	}

	public BigDecimal getMaximumPayment() {
		return maximumPayment;
	}

	public void setMaximumPayment(BigDecimal maximumPayment) {
		this.maximumPayment = maximumPayment;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public Set<GatewayConfig> getConfigs() {
		return configs;
	}

	public void setConfigs(Set<GatewayConfig> configs) {
		this.configs = configs;
	}

	@Override
	public String toString() {
		return "Gateway [gatewayId=" + gatewayId + ", description=" + description + ", minimumPayment=" + minimumPayment
				+ ", maximumPayment=" + maximumPayment + ", enabled=" + enabled + ", lastUpdatedBy=" + lastUpdatedBy
				+ ", lastUpdatedDatetime=" + lastUpdatedDatetime + ", configs=" + configs + "]";
	}
}
