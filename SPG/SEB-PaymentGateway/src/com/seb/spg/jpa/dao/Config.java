package com.seb.spg.jpa.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "spg.config")
public class Config extends DateBase{

	@Id
	@Column(name="config_key", nullable = false, length = 100)
	private String configKey;

	@Column(name="config_value", nullable = false)
	private String configValue;

	public String getConfigKey() {
		return configKey;
	}

	public void setConfigKey(String configKey) {
		this.configKey = configKey;
	}

	public String getConfigValue() {
		return configValue;
	}

	public void setConfigValue(String configValue) {
		this.configValue = configValue;
	}

	@Override
	public String toString() {
		return "Config [configKey=" + configKey + ", configValue=" + configValue + "]";
	}
}
