package com.seb.spg.jpa.dao;

import java.util.Arrays;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import com.seb.spg.consts.FpxBankType;

@Entity
@Table(name = "spg.fpx_bank")
public class FpxBank {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="bank_id", length = 10)
	private int bankId;
	
	@Column(name="bank_fpx_id", length = 10)
	private String bankFpxId;
	
	@Column(name="bank_name", nullable = true, length = 200)
	private String bankName;
	
	@Enumerated(EnumType.STRING)
	@Column(name="bank_type", nullable = false)
	private FpxBankType bankType;
	
	@Column(name="logo", nullable = true)
	private byte[] logo;

	@Column(name="enabled")
	private boolean enabled;
	
	@Column(name="deleted")
	private boolean deleted;
	
	@Column(name="last_updated_by", nullable = true, length = 50)
	private String lastUpdatedBy;
	
	@Column(name="last_updated_datetime", nullable = true, updatable = false, insertable = false)
	@Generated(value=GenerationTime.ALWAYS)
	private Date lastUpdatedDatetime;
	
	@Column(name="created_by", nullable = false, length = 50)
	private String createdBy;
	
	@Column(name="created_datetime", updatable = false, insertable = false)
	@Generated(value=GenerationTime.INSERT)
	private Date createdDatetime;

	public int getBankId() {
		return bankId;
	}

	public void setBankId(int bankId) {
		this.bankId = bankId;
	}

	public String getBankFpxId() {
		return bankFpxId;
	}

	public void setBankFpxId(String bankFpxId) {
		this.bankFpxId = bankFpxId;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public FpxBankType getBankType() {
		return bankType;
	}

	public void setBankType(FpxBankType bankType) {
		this.bankType = bankType;
	}

	public byte[] getLogo() {
		return logo;
	}

	public void setLogo(byte[] logo) {
		this.logo = logo;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	@Override
	public String toString() {
		return "FpxBank [bankId=" + bankId + ", bankFpxId=" + bankFpxId + ", bankName=" + bankName + ", bankType="
				+ bankType + ", logo=" + Arrays.toString(logo) + ", enabled=" + enabled + ", deleted=" + deleted
				+ ", lastUpdatedBy=" + lastUpdatedBy + ", lastUpdatedDatetime=" + lastUpdatedDatetime + ", createdBy="
				+ createdBy + ", createdDatetime=" + createdDatetime + "]";
	}
}
