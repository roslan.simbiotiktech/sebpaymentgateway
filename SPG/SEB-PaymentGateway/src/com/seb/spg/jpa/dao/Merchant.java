package com.seb.spg.jpa.dao;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name = "spg.merchant")
public class Merchant {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="merchant_id")
	private int merchantId;
	
	@Column(name="merchant_name", nullable = false, length = 50)
	private String merchantName;
	
	@Column(name="description", nullable = true, length = 500)
	private String description;
	
	@Column(name="sign_secret", nullable = false, length = 64)
	private String signatureSecret;
	
	@Column(name="minimum_payment", nullable = true)
	private BigDecimal minimumPayment;
	
	@Column(name="maximum_payment", nullable = true)
	private BigDecimal maximumPayment;
	
	@Column(name="s2s_payment_update_url", nullable = false, length = 1000)
	private String serverToServerPaymentUpdateUrl;
	
	@Column(name="client_payment_update_url", nullable = true, length = 1000)
	private String clientPaymentUpdateUrl;
	
	@Column(name="enabled")
	private boolean enabled;
	
	@Column(name="last_updated_by", nullable = true, length = 50)
	private String lastUpdatedBy;
	
	@Column(name="last_updated_datetime", nullable = true, updatable = false, insertable = false)
	@Generated(value=GenerationTime.ALWAYS)
	private Date lastUpdatedDatetime;
	
	@Column(name="created_by", nullable = false, length = 50)
	private String createdBy;
	
	@Column(name="created_datetime", updatable = false, insertable = false)
	@Generated(value=GenerationTime.INSERT)
	private Date createdDatetime;

	public int getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSignatureSecret() {
		return signatureSecret;
	}

	public void setSignatureSecret(String signatureSecret) {
		this.signatureSecret = signatureSecret;
	}

	public BigDecimal getMinimumPayment() {
		return minimumPayment;
	}

	public void setMinimumPayment(BigDecimal minimumPayment) {
		this.minimumPayment = minimumPayment;
	}

	public BigDecimal getMaximumPayment() {
		return maximumPayment;
	}

	public void setMaximumPayment(BigDecimal maximumPayment) {
		this.maximumPayment = maximumPayment;
	}

	public String getServerToServerPaymentUpdateUrl() {
		return serverToServerPaymentUpdateUrl;
	}

	public void setServerToServerPaymentUpdateUrl(String serverToServerPaymentUpdateUrl) {
		this.serverToServerPaymentUpdateUrl = serverToServerPaymentUpdateUrl;
	}

	public String getClientPaymentUpdateUrl() {
		return clientPaymentUpdateUrl;
	}

	public void setClientPaymentUpdateUrl(String clientPaymentUpdateUrl) {
		this.clientPaymentUpdateUrl = clientPaymentUpdateUrl;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDatetime() {
		return lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Date lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	@Override
	public String toString() {
		return "Merchant [merchantId=" + merchantId + ", merchantName=" + merchantName + ", description=" + description
				+ ", signatureSecret=" + signatureSecret + ", minimumPayment=" + minimumPayment + ", maximumPayment="
				+ maximumPayment + ", serverToServerPaymentUpdateUrl=" + serverToServerPaymentUpdateUrl
				+ ", clientPaymentUpdateUrl=" + clientPaymentUpdateUrl + ", enabled=" + enabled + ", lastUpdatedBy="
				+ lastUpdatedBy + ", lastUpdatedDatetime=" + lastUpdatedDatetime + ", createdBy=" + createdBy
				+ ", createdDatetime=" + createdDatetime + "]";
	}
}
