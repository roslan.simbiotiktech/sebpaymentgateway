package com.seb.spg.jpa.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name = "spg.api_user")
public class ApiUser {

	@Id
	@Column(name="user_id", nullable = false, length = 20)
	private String userId;

	@Column(name="password", nullable = false, length = 128)
	private String password;
	
	@Column(name="created_by", nullable = false, length = 50)
	private String createdBy;
	
	@Column(name="created_datetime", updatable = false, insertable = false)
	@Generated(value=GenerationTime.INSERT)
	private Date createdDatetime;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	@Override
	public String toString() {
		return "ApiUser [userId=" + userId + ", password=" + password + ", createdBy=" + createdBy
				+ ", createdDatetime=" + createdDatetime + "]";
	}
}
