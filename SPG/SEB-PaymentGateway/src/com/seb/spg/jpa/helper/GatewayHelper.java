package com.seb.spg.jpa.helper;

import java.math.BigDecimal;
import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.anteater.library.api.utility.StringUtil;
import com.anteater.library.jpa.DatabaseFacade;
import com.anteater.library.jpa.DatabaseFacadeException;
import com.seb.spg.api.object.OConfig;
import com.seb.spg.api.object.OFpx;
import com.seb.spg.api.object.OFpxBank;
import com.seb.spg.api.object.OFpxBankUpdate;
import com.seb.spg.api.object.OGateway;
import com.seb.spg.api.object.OGatewayBase;
import com.seb.spg.api.object.OMbb;
import com.seb.spg.api.request.fpxbank.FpxBankListingRequest;
import com.seb.spg.consts.FpxBankType;
import com.seb.spg.gateway.exception.InvalidFpxPkiException;
import com.seb.spg.gateway.fpx.FpxPaymentGateway;
import com.seb.spg.gateway.fpx.FpxPkiImplementation;
import com.seb.spg.gateway.mbb.MaybankPaymentGateway;
import com.seb.spg.jpa.dao.Config;
import com.seb.spg.jpa.dao.FpxBank;
import com.seb.spg.jpa.dao.Gateway;
import com.seb.spg.jpa.dao.GatewayConfig;
import com.seb.spg.util.DateUtil;
import com.seb.spg.util.ImageUtil;
import com.seb.spg.util.MapUtil;

public class GatewayHelper extends DatabaseHelper {
	
	private static final Logger logger = LogManager.getLogger(GatewayHelper.class);
	
	public final static String FPX_GATEWAY_CONFIG_MERCHANT_ID = "MERCHANT_ID";
	public final static String FPX_GATEWAY_CONFIG_EXCHANGE_ID = "EXCHANGE_ID";
	public final static String FPX_GATEWAY_CONFIG_SELLER_BANK_CODE = "SELLER_BANK_CODE";
	public final static String FPX_GATEWAY_CONFIG_VERSION = "VERSION";
	public final static String FPX_GATEWAY_CONFIG_SUBMIT_URL = "SUBMIT_URL";
	public final static String FPX_GATEWAY_CONFIG_QUERY_URL = "QUERY_URL";
	public final static String FPX_GATEWAY_CONFIG_BANK_LISTING_URL = "BANK_LISTING_URL";
	public final static String FPX_GATEWAY_CONFIG_CSR_COUNTRY_NAME = "CSR_COUNTRY_NAME";
	public final static String FPX_GATEWAY_CONFIG_CSR_STATE_OR_PROVINCE_NAME = "CSR_STATE_OR_PROVINCE_NAME";
	public final static String FPX_GATEWAY_CONFIG_CSR_LOCALITY_NAME = "CSR_LOCALITY_NAME";
	public final static String FPX_GATEWAY_CONFIG_CSR_ORGANIZATION_NAME = "CSR_ORGANIZATION_NAME";
	public final static String FPX_GATEWAY_CONFIG_CSR_ORGANIZATION_UNIT = "CSR_ORGANIZATION_UNIT";
	public final static String FPX_GATEWAY_CONFIG_CSR_COMMON_NAME = "CSR_COMMON_NAME";
	public final static String FPX_GATEWAY_CONFIG_CSR_EMAIL = "CSR_EMAIL";
	public final static String FPX_GATEWAY_CONFIG_CERTIFICATE_EXPIRY_WARNING_DAYS_BEFORE = "CERTIFICATE_EXPIRY_WARNING_DAYS_BEFORE";
	public final static String FPX_GATEWAY_CONFIG_CERTIFICATE_EXPIRY_WARNING_EMAIL = "CERTIFICATE_EXPIRY_WARNING_EMAIL";
	public final static String FPX_PUBLIC_KEY_FILENAME = "PUBLIC_KEY_FILENAME";
	public final static String FPX_PUBLIC_KEY = "PUBLIC_KEY";
	public final static String FPX_PRIVATE_KEY_FILENAME = "PRIVATE_KEY_FILENAME";
	public final static String FPX_PRIVATE_KEY = "PRIVATE_KEY";
	public final static String FPX_PRIVATE_KEY_IDENTITY_CERT_FILENAME = "PRIVATE_KEY_IDENTITY_CERT_FILENAME";
	public final static String FPX_PRIVATE_KEY_IDENTITY_CERT = "PRIVATE_KEY_IDENTITY_CERT";

	public final static String MBB_GATEWAY_CONFIG_HASH_KEY = "HASH_KEY";
	public final static String MBB_GATEWAY_CONFIG_AMEX_MERCHANT_ID = "AMEX_MERCHANT_ID";
	public final static String MBB_GATEWAY_CONFIG_AMEX_HASH_KEY = "AMEX_HASH_KEY";
	
	public final static String CONFIG_S2S_UPDATE_RETRY_WITHIN_MINUTES = "S2S_UPDATE_RETRY_WITHIN_MINUTES";
	public final static String CONFIG_S2S_UPDATE_TIMEOUT_SECONDS = "S2S_UPDATE_TIMEOUT_SECONDS";
	public final static String CONFIG_GATEWAY_QUERY_TIMEOUT_SECONDS = "GATEWAY_QUERY_TIMEOUT_SECONDS";
	public final static String CONFIG_EMAIL_SENDER = "EMAIL_SENDER";
	public final static String CONFIG_PUBLIC_WEB_PATH = "PUBLIC_WEB_PATH";
	
	public GatewayHelper(DatabaseFacade db) {
		super(db);
	}
	
	public Config getConfig(String configKey){
		Session session = db.getSession();
		Criteria criteria = session.createCriteria(Config.class);
		criteria.add(Restrictions.eq("configKey", configKey));
		return db.queryUnique(criteria);
	}
	
	public List<Config> getConfigs(){
		Session session = db.getSession();
		Criteria criteria = session.createCriteria(Config.class);
		return db.query(criteria);
	}
	
	public FpxBank getFpxBank(String bankId){
		Session session = db.getSession();
		Criteria criteria = session.createCriteria(FpxBank.class);
		criteria.add(Restrictions.eq("bankId", bankId));
		return db.queryUnique(criteria);
	}
	
	public Gateway getGateway(String gatewayId){
		Session session = db.getSession();
		Criteria criteria = session.createCriteria(Gateway.class);
		criteria.add(Restrictions.eq("gatewayId", gatewayId));
		return db.queryUnique(criteria);
	}
	
	public GatewayConfig getGatewayConfig(String gatewayId, String configKey){
		Session session = db.getSession();
		Criteria criteria = session.createCriteria(GatewayConfig.class);
		criteria.add(Restrictions.eq("gatewayId", gatewayId));
		criteria.add(Restrictions.eq("configKey", configKey));
		return db.queryUnique(criteria);
	}
	
	public String getGatewayConfigValue(String gatewayId, String configKey){
		Session session = db.getSession();
		Criteria criteria = session.createCriteria(GatewayConfig.class);
		criteria.add(Restrictions.eq("gatewayId", gatewayId));
		criteria.add(Restrictions.eq("configKey", configKey));
		GatewayConfig config = db.queryUnique(criteria);
		if(config != null) return config.getConfigValue();
		else return null;
	}
	
	public Map<String, String> getGatewayConfigValueLike(String gatewayId, String configKeyLike){
		Session session = db.getSession();
		Criteria criteria = session.createCriteria(GatewayConfig.class);
		criteria.add(Restrictions.eq("gatewayId", gatewayId));
		criteria.add(Restrictions.like("configKey", configKeyLike));
		return MapUtil.toMap(db.query(criteria), GatewayConfig::getConfigKey, GatewayConfig::getConfigValue);
	}
	
	public List<FpxBank> getActiveFpxBanks(){
		Session session = db.getSession();
		Criteria criteria = session.createCriteria(FpxBank.class);
		criteria.add(Restrictions.eq("deleted", false));
		criteria.addOrder(Order.asc("bankType"))
			.addOrder(Order.asc("bankName").ignoreCase());
		return db.query(criteria);
	}
	
	public List<FpxBank> getFpxBanks(FpxBankListingRequest request){
		Session session = db.getSession();
		Criteria criteria = session.createCriteria(FpxBank.class);
		
		addCriteria(criteria, "bankFpxId", request.getFpxBankId());
		addCriteria(criteria, "bankName", request.getFpxBankName());
		addCriteria(criteria, "enabled", request.getEnabled());
		
		return db.query(criteria);
	}
	
	public FpxBank getFpxBank(int bankId){
		Session session = db.getSession();
		Criteria criteria = session.createCriteria(FpxBank.class);
		criteria.add(Restrictions.eq("bankId", bankId));
		return db.queryUnique(criteria);
	}
	
	public Map<String, FpxBank> getFpxBank(FpxBankType bankType){
		Session session = db.getSession();
		Criteria criteria = session.createCriteria(FpxBank.class);
		criteria.add(Restrictions.eq("bankType", bankType));
		return MapUtil.toMap(db.query(criteria), FpxBank::getBankFpxId);
	}
	
	public List<Gateway> getGateways(){
		Session session = db.getSession();
		Criteria criteria = session.createCriteria(Gateway.class);
		return db.query(criteria);
	}
	
	public OGateway constructGateway(Gateway gateway){
		OGateway o = new OGateway();
		
		constructGatewayBase(gateway, o); 
		
		o.setGatewayId(gateway.getGatewayId());
		o.setDescription(gateway.getDescription());
		o.setLastUpdatedBy(gateway.getLastUpdatedBy());
		o.setLastUpdatedDatetime(gateway.getLastUpdatedDatetime());
		
		return o;
	}
	
	public void constructGatewayBase(Gateway gateway, OGatewayBase o){

		if(gateway.getMinimumPayment() != null){
			o.setMinimunPaymentAmount(gateway.getMinimumPayment().doubleValue());
		}
		
		if(gateway.getMaximumPayment() != null){
			o.setMaximumPaymentAmount(gateway.getMaximumPayment().doubleValue());
		}
		
		o.setEnabled(gateway.isEnabled());
		
	}
	
	public OFpx constructFpxGateway(Gateway gateway){
		OFpx o = new OFpx();
				
		constructGatewayBase(gateway, o); 
		
		if(gateway != null && gateway.getConfigs() != null){
			
			for(GatewayConfig gatewayConfig : gateway.getConfigs()){
				String configKey = gatewayConfig.getConfigKey();
				String configValue = gatewayConfig.getConfigValue();
				
				if(configKey.equalsIgnoreCase(FPX_GATEWAY_CONFIG_MERCHANT_ID)){
					o.setMerchantId(configValue);
				}else if(configKey.equalsIgnoreCase(FPX_GATEWAY_CONFIG_EXCHANGE_ID)){
					o.setExchangeId(configValue);
				}else if(configKey.equalsIgnoreCase(FPX_GATEWAY_CONFIG_SELLER_BANK_CODE)){
					o.setSellerBankCode(configValue);
				}else if(configKey.equalsIgnoreCase(FPX_GATEWAY_CONFIG_VERSION)){
					o.setVersion(configValue);
				}else if(configKey.equalsIgnoreCase(FPX_GATEWAY_CONFIG_SUBMIT_URL)){
					o.setSubmitUrl(configValue);
				}else if(configKey.equalsIgnoreCase(FPX_GATEWAY_CONFIG_QUERY_URL)){
					o.setQueryUrl(configValue);
				}else if(configKey.equalsIgnoreCase(FPX_GATEWAY_CONFIG_BANK_LISTING_URL)){
					o.setBankListingUrl(configValue);
				}else if(configKey.equalsIgnoreCase(FPX_GATEWAY_CONFIG_CSR_COUNTRY_NAME)){
					o.setCsrCountryName(configValue);
				}else if(configKey.equalsIgnoreCase(FPX_GATEWAY_CONFIG_CSR_STATE_OR_PROVINCE_NAME)){
					o.setCsrStateOrProvinceName(configValue);
				}else if(configKey.equalsIgnoreCase(FPX_GATEWAY_CONFIG_CSR_LOCALITY_NAME)){
					o.setCsrLocalityName(configValue);
				}else if(configKey.equalsIgnoreCase(FPX_GATEWAY_CONFIG_CSR_ORGANIZATION_NAME)){
					o.setCsrOrganizationName(configValue);
				}else if(configKey.equalsIgnoreCase(FPX_GATEWAY_CONFIG_CSR_ORGANIZATION_UNIT)){
					o.setCsrOrganizationUnit(configValue);
				}else if(configKey.equalsIgnoreCase(FPX_GATEWAY_CONFIG_CSR_COMMON_NAME)){
					o.setCsrCommonName(configValue);
				}else if(configKey.equalsIgnoreCase(FPX_GATEWAY_CONFIG_CSR_EMAIL)){
					o.setCsrEmail(configValue);
				}else if(configKey.equalsIgnoreCase(FPX_GATEWAY_CONFIG_CERTIFICATE_EXPIRY_WARNING_DAYS_BEFORE)){
					o.setCertificateExpiryWarningDaysBefore(Integer.parseInt(configValue));
				}else if(configKey.equalsIgnoreCase(FPX_GATEWAY_CONFIG_CERTIFICATE_EXPIRY_WARNING_EMAIL)){
					o.setCertificateExpiryWarningEmail(configValue);
				}else if(configKey.equalsIgnoreCase(FPX_PUBLIC_KEY)){
					o.setPublicKey(configValue);
				}else if(configKey.equalsIgnoreCase(FPX_PUBLIC_KEY_FILENAME)){
					o.setPublicKeyFileName(configValue);
				}else if(configKey.equalsIgnoreCase(FPX_PRIVATE_KEY)){
					o.setPrivateKey(configValue);
				}else if(configKey.equalsIgnoreCase(FPX_PRIVATE_KEY_FILENAME)){
					o.setPrivateKeyFileName(configValue);
				}else if(configKey.equalsIgnoreCase(FPX_PRIVATE_KEY_IDENTITY_CERT)){
					o.setPrivateKeyIdentityCert(configValue);
				}else if(configKey.equalsIgnoreCase(FPX_PRIVATE_KEY_IDENTITY_CERT_FILENAME)){
					o.setPrivateKeyIdentityCertFileName(configValue);
				}
			}
			
			try {
				X509Certificate x509Cert = FpxPkiImplementation.getPublicCertificate();
				if(x509Cert != null){
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(DateUtil.getStartDate(x509Cert.getNotAfter()));
					calendar.add(Calendar.DAY_OF_MONTH, -1);
					o.setPublicKeyExpiryDate(calendar.getTime());
				}
				
				X509Certificate x509PrivateIdentityCert = FpxPkiImplementation.getPrivateKeyIdentityCertificate();
				if(x509PrivateIdentityCert != null){
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(DateUtil.getStartDate(x509PrivateIdentityCert.getNotAfter()));
					calendar.add(Calendar.DAY_OF_MONTH, -1);
					o.setPrivateKeyExpiryDate(calendar.getTime());
				}
			} catch (InvalidFpxPkiException e) {
				logger.error("Certificate Error Getting Expiry Date", e);
			}
		}
		
		return o;
	}
	
	public OMbb constructMbbGateway(Gateway gateway){
		OMbb o = new OMbb();
		
		constructGatewayBase(gateway, o); 
		
		if(gateway != null && gateway.getConfigs() != null){
			
			for(GatewayConfig gatewayConfig : gateway.getConfigs()){
				String configKey = gatewayConfig.getConfigKey();
				String configValue = gatewayConfig.getConfigValue();
				
				if(configKey.equalsIgnoreCase(FPX_GATEWAY_CONFIG_MERCHANT_ID)){
					o.setMerchantId(configValue);
				}else if(configKey.equalsIgnoreCase(FPX_GATEWAY_CONFIG_SUBMIT_URL)){
					o.setSubmitUrl(configValue);
				}else if(configKey.equalsIgnoreCase(FPX_GATEWAY_CONFIG_QUERY_URL)){
					o.setQueryUrl(configValue);
				}else if(configKey.equalsIgnoreCase(MBB_GATEWAY_CONFIG_AMEX_MERCHANT_ID)){
					o.setAmexMerchantId(configValue);
				}else if(configKey.equalsIgnoreCase(MBB_GATEWAY_CONFIG_AMEX_HASH_KEY)){
					o.setAmexHashKey(configValue);
				}else if(configKey.equalsIgnoreCase(MBB_GATEWAY_CONFIG_HASH_KEY)){
					o.setHashKey(configValue);
				}
			}
		}
		
		return o;
	}
	
	public void updateConfig(String configKey, String value, String userId) throws DatabaseFacadeException{
		Date currentDate = Calendar.getInstance().getTime();
		if(!StringUtil.isEmpty(value)){
			Config config = getConfig(configKey);
			if(config != null && !config.getConfigValue().equalsIgnoreCase(value)){
				config.setConfigValue(value);
				config.setLastUpdatedBy(userId);
				config.setLastUpdatedDatetime(currentDate);
				db.update(config);
			}
		}
	}
	
	public void updateGatewayConfig(String gatewayId, String configKey, String value, String userId) throws DatabaseFacadeException{
		Date currentDate = Calendar.getInstance().getTime();
		if(!StringUtil.isEmpty(value)){
			GatewayConfig gatewayConfig = getGatewayConfig(gatewayId, configKey);
			
			if(gatewayConfig == null){
				gatewayConfig = new GatewayConfig();
				gatewayConfig.setConfigValue(value);
				gatewayConfig.setConfigKey(configKey);
				gatewayConfig.setGatewayId(gatewayId);
				gatewayConfig.setCreatedBy(userId);
				db.insert(gatewayConfig);
			}else if(!gatewayConfig.getConfigValue().equalsIgnoreCase(value)){
				gatewayConfig.setConfigValue(value);
				gatewayConfig.setLastUpdatedBy(userId);
				gatewayConfig.setLastUpdatedDatetime(currentDate);
				db.update(gatewayConfig);
			}
		}
	}
	
	public void manageGateway(Gateway gateway, OGatewayBase o, String userId) throws DatabaseFacadeException{
		
		gateway.setEnabled(o.isEnabled());
		
		if(o.getMinimunPaymentAmount() != null){
			BigDecimal b = null;
			
			if(o.getMinimunPaymentAmount() > 0 ){
				b = new BigDecimal(o.getMinimunPaymentAmount());
			}
			
			gateway.setMinimumPayment(b);
		}else{
			gateway.setMinimumPayment(null);
		}
		
		if(o.getMaximumPaymentAmount() != null){
			BigDecimal b = null;
			
			if(o.getMaximumPaymentAmount() > 0 ){
				b = new BigDecimal(o.getMaximumPaymentAmount());
			}
			
			gateway.setMaximumPayment(b);
		}else{
			gateway.setMaximumPayment(null);
		}
		
		gateway.setLastUpdatedBy(userId);
		gateway.setLastUpdatedDatetime(Calendar.getInstance().getTime());
		
		db.update(gateway);
	}
	
	public void manageFpxGateway(String gatewayId, OFpx o, String userId) throws DatabaseFacadeException{
		
		HashMap<String, String> maps = new HashMap<String, String>();
		maps.put(FPX_GATEWAY_CONFIG_MERCHANT_ID, o.getMerchantId());
		maps.put(FPX_GATEWAY_CONFIG_EXCHANGE_ID, o.getExchangeId());                     
		maps.put(FPX_GATEWAY_CONFIG_SELLER_BANK_CODE, o.getSellerBankCode());
		maps.put(FPX_GATEWAY_CONFIG_VERSION, o.getVersion());
		maps.put(FPX_GATEWAY_CONFIG_SUBMIT_URL, o.getSubmitUrl());
		maps.put(FPX_GATEWAY_CONFIG_QUERY_URL, o.getQueryUrl());
		maps.put(FPX_GATEWAY_CONFIG_BANK_LISTING_URL, o.getBankListingUrl());
		maps.put(FPX_GATEWAY_CONFIG_CSR_COUNTRY_NAME, o.getCsrCountryName());
		maps.put(FPX_GATEWAY_CONFIG_CSR_STATE_OR_PROVINCE_NAME, o.getCsrStateOrProvinceName());
		maps.put(FPX_GATEWAY_CONFIG_CSR_LOCALITY_NAME, o.getCsrLocalityName());
		maps.put(FPX_GATEWAY_CONFIG_CSR_ORGANIZATION_NAME, o.getCsrOrganizationName());
		maps.put(FPX_GATEWAY_CONFIG_CSR_ORGANIZATION_UNIT, o.getCsrOrganizationUnit());
		maps.put(FPX_GATEWAY_CONFIG_CSR_COMMON_NAME, o.getCsrCommonName());
		maps.put(FPX_GATEWAY_CONFIG_CSR_EMAIL, o.getCsrEmail());
		maps.put(FPX_GATEWAY_CONFIG_CERTIFICATE_EXPIRY_WARNING_DAYS_BEFORE, String.valueOf(o.getCertificateExpiryWarningDaysBefore()));
		maps.put(FPX_GATEWAY_CONFIG_CERTIFICATE_EXPIRY_WARNING_EMAIL, o.getCertificateExpiryWarningEmail());
		maps.put(FPX_PUBLIC_KEY_FILENAME, o.getPublicKeyFileName());
		maps.put(FPX_PUBLIC_KEY, o.getPublicKey());
		maps.put(FPX_PRIVATE_KEY_FILENAME, o.getPrivateKeyFileName());
		maps.put(FPX_PRIVATE_KEY, o.getPrivateKey());
		maps.put(FPX_PRIVATE_KEY_IDENTITY_CERT_FILENAME, o.getPrivateKeyIdentityCertFileName());
		maps.put(FPX_PRIVATE_KEY_IDENTITY_CERT, o.getPrivateKeyIdentityCert());
		
		for(Entry<String, String> map : maps.entrySet()){
			updateGatewayConfig(gatewayId, map.getKey(), map.getValue(), userId);
		}

	}
	
	public void manageMbbGateway(String gatewayId, OMbb o, String userId) throws DatabaseFacadeException{
		
		HashMap<String, String> maps = new HashMap<String, String>();
		maps.put(FPX_GATEWAY_CONFIG_MERCHANT_ID, o.getMerchantId());
		maps.put(FPX_GATEWAY_CONFIG_SUBMIT_URL, o.getSubmitUrl());
		maps.put(FPX_GATEWAY_CONFIG_QUERY_URL, o.getQueryUrl());
		maps.put(MBB_GATEWAY_CONFIG_HASH_KEY, o.getHashKey());
		maps.put(MBB_GATEWAY_CONFIG_AMEX_MERCHANT_ID, o.getMerchantId());
		maps.put(MBB_GATEWAY_CONFIG_AMEX_HASH_KEY, o.getHashKey());
		
		for(Entry<String, String> map : maps.entrySet()){
			updateGatewayConfig(gatewayId, map.getKey(), map.getValue(), userId);
		}
	}
	
	public OFpxBank constructFpxBank(FpxBank i, boolean isListing){
		OFpxBank o = new OFpxBank();
		
		o.setBankFpxId(i.getBankFpxId());
		o.setBankId(i.getBankId());
		o.setBankName(i.getBankName());
		o.setBankType(i.getBankType());
		o.setEnabled(i.isEnabled());
		
		if(!isListing){
			o.setCreatedBy(i.getCreatedBy());
			o.setCreatedDatetime(i.getCreatedDatetime());
			o.setLastUpdatedBy(i.getLastUpdatedBy());
			o.setLastUpdatedDatetime(i.getLastUpdatedDatetime());
		}
		
		if(i.getLogo() != null){
			o.setLogo(ImageUtil.convertImageTo64(i.getLogo()));
		}
		
		return o;
	}
	
	public OFpxBank constructFpxBank(FpxBank i){
		return constructFpxBank(i, false);
	}
	
	public FpxBank manageFpxBank(OFpxBankUpdate o, FpxBank fpxBank, String userId) throws DatabaseFacadeException{
		FpxBank f = new FpxBank();
		Date currentDate = Calendar.getInstance().getTime();
		
		if(fpxBank != null){
			f = fpxBank;
			f.setLastUpdatedBy(userId);
			f.setLastUpdatedDatetime(currentDate);
		}else{
			f.setCreatedBy(userId);
			f.setCreatedDatetime(currentDate);
		}
		
		if(!StringUtil.isEmpty(o.getBankName())){
			f.setBankName(o.getBankName());
		}
		
		if(o.getLogo() != null){
			f.setLogo(ImageUtil.convert64ToImage(o.getLogo()));
		}
		
		return f;
	}
	
	public OConfig constructConfig(List<Config> configs){
		OConfig o = new OConfig();
		
		if(configs != null){
			for(Config config : configs){
				String configKey = config.getConfigKey();
				String configValue = config.getConfigValue();
				
				if(configKey.equalsIgnoreCase(CONFIG_S2S_UPDATE_RETRY_WITHIN_MINUTES)){
					o.setS2sUpdateRetryWithinMinutes(StringUtil.parseInt(configValue, 0));
				}else if(configKey.equalsIgnoreCase(CONFIG_S2S_UPDATE_TIMEOUT_SECONDS)){
					o.setS2sUpdateTimeoutSeconds(StringUtil.parseInt(configValue, 0));
				}else if(configKey.equalsIgnoreCase(CONFIG_GATEWAY_QUERY_TIMEOUT_SECONDS)){
					o.setGatewayQueryTimeoutSeconds(StringUtil.parseInt(configValue, 0));
				}else if(configKey.equalsIgnoreCase(CONFIG_EMAIL_SENDER)){
					o.setEmailSender(configValue);
				}else if(configKey.equalsIgnoreCase(CONFIG_PUBLIC_WEB_PATH)){
					o.setPublicWebPath(configValue);
				}
			}
		}
		
		return o;
	}
	
	public void manageConfig(OConfig o, String userId) throws DatabaseFacadeException{
		
		HashMap<String, String> maps = new HashMap<String, String>();
		maps.put(CONFIG_S2S_UPDATE_RETRY_WITHIN_MINUTES, o.getS2sUpdateRetryWithinMinutes() > 0 ? o.getS2sUpdateRetryWithinMinutes() + "" : "");
		maps.put(CONFIG_S2S_UPDATE_TIMEOUT_SECONDS, o.getS2sUpdateTimeoutSeconds() > 0 ? o.getS2sUpdateTimeoutSeconds() + "" : "");
		maps.put(CONFIG_GATEWAY_QUERY_TIMEOUT_SECONDS, o.getGatewayQueryTimeoutSeconds() > 0 ? o.getGatewayQueryTimeoutSeconds() + "" : "");
		maps.put(CONFIG_EMAIL_SENDER, o.getEmailSender());
		maps.put(CONFIG_PUBLIC_WEB_PATH, o.getPublicWebPath());
		
		for(Entry<String, String> map : maps.entrySet()){
			updateConfig(map.getKey(), map.getValue(), userId);
		}
	}
	
	public static boolean matchGatewayId(String inputGatewayId, String systemGatewayId){
		return !StringUtil.isEmpty(inputGatewayId) && inputGatewayId.equalsIgnoreCase(systemGatewayId) ? true : false;
	}
	
	public static boolean isFpx(String gatewayId){
		return matchGatewayId(gatewayId, FpxPaymentGateway.GATEWAY_ID);
	}
	
	public static boolean isMbb(String gatewayId){
		return matchGatewayId(gatewayId, MaybankPaymentGateway.GATEWAY_ID);
	}
}
