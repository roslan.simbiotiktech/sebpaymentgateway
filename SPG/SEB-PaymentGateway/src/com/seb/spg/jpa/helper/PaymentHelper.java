package com.seb.spg.jpa.helper;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.anteater.library.jpa.DatabaseFacade;
import com.seb.spg.consts.PaymentStage;
import com.seb.spg.jpa.dao.Payment;

public class PaymentHelper extends DatabaseHelper {

	public PaymentHelper(DatabaseFacade db) {
		super(db);
	}
	
	public Payment getPayment(int merchantId, String merchantTransactionId){
		return getPayment(merchantId, merchantTransactionId, null);
	}
	
	public Payment getLockedPayment(int merchantId, String merchantTransactionId){
		return getPayment(merchantId, merchantTransactionId, LockMode.PESSIMISTIC_READ);
	}
	
	private Payment getPayment(int merchantId, String merchantTransactionId, LockMode lockMode){
		Session session = db.getSession();
		Criteria criteria = session.createCriteria(Payment.class);
		criteria.add(Restrictions.eq("merchantId", merchantId));
		criteria.add(Restrictions.eq("merchantTransactionId", merchantTransactionId));
		
		if(lockMode != null){
			criteria.setLockMode(lockMode);	
		}
		
		return db.queryUnique(criteria);
	}
	
	public Payment getPayment(long paymentId){
		return getPayment(paymentId, null);
	}
	
	/**
	 * Lock Object
	 * - must be run in transactional context
	 */
	public Payment getLockedPayment(long paymentId){
		return getPayment(paymentId, LockMode.PESSIMISTIC_READ);
	}
	
	private Payment getPayment(long paymentId, LockMode lockMode){
		Session session = db.getSession();
		Criteria criteria = session.createCriteria(Payment.class);
		criteria.add(Restrictions.eq("paymentId", paymentId));
		if(lockMode != null){
			criteria.setLockMode(lockMode);	
		}
		
		return db.queryUnique(criteria);
	}
	
	public List<Payment> getUnnotifiedPayments(String gatewayId, Date notBefore){
		Session session = db.getSession();
		Criteria criteria = session.createCriteria(Payment.class);
		
		PaymentStage[] statuses = new PaymentStage[] {PaymentStage.RECEIVED_FAILED, PaymentStage.RECEIVED_SUCCESS};
		criteria.add(Restrictions.in("status", statuses));
		criteria.add(Restrictions.ge("gatewayAuthorizationDatetime", notBefore));
		criteria.add(Restrictions.eq("gatewayId", gatewayId));
		
		return db.query(criteria);
	}
}
