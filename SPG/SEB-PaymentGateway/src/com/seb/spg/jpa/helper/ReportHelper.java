package com.seb.spg.jpa.helper;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import com.anteater.library.api.utility.ResourceUtil;
import com.anteater.library.jpa.DatabaseFacade;
import com.anteater.library.jpa.PaginatedList;
import com.seb.spg.api.object.OPaymentDetailReport;
import com.seb.spg.api.object.OPaymentReport;
import com.seb.spg.api.object.Paging;
import com.seb.spg.api.request.report.PaymentReportExportRequest;
import com.seb.spg.api.request.report.PaymentReportRequest;
import com.seb.spg.jpa.dao.Payment;
import com.seb.spg.jpa.dao.Status;

public class ReportHelper extends DatabaseHelper {

	public ReportHelper(DatabaseFacade db) {
		super(db);
	}
	
	public Criteria getPaymentReportCriteria(PaymentReportExportRequest r){
		
		Session session = db.getSession();
		Criteria criteria = session.createCriteria(Payment.class, "p");
		criteria.createAlias("p.gatewayStatus", "gs", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("p.merchant", "m");
		
		addCriteria(criteria, "p.gatewayTransactionId", r.getPaymentGatewayReferenceId());
		addCriteria(criteria, "m.merchantId", r.getMerchantId());
		addCriteria(criteria, "p.merchantTransactionId", r.getMerchantReferenceId());
		addCriteria(criteria, "p.customerEmail", r.getCustomerEmail());
		addCriteria(criteria, "p.customerUsername", r.getCustomerUsername());
		
		if(r.getSpgReferenceStatus() != null){
			criteria.add(Restrictions.in("p.status", Arrays.asList(r.getSpgReferenceStatus().getPaymentStages())));
		}
		
		if(r.getPaymentGatewayType() != null){
			addCriteria(criteria, "p.gatewayId", r.getPaymentGatewayType().name());
		}
		
		return criteria;
	}
	
	public List<Payment> getPaymentReportExport(PaymentReportExportRequest r){
		return db.query(getPaymentReportCriteria(r));
	}
	
	public PaginatedList<Payment> getPaymentReport(PaymentReportRequest r){
		
		Paging paging = r.getPaging();
		
		return query(getPaymentReportCriteria(r), paging.getPage(), paging.getRecordsPerPage());
	}
	
	public List<Status> getPaymentDetailReport(Long paymentId){
		
		Session session = db.getSession();
		Criteria criteria = session.createCriteria(Status.class);
		criteria.add(Restrictions.eq("paymentId", paymentId));
		
		return db.query(criteria);
	}
	
	public OPaymentReport constructPaymentReport(Payment i, Locale locale){
		
		OPaymentReport o = new OPaymentReport();
		o.setAmount(i.getAmount().doubleValue());
		o.setClientReturnUrl(i.getClientReturnUrl());
		o.setCreatedBy(i.getCreatedBy());
		o.setCreatedDatetime(i.getCreatedDatetime());
		o.setCustomerEmail(i.getCustomerEmail());
		o.setCustomerUsername(i.getCustomerUsername());
		o.setGatewayAuthorizationDatetime(i.getGatewayAuthorizationDatetime());
		o.setGatewayId(i.getGatewayId());
		o.setGatewayStatusId(i.getGatewayStatusId());
		o.setGatewayTransactionId(i.getGatewayTransactionId());
		o.setLastUpdatedBy(i.getLastUpdatedBy());
		o.setLastUpdatedDatetime(i.getLastUpdatedDatetime());
		o.setMerchantClientStatusId(i.getMerchantClientStatusId());
		o.setMerchantId(i.getMerchantId());
		o.setMerchantS2sStatusId(i.getMerchantS2sStatusId());
		o.setMerchantTransactionId(i.getMerchantTransactionId());
		o.setPaymentId(i.getPaymentId());
		o.setProductDescription(i.getProductDescription());
		o.setStatus(ResourceUtil.get(i.getStatus().name(), locale));
		
		return o;
	}
	
	public OPaymentDetailReport constructPaymentDetailReport(Status i, Locale locale){
		
		OPaymentDetailReport o = new OPaymentDetailReport();
		o.setCreatedDatetime(i.getCreatedDatetime());
		o.setDirection(ResourceUtil.get(i.getDirection().name(), locale));
		o.setRemark(i.getRemark());
		o.setStatus(i.getStatus());
		
		return o;
	}
}
