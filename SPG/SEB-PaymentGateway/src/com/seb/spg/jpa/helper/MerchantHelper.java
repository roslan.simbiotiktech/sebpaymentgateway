package com.seb.spg.jpa.helper;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.anteater.library.api.utility.StringUtil;
import com.anteater.library.jpa.DatabaseFacade;
import com.seb.spg.api.object.OMerchantBase;
import com.seb.spg.api.object.OMerchant;
import com.seb.spg.jpa.dao.Merchant;

public class MerchantHelper extends DatabaseHelper {

	public MerchantHelper(DatabaseFacade db) {
		super(db);
	}
	
	public Merchant getMerchant(int merchantId){
		Session session = db.getSession();
		Criteria criteria = session.createCriteria(Merchant.class);
		criteria.add(Restrictions.eq("merchantId", merchantId));
		return db.queryUnique(criteria);
	}
	
	public Merchant getMerchant(String merchantName){
		Session session = db.getSession();
		Criteria criteria = session.createCriteria(Merchant.class);
		criteria.add(Restrictions.eq("merchantName", merchantName));
		return db.queryUnique(criteria);
	}
	
	public List<Merchant> listMerchant(){
		Session session = db.getSession();
		Criteria criteria = session.createCriteria(Merchant.class);
		return db.query(criteria);
	}

	public OMerchant constructMerchant(Merchant merchant){
		OMerchant o = new OMerchant();
		o.setMerchantId(merchant.getMerchantId());
		o.setMerchantName(merchant.getMerchantName());
		o.setClientPaymentUpdateUrl(merchant.getClientPaymentUpdateUrl());
		o.setCreatedBy(merchant.getCreatedBy());
		o.setCreatedDatetime(merchant.getCreatedDatetime());
		o.setDescription(merchant.getDescription());
		o.setEnabled(merchant.isEnabled());
		o.setLastUpdatedBy(merchant.getLastUpdatedBy());
		o.setLastUpdatedDatetime(merchant.getLastUpdatedDatetime());
		
		BigDecimal maximumPayment = new BigDecimal("0");
		
		if(merchant.getMaximumPayment() != null){
			maximumPayment = merchant.getMaximumPayment();
		}
		o.setMaximumPayment(maximumPayment.doubleValue());
		
		BigDecimal minimumPayment = new BigDecimal("0");
		
		if(merchant.getMinimumPayment() != null){
			minimumPayment = merchant.getMinimumPayment();
		}
		o.setMinimumPayment(minimumPayment.doubleValue());
		
		o.setServerToServerPaymentUpdateUrl(merchant.getServerToServerPaymentUpdateUrl());
		o.setSignatureSecret(merchant.getSignatureSecret());
		
		return o;
	}
	
	public Merchant createMerchant(OMerchantBase o, String userId){
		return manageMerchant(o, null, userId);
	}
	
	public Merchant manageMerchant(OMerchantBase o, Merchant merchant, String userId){
		Merchant m = new Merchant();
		Date currentDate = Calendar.getInstance().getTime();
		
		if(merchant != null){
			m = merchant;
			m.setLastUpdatedBy(userId);
			m.setLastUpdatedDatetime(currentDate);
		}else{
			m.setCreatedBy(userId);
			m.setCreatedDatetime(currentDate);
		}
		
		if(!StringUtil.isEmpty(o.getMerchantName())){
			m.setMerchantName(o.getMerchantName());
		}
		
		if(!StringUtil.isEmpty(o.getClientPaymentUpdateUrl())){
			m.setClientPaymentUpdateUrl(o.getClientPaymentUpdateUrl());
		}
		
		if(!StringUtil.isEmpty(o.getDescription())){
			m.setDescription(o.getDescription());
		}
		
		if(o.getMaximumPayment() > 0 ){
			m.setMaximumPayment(new BigDecimal(o.getMaximumPayment()));
		}
		
		if(o.getMinimumPayment() > 0){
			m.setMinimumPayment(new BigDecimal(o.getMinimumPayment()));
		}
		
		if(!StringUtil.isEmpty(o.getServerToServerPaymentUpdateUrl())){
			m.setServerToServerPaymentUpdateUrl(o.getServerToServerPaymentUpdateUrl());
		}
		
		if(!StringUtil.isEmpty(o.getSignatureSecret())){
			m.setSignatureSecret(o.getSignatureSecret());
		}
		
		m.setEnabled(o.isEnabled());
		
		return m;
	}
	
}
