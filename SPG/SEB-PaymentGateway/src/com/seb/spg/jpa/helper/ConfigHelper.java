package com.seb.spg.jpa.helper;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.anteater.library.jpa.DatabaseFacade;
import com.seb.spg.consts.BypassGatewayStatus;
import com.seb.spg.jpa.dao.Config;

public class ConfigHelper extends DatabaseHelper {

	public ConfigHelper(DatabaseFacade db) {
		super(db);
	}

	public String get(String configKey){
		Criteria criteria = db.getSession()
				.createCriteria(Config.class)
				.add(Restrictions.eq("configKey", configKey));
		
		Config config = db.queryUnique(criteria);
		if(config == null){
			throw new RuntimeException("The requested config '" + configKey + "' was not found.");
		}else{
			return config.getConfigValue();
		}
	}
	
	private static final String EMAIL_SENDER = "EMAIL_SENDER";
	public String getEmailSender(){
		return get(EMAIL_SENDER);
	}
	
	private static final String S2S_UPDATE_TIMEOUT_SECONDS = "S2S_UPDATE_TIMEOUT_SECONDS";
	public int getServerToServerUpdateTimeoutInSeconds(){
		String value = get(S2S_UPDATE_TIMEOUT_SECONDS);
		return Integer.parseInt(value);
	}
	
	private static final String GATEWAY_QUERY_TIMEOUT_SECONDS = "GATEWAY_QUERY_TIMEOUT_SECONDS";
	public int getGatewayQueryTimeoutInSeconds(){
		String value = get(GATEWAY_QUERY_TIMEOUT_SECONDS);
		return Integer.parseInt(value);
	}
	
	private static final String S2S_UPDATE_RETRY_WITHIN_MINUTES = "S2S_UPDATE_RETRY_WITHIN_MINUTES";
	public int getServerToServerUpdateRetryWithinMinutes(){
		String value = get(S2S_UPDATE_RETRY_WITHIN_MINUTES);
		return Integer.parseInt(value);
	}
	
	private static final String PUBLIC_WEB_PATH = "PUBLIC_WEB_PATH";
	public String getPublicWebPath(){
		return get(PUBLIC_WEB_PATH);
	}
	
	private static final String SPG_BYPASS_GATEWAY_ROUTING = "SPG_BYPASS_GATEWAY_ROUTING";
	public boolean isSPGBypassGatewayRounting(){
		
		String value = get(SPG_BYPASS_GATEWAY_ROUTING);
		if(value == null || value.isEmpty()){
			return false;
		}else{
			return Boolean.parseBoolean(value);
		}
	}
	
	private static final String SPG_BYPASS_GATEWAY_STATIC_STATUS = "SPG_BYPASS_GATEWAY_STATIC_STATUS";
	public BypassGatewayStatus getSPGBypassGatewayStatus(){
		String value = get(SPG_BYPASS_GATEWAY_STATIC_STATUS);
		if(value == null || value.isEmpty()){
			return null;
		}else{
			return BypassGatewayStatus.valueOf(value.toUpperCase());
		}
	}
	
	private static final String SPG_TEST_MODE_ENABLED = "SPG_TEST_MODE_ENABLED";
	public boolean isSPGTestModeEnabled(){
		
		String value = get(SPG_TEST_MODE_ENABLED);
		if(value == null || value.isEmpty()){
			return false;
		}else{
			return Boolean.parseBoolean(value);
		}
	}
}
