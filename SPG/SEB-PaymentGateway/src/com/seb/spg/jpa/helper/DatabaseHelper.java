package com.seb.spg.jpa.helper;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.anteater.library.api.utility.StringUtil;
import com.anteater.library.jpa.DatabaseFacade;

public class DatabaseHelper extends com.anteater.library.jpa.DatabaseHelper {

	public DatabaseHelper(DatabaseFacade db) {
		super(db);
	}
	
	protected void addCriteria(Criteria criteria, String key, Integer value){
		if(value != null){
			criteria.add(Restrictions.eq(key, value));
		}
	}
	
	protected void addCriteria(Criteria criteria, String key, String value){
		if(!StringUtil.isEmpty(value)){
			criteria.add(Restrictions.eq(key, value));
		}
	}
	
	protected void addCriteria(Criteria criteria, String key, Boolean value){
		if(value != null){
			criteria.add(Restrictions.eq(key, value));
		}
	}
}
