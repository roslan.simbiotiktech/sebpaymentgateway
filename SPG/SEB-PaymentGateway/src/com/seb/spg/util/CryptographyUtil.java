package com.seb.spg.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CryptographyUtil {

	private static final Logger logger = LogManager.getLogger(CryptographyUtil.class);
	
	public static String toSha512Hexadecimal(String plain){
		try{
			MessageDigest digest = MessageDigest.getInstance("SHA-512");
			byte[] hash = digest.digest(plain.getBytes("UTF-8"));
			return new String(Hex.encodeHex(hash));
		}catch(Exception e){
			logger.error(e.getMessage(), e);
			throw new RuntimeException("Error sha512 - " + e.getMessage());
		}
	}
	
	public static String toSha512HexadecimalLowercase(String plain){
		return toSha512Hexadecimal(plain).toLowerCase();
	}
	
	public static String toSha512HexadecimalUppercase(String plain){
		return toSha512Hexadecimal(plain).toUpperCase();
	}
	
	public static String toHex(byte[] bytes){
		return Hex.encodeHexString(bytes).toUpperCase();
	}
	
	public static byte[] fromHex(String hex) throws DecoderException{
		return Hex.decodeHex(hex.toCharArray());
	}
	
	public static void main(String [] args) throws DecoderException, UnsupportedEncodingException{
		
		String t = "KIOSK01P00043000.00Bill Paymentname@mail.comhttps://domain.co/success.jspVISA4012001038443335Any name202011123ABCD1234";
		String hex = toSha512HexadecimalLowercase(t);
		System.out.println(hex);
		
		System.out.println(URLEncoder.encode("name@mail.com", "UTF-8"));
		/*
		String bytes = new String(fromHex(hex));
		System.out.println(bytes);*/
		
		
	}
}
