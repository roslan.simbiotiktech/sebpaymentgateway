package com.seb.spg.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.codec.binary.Base64;

public class ImageUtil {
	
	public static String convertImageTo64(byte[] image){
		String encodedfile = null;
		
        try {
        	encodedfile = new String(Base64.encodeBase64(image), "UTF-8");
        } catch (IOException e) {
        	System.out.println("Error convertImageTo64");
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
		return encodedfile;
	}
	
	public static byte[] convert64ToImage(String base64Image){
        Base64 decoder = new Base64(); 
		byte[] imgBytes = decoder.decode(base64Image);
		return imgBytes;
	}
	
	public static void main(String [] args){
		
		File file =  new File("C:/Users/Jack Tee/Desktop/logo.png");

        try {
            FileInputStream fileInputStreamReader = new FileInputStream(file);
            byte[] bytes = new byte[(int)file.length()];
            fileInputStreamReader.read(bytes);
            convertImageTo64(bytes);
            fileInputStreamReader.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
		
		String base64Image = "iVBORw0KGgoAAAANSUhEUgAAAH8AAAB/CAMAAADxY+0hAAAA8FBMVEX/1mf/4or/1mj/12n/2Gz/twD/2nL/2W//4IT/23b/3n//3XsuAAD/uQD/5Iv/4Yf/6I0qAADorhQAAADg0oDFjxoiAAAvEgw9JBzq1IcmAAAcAAAXAAD5rQBzTRfijAawmWgLAAA3EgpwVDz9vw52VxT3tw3ix3zvlAJ6Uhf02YbUunVOMycfAAqXf1MyGga7hxo7Ig/cpBVXPy4lAApUMxQ7GApoRBKrexaFYBbPmRdMKhSQVQj6nABXORE3EBGgYQaMcUrDp219Sw/4pABkSDO7dgj/y02ubgn/xT1XMQNiMwBMKQWVZwBBIQC1ewsaoRRXAAAG4UlEQVRoge2aDVPbOBCGSRoDobbjj4sTbENw4zrBCVxIA6VpCwmF68ddy///NydLtrWSlYI/ws3N+J3OFGaYeV7trleyVzs7tWrVqlWrVq1atWo9S01G/yH6pW1Q2qtEL2eBA2e0XRMMvMXqBSwAekssxsL28Bvo1ME2DCT4FLXHSOSgYjxdO0HuAjEmUgeV4+nKE3C7zXrYjgGIj+HtCN1ORC3QEFRngMWTZfPabUMDSQSqxseBJ8jXqUAYgIOKAsDjeTjjATsABqrH7ybwAyrqYLdqAxS/l6yewvexqAkmApXwudUDOkZ3kKgJ4gAYKM0X4hN4I1UndsAawAGoEN+G+E6DUewgMUADUPXqhfTEAWegZAAyeLx4StdUlXcQG0jaQCn+RnyM0ybDQ42NASkCNgBV45PQa6O+NwzjCKiRYgMkAzgAJRLwNN61ZWdKUkBcqGkKQAAK8gX4AwY/udVlXXbehSgF6nl/Op0+HKY1QDJQ4hF4cvWTvq33bizZGR6qDXXt6brVP4QZ2CUGivGfgbdkdxZ8cGXnIVTVtSvLOuHDDBR9BDfhG2nuF7ZsznzJf+shA6oG+MhAJgA5DXBbTtx2AH6y0HUX4SVkwJXdB+0c8JMMwB6Uy4Co6bJ4lHsH4yUpiAwM1w7gd7KPQAE+OG5kS092HpWI/l1SohrQdRnwG4IKyGHgKfyoZ8neN7x46fsOMoAiIDP8qASTJpTbQDPlb8Df2rK3kmK1d1ANHLssn1ZAgQA8gZ/0UO5X90rMD9QdBRchw08CUKQEMR9HX4ifopbzbZzgJelrp6XgGoB8rgLylCCz/Kj0uS0nyv34SJKAgT1SA4if7oaiAJRYPmw73mp8r0iMgTapAWuolg4At3wWj3Ivu984PHoI2q8D0ojS7RgbKNQDYv4e6TyIT087UfBR7sdSRt+/kiJ0hqHGBiB3AlJ+FH64fG2ESs9djQdHStYAkhKcwgh0Cu5CfPiT5WujN2jLQXgcfUWhJvDP0b/gOIqAljThtAeV4KfLx23HWX0ejH3DUPzl4zJ2oATdR8OX5kG0GR07NALpOSTfOYiGvw34Se4HY8m/nisfTNM8DTB+/gv9/OjPlkqyG8Y1UKwA4qcPVH8nyX1UeoMjtEjj2v3x9z/eT7z//rr68eOXM+/+xGkJaBGmBZDrIMzzcfpJ21kh/L2kGGfG4zIIguXKj4A/jSAwZt3ujUKygWtAVflduAxfC/GWg/Co7/oz0/DxWsn+j3/2/a5pEAP++6gGUBGyLbA4v6GObi3Z+vQZRx+Ff2FkH0Cla17H9ejfILNDQQsuvP7w3NLlq0+DwZGvoPD3DJLqWPg5QPz3PnoS/WB52pOt/l2V8Ve1k1tHd6++zOZGMDMxX1meHhN9MDDfOzMCY3n9foH+8t1Eq4of1/9kaFq65fYuLq90zEcHX4vIvPYx3/54+eeZi1qke47rf79Q/YP+A3cfLXJgOrpuyzJZ/8zDhy7ZupgrmC/rtm67prUeacL+k5ufFiBpgNrhyXo6vZXJ+iW/a0eHTutyTvKP7PSnD+u7kOu/eTfA32y/mhaGowerF9f/Xwtdti8Mn9S/554fhqGmcUfA/PsPbMDsuT96y9aGTsw/GiMDHwk+4nsn8HOE6Aich7/BQEOl/MHgjXPqS4AP/i7zGv5MPjj80wNAZyP/eAN/P81+3gMo8/IhyMCz+PAjQM4DOBcA3sDz+Nw3gPx8+P7BZOBZfIgv9v6RyUCH8hdzAX/mmifw6MkVX24+ePlnDGhr153hjsPw/beWOdFg8ik+7wswm4E2a0C7M3XSc9B2nPL95Zm8OFSz+PzLhy/ArAES/7CvW5dLX7kfj8cRH2u20N21xuKLRT9rIPnmnByEJ6ZuezdfIsn61SnS8YUnW7ehKsQXmcP83sBdz9VtvPWid178v2557+Cul119/g9Qoo8AcQpG66kNpdsPJyHFw75fdArVbPKv4dCAFv7BStVg8NnaKzYE22SAPAUqK6brFm48IgOZh0A0eAB7TmX4DQYOmKnPBjwzfik7fqA7UTp+EDjosOMnUHuF8ZsMHJCxG2CD4VeCLx19sQE4fCPDv33h8K8aPOC34OAVTD7p+BMOP2njKYVnDYAQMKPfdPwrmD6XxLOHgXT0nZl8p9PvyoffzKd4ePNAPHnnhv+l8cKLF4K7DzTz9PZDZXzGQHLvJMPeDh7cOmrxFvAdlOzdl2qvfmQMCC7/iO/+VHn3hnfA3X1Cv7e2hmeuvbWAuHtXCbzqm0+cg82XzwC9+tt3zSb0ELl4xVABe1vXDzkHGTW3SqcORCaa24fzHrLaOlvo5IWptWrVqlWrVq1a/2v9C/Zt8uXVau1PAAAAAElFTkSuQmCCxxxxxxxxxxxxxx";
		
		convert64ToImage(base64Image);
		
	}
}
