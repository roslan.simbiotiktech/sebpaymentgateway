package com.seb.spg.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MapUtil {

	public static <V, K> Map<K, V> toMap(Collection<V> list, Function<? super V, ? extends K> keyMapper){
		
		if(list == null) return null;
		else if(list.isEmpty()) return new HashMap<K, V>();
		
		return list.stream().collect(Collectors.toMap(keyMapper, v -> v));
	}
	
	public static <V, K, M> Map<K, M> toMap(Collection<V> list, Function<? super V, ? extends K> keyMapper,
			Function<? super V, ? extends M> valueMapper){
		
		if(list == null) return null;
		else if(list.isEmpty()) return new HashMap<K, M>();
		
		return list.stream().collect(Collectors.toMap(keyMapper, valueMapper));
	}
}
