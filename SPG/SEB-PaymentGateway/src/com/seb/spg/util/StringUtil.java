package com.seb.spg.util;

public class StringUtil extends com.anteater.library.api.utility.StringUtil {
	
	public static String leftMasked(String input, int remain){
		
		StringBuilder sb = new StringBuilder();
		
		for(int i = 0; i < input.length(); i++){
			
			if(input.length() - i <= remain){
				sb.append(input.charAt(i));	
			}else{
				sb.append("*");
			}
		}
		
		return sb.toString();		
	}	
}
