package com.seb.spg.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.spg.gateway.HttpPostable;

public class HttpUtil {

	private static final Logger logger = LogManager.getLogger(HttpUtil.class);
	private static int DEFAULT_TIMEOUT_MILIS = 30000;
	
	public static String post(HttpPostable postable, int timeoutMilis) throws IOException, HttpException {
		return post(postable.getPostUrl(), postable.getPostParameter(), timeoutMilis);
	}
	
	public static String post(HttpPostable postable) throws IOException, HttpException { 
		return post(postable.getPostUrl(), postable.getPostParameter(), DEFAULT_TIMEOUT_MILIS);
	}
	
	public static String post(String postUrl, Map<String, String> postParameters, int timeoutMilis) throws IOException, HttpException {
		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(postUrl);
		
		logger.debug("posting to {}", postUrl);
		if(postParameters != null)
			logger.debug(Arrays.toString(postParameters.entrySet().toArray()));
		
		RequestConfig requestConfig = RequestConfig.custom()
				  .setSocketTimeout(timeoutMilis)
				  .setConnectTimeout(timeoutMilis)
				  .setConnectionRequestTimeout(timeoutMilis)
				  .build();

		post.setConfig(requestConfig);
		
		List<NameValuePair> postParams = new ArrayList<NameValuePair>();
		
		for(Entry<String, String> entry : postParameters.entrySet()){
			NameValuePair nvp = new BasicNameValuePair(entry.getKey(), entry.getValue());
			postParams.add(nvp);
		}
		
		post.setEntity(new UrlEncodedFormEntity(postParams));
		
		HttpResponse response = client.execute(post);
		
		if(response.getStatusLine().getStatusCode() == HttpServletResponse.SC_OK){
			try(InputStream is = response.getEntity().getContent()){
				
				String output = IOUtils.toString(is, "UTF-8");
				if(output != null){
					output = output.trim();
					logger.info("Http 200 success - {}", output);
					return output;
				}else{
					logger.info("Http 200 success but output is NULL");
					return null;
				}
			}
		}else{
			
			logger.error("HTTP Error code {} returned from server {}", response.getStatusLine().getStatusCode(), postUrl);
			
			try(InputStream is = response.getEntity().getContent()){
				String error = IOUtils.toString(is, "UTF-8");
				throw new HttpException(error);
			}
		}
	}
	
	public static void main(String [] args) {
		Map<String, String> postParameters = new HashMap<>();
		postParameters.put("lol", "!@#$%^");
		try {
			String s = post("http://localhost:8080/SPG/fpx/payment", postParameters, 10000);
			System.out.println(s);
		} catch (IOException e) {
			
			e.printStackTrace();
		} catch (HttpException e) {
			e.printStackTrace();
		}
		
	}
}
