package com.seb.spg.notification.email;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.naming.NamingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class EmailManager extends EmailProcessor {

	private static final Logger logger = LogManager.getLogger(EmailManager.class);

	public void sendFpxCertificateExpiryWarningEmail(
			String [] recipients, 
			String exchangeCertificateName, Date exchangeCertificateExpiry, String exchangeCertificateRemarks,
			Date fpxPublicCertificateExpiry, String fpxPublicCertificateRemarks){

		try{
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
			
			Map<String, String> properties = new HashMap<String, String>();
			properties.put("exchange_cert_name", exchangeCertificateName);
			properties.put("exchange_cert_remark", exchangeCertificateRemarks);
			properties.put("fpx_public_cert_remark", fpxPublicCertificateRemarks);
			
			if(exchangeCertificateExpiry != null){
				properties.put("exchange_cert_expiry", sdf.format(exchangeCertificateExpiry));	
			}else{
				properties.put("exchange_cert_expiry", "N/A");
			}
			
			if(fpxPublicCertificateExpiry != null){
				properties.put("fpx_public_cert_expiry", sdf.format(fpxPublicCertificateExpiry));	
			}else{
				properties.put("fpx_public_cert_expiry", "N/A");
			}
			
			String content = processTemplateFile("tpl_certificates_warning.txt", properties);

			StringBuilder actualContent = new StringBuilder();
			String subject = getSubject(content, actualContent);

			send(recipients, null, null, subject, actualContent.toString(), "text/html", null);	
		} catch (IOException | MessagingException | NamingException e) {
			logger.error("error sending email to - " + Arrays.toString(recipients), e);
		}
	}
}
