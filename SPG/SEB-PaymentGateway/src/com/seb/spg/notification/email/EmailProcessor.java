package com.seb.spg.notification.email;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.anteater.library.jpa.DatabaseFacade;
import com.seb.spg.jpa.helper.ConfigHelper;

public abstract class EmailProcessor {
	
	private static final Logger logger = LogManager.getLogger(EmailProcessor.class);
	
	private static final String PATH = "/template/";
	
	private static final String HEADER = "cid:Header";
	private static final String FOOTER = "cid:Footer";
	
	private String senderEmail;

	public EmailProcessor() {
		
		try(DatabaseFacade db = new DatabaseFacade()){
			ConfigHelper cHelper = new ConfigHelper(db);
			senderEmail = cHelper.getEmailSender();
		}
	}
	
	protected void send(String [] to, 
			String [] cc, String [] bcc, 
			String subject, String text, String type, 
			String [] attachments) throws MessagingException, NamingException, IOException{

		logger.info("Sending email to " + Arrays.toString(to) + " for subject: " + subject);
		
		Context initCtx = new InitialContext();
		Context envCtx = (Context) initCtx.lookup("java:comp/env");
		Session session = (Session) envCtx.lookup("mail/SPG");
		
		Message message = new MimeMessage(session);
		message.setSubject(subject);
		message.setFrom(new InternetAddress(senderEmail));
		
		InternetAddress toAddr [] = convert(to);
		InternetAddress ccAddr [] = convert(cc);
		InternetAddress bccAddr [] = convert(bcc);

		if(toAddr != null){
			message.setRecipients(Message.RecipientType.TO, toAddr);	
		}
		if(ccAddr != null){
			message.setRecipients(Message.RecipientType.CC, ccAddr);	
		}
		if(bccAddr != null){
			message.setRecipients(Message.RecipientType.BCC, bccAddr);	
		}
		
		MimeMultipart multipart = new MimeMultipart("related");
		BodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setContent(text, type);
		multipart.addBodyPart(messageBodyPart);
		embededBranding(multipart, text);
		embededAttachment(multipart, attachments);
		
		message.setContent(multipart);
		Transport.send(message);
		
		logger.info("Email sent successfully");
    }
	
	private void embededAttachment(MimeMultipart multipart, String [] attachments) throws MessagingException{

		if(attachments != null && attachments.length > 0){
			
			for(String attach : attachments){
				File f = new File(attach);
				
				BodyPart attachmentPart = new MimeBodyPart();
				DataSource fds = new FileDataSource(attach);
				attachmentPart.setDataHandler(new DataHandler(fds));
				attachmentPart.setFileName(f.getName());
				multipart.addBodyPart(attachmentPart);
				
				logger.debug("Embeded attachment");
			}
		}
	}
	
	private void embededBranding(MimeMultipart multipart, String text) throws MessagingException, IOException{

		if(text.indexOf(HEADER) != -1){
			// Add Header
			try(InputStream in = getTemplatesStream("email_banner_top.jpg")){
				BodyPart headerImagePart = new MimeBodyPart();
				
				DataSource ds = new ByteArrayDataSource(in, "image/jpg");
				headerImagePart.setDataHandler(new DataHandler(ds));
				headerImagePart.setHeader("Content-ID", "<Header>");
				multipart.addBodyPart(headerImagePart);
				
				logger.debug("Embeded sesco header image");	
			}
		}
		
		if(text.indexOf(FOOTER) != -1){
			// Add Footer
			try(InputStream in = getTemplatesStream("email_banner_bottom.jpg")){
				BodyPart headerImagePart = new MimeBodyPart();
				
				DataSource ds = new ByteArrayDataSource(in, "image/jpg");
				headerImagePart.setDataHandler(new DataHandler(ds));
				headerImagePart.setHeader("Content-ID", "<Footer>");
				multipart.addBodyPart(headerImagePart);
				
				logger.debug("Embeded sesco footer image");	
			}
		}
	}
	
	private InputStream getTemplatesStream(String fileName){
		return getClass().getResourceAsStream(PATH + fileName);
	}
	
	private InternetAddress[] convert(String [] address) throws AddressException{
		if(address != null && address.length > 0){
			
			InternetAddress[] addr = new InternetAddress[address.length];
			for(int i = 0; i < address.length; i++){
				addr[i] = new InternetAddress(address[i]);
			}
			
			return addr;
		}else{
			return null;
		}
	}
	
	protected String processTemplateFile(String fileName, Map<String, String> properties) throws IOException {
		
		String content = IOUtils.toString(getTemplatesStream(fileName), "UTF-8"); 
		
		Set<Entry<String, String>> set = properties.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		
		while(iterator.hasNext()){
			Entry<String, String> next = iterator.next();
			
			String target = "{" + next.getKey() + "}";
			String value = next.getValue() != null ? next.getValue() : "";
			
			content = content.replace(target, value);
		}
		
		return content;
	}

	protected String getSubject(String file, StringBuilder outContent) {
		int lineNumber = 0;
		String subject = null;
		Scanner scanner = new Scanner(file);
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			
			if(lineNumber == 0){
				subject = line.trim();
			}else{
				outContent.append(line);
				if(scanner.hasNextLine()){
					outContent.append("\n");
				}
			}
			
			lineNumber++;
		}
		scanner.close();
		
		return subject;
	}
	
	public String[] getRecipients(String csv){
		if(csv == null) return null;
		return csv.split(",");
	}
}
